import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCardViewComponent } from './modal-card-view.component';

describe('ModalCardViewComponent', () => {
  let component: ModalCardViewComponent;
  let fixture: ComponentFixture<ModalCardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCardViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
