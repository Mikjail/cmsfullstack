import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormControl, Validators  } from '@angular/forms';

import { User } from '../../models';
import { UserService } from '../../services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


    
  /**
   * For variable to create formvalidation.
   * 
   * @type {FormGroup}
   * @memberOf LabelComponent
   */
  registerForm: FormGroup; 
  private submitted = false;
  private submittedUser: User;


  constructor( private userService: UserService) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      username: new FormControl('',Validators.required),
      password: new FormControl('', Validators.required)
  })

  }


  onSubmit({username="", password= ""}){
    this.submittedUser = new User("",username, password);
    this.userService.create(this.submittedUser).subscribe(
      response =>{
        
      })
  }

}
