const MongoClient = require('mongodb').MongoClient;
const { MongoDataBase } = require('../mongodb/mongoCommonQuery');
const replyHandler = require('../reply/replyHandler');
const replyMssgs = require ('../reply/replyMssgs');
const CriteriaBuilder = require('../utils/criteriaBuilder');
const {HTTPS_PROXY, CLIENT_URL, ALL_COUNTRIES_SET, ALL_LANGUAGES_SET} =  require('../constants');
const httpRequest = require('request');
const _ = require('underscore');
const log4js = require('log4js');
const logger = log4js.getLogger('appDataGenerator.js');

class ParentController {

    constructor(){
        return this;
    }

    getAllLanguages(request, reply) {
        const urlClient = CLIENT_URL;
        logger.debug(`calling ${urlClient}/masterData/dropDownLists`);
        let languageListBody = '';
        let backUpData= ''
        
          // const get = httpRequest.defaults({ 'proxy': HTTPS_PROXY });
          const get = httpRequest.defaults();
    
        get(`${urlClient}/masterData/dropDownLists`)
            .on('data', function (chunk) {
                languageListBody += String.fromCharCode.apply(null, new Uint8Array(chunk));
    
            }).on('response', function (response) {
                if (response.statusCode != 200) {
                    backUpData = ALL_LANGUAGES_SET
                }
            }).on('end',function(){
                logger.debug("Language List succeeded");
                const languages = backUpData? JSON.parse(backUpData).languages: JSON.parse(languageListBody).languages;
                replyHandler.handleOk({
                    data: {
                        languages: _.map(languages, function (currentObject) {
                            return _.pick(currentObject, "language", "languageCode");
                        })
                    }
                }, reply);
            }).on('error', function (error) {
                reply(error)
            })
    }

    getAllCountries (request, reply) {
        const urlClient = CLIENT_URL;
        logger.debug(`calling ${urlClient}/masterData/countryList`);
        let countryNationalBody = '';
        let backUpData= ''
    
        // const get = httpRequest.defaults({ 'proxy': HTTPS_PROXY });
        const get = httpRequest.defaults();

        get(`${urlClient}/masterData/countryList`)
            .on('data', function (chunk) {
                countryNationalBody += String.fromCharCode.apply(null, new Uint8Array(chunk));
            }).on('response', function (response) {
                if (response.statusCode != 200) {
                    backUpData = ALL_COUNTRIES_SET;
                }
            }).on('end',function(){
                logger.debug("countriesNationalities succeeded");
                const countries = backUpData ? JSON.parse(backUpData).countryList: JSON.parse(countryNationalBody).countryList;
                replyHandler.handleOk({
                    data: {
                        countries: _.map(countries, function (currentObject) {
                            return _.pick(currentObject, "countryName", "countryCode");
                        })
                    }
                }, reply);
            }).on('error', function (error) {
                reply({ error });
            });
    };

    async addUpdate (request, reply, dbName) {
        const MongoDB = new MongoDataBase();
        try{
            const { countryCode, _id } = request.payload;
            
            if(_id) delete request.payload._id

            await MongoDB.connect();
            
            const res = await MongoDB.addUpdateQuery(request.payload, dbName,{ countryCode });   
           
            reply(replyMssgs.createdResponse(null, res));
          
        }catch(error){
    
            reply(replyMssgs.createdResponse(error, null));
        
        }finally{
    
            MongoDB.db.close();
        }
        
    };
    
    async removeByParams (request, reply, dbName){
        const MongoDB = new MongoDataBase();
        try{
            const key = Object.keys(request.params)[0]
            const paramValue = request.params[key];

            await MongoDB.connect();
    
            const res = await MongoDB.deleteQuery({ [key] : paramValue}, dbName)
    
            reply(replyMssgs.deletedResponse(null,dbName + " deleted " + res.deletedCount));
            
        }catch(error){
            reply(replyMssgs.createdResponse(error, null));
        }finally{
            MongoDB.db.close();
        }
    };
    
    async getByQueryCriteria (request, reply, dbName) {
        const MongoDB = new MongoDataBase();
        try{
            const dataToFilter =  dbName !=='files'? request.url.query: request.params;
            const limit = request.url.query.limit ? parseInt(request.url.query.limit) : Number.MAX_SAFE_INTEGER;
            const criteriaBuilder = new CriteriaBuilder();
            
            await MongoDB.connect();
                
            criteriaBuilder.filters(dataToFilter, dbName);
        
            const res = await MongoDB.findQuery(criteriaBuilder.build(), dbName, limit);
            
            if(dbName ==='files'){
                reply(res[0].file.buffer).header('Content-Disposition', 'inline').header('Content-type', 'image/png')
            }else{
                reply(replyMssgs.foundResponse(null, res));
            } 

        }catch(error){
            reply(replyMssgs.createdResponse(error, null));
        }finally{
            MongoDB.db.close();
        }
        
    }



}

module.exports =  ParentController;