import { environment } from '../../environments/environment';

export class Constants{
    root_dir: string;
    IApiRest : API_REST;
    constructor(){
        console.log(environment.apiUrl);
        this.root_dir = environment.apiUrl; 
        this.IApiRest = new API_REST();
    }
}

export class API_REST{
   API_COUNTRY = "/allCountries";
   API_LANGUAGE = "/allLanguages";
   API_CAMPAIGN   = '/campaigns'; 
   API_CALLCENTER = '/callcenters';
   API_OFFICE_LOCATIONS = '/locations';
   API_OFFICE_LOCATIONS_MULTI = '/multiLocations'
   API_FILES = '/files'
   API_FILES_MULTIDELETE = '/removeMultiFiles';
   API_EMAILUS = "/contactUs";
   API_CALLCENTER_MULTI="/multiCallcenters";
}