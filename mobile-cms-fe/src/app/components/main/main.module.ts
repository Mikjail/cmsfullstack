import { NgModule, CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA  } from "@angular/core";
import { CallcenterComponent, CampaignComponent, EmailsettingComponent, ImageComponent, OfficeLocationComponent } from "..";
import { UtilsModule } from "../utils/utils.module";
import { CountryLangBasedModule } from "./countryLangBased/countrylang-based.module";



@NgModule({
    declarations:[
        CallcenterComponent,
        CampaignComponent,
        EmailsettingComponent,
        ImageComponent,
        OfficeLocationComponent
    ],
    exports:[
        CallcenterComponent,
        CampaignComponent,
        EmailsettingComponent,
        ImageComponent,
        OfficeLocationComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    imports:[
        UtilsModule,
        CountryLangBasedModule
    ]
})

export class MainModule {}