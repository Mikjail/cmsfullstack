import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelCreationComponent } from './panel-creation.component';

describe('PanelCreationComponent', () => {
  let component: PanelCreationComponent;
  let fixture: ComponentFixture<PanelCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
