# CMS Mobile Suite Backend

## Prerequisites to run 
 * node-v8.9.1
 * Download from https://nodejs.org/en/
 * Extract and set envirenment variables as follows
      * export NODE_HOME=/home/lasantha/dev/install/node-v8.9.1-linux-x64
      * export PATH=$NODE_HOME/bin:$PATH
      * Check the node version by node -v and npm version by npm -v

## Step To Run
  * npm run dev ( Wil run backend and front end)
  * npm run devBe ( Will only run backend)