export abstract class MainEntity{
    constructor(
        public countryName:string = '', 
        public countryCode:string = ''){
            this.countryName = countryName;
            this.countryCode = countryCode;
    }
}

export abstract class MainBasedProps{
    constructor( public properties: Array<Array<MainProperties>> = new Array<Array<MainProperties>>()){
        this.properties = properties;
    }
}

export abstract class MainProperties{
    constructor( public language:string = "" ){
        this.language= language;
    }
}