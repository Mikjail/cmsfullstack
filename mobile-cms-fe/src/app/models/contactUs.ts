export class ContactUs{
    _id:string;
    email:string;
    published:any;
    constructor(email="", published:any=''){
        this.email=email;
        this.published = published;
    }
}
export class ContactToSearch{
    email:string;
    published:any;
}