import { Component, OnInit, NgZone, ViewChild, ElementRef, Input } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import {  } from 'googlemaps';
import { MapsAPILoader } from '@agm/core'
import { Marker } from '../../../models';
@Component({
  selector: 'app-geolocation',
  templateUrl: './geolocation.component.html',
  styleUrls: ['./geolocation.component.scss']
})
export class GeolocationComponent implements OnInit {
  @Input('init') geoLocation = { draggable:true, latitude: 0, longitude:0, newLoc: false};
  
  public searchControl: FormControl;
  public zoom: number;
  public marker;

  @ViewChild("search")
  public searchElementRef: ElementRef;
  
  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
      //set google maps defaults
      this.zoom = 10;

      //create search FormControl
      this.searchControl = new FormControl();
      this.geoLocation.draggable = true;
      //set current position
      if(this.geoLocation.latitude == 0 &&
       this.geoLocation.longitude == 0){
        this.setCurrentPosition();
      }
      
      //load Places Autocomplete
      this.mapsAPILoader.load().then(() => {
        let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ["address"]
        });
        
        autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
            //get the place result
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          
            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
            
            //set latitude, longitude and zoom
            this.geoLocation.latitude = place.geometry.location.lat();
            this.geoLocation.longitude = place.geometry.location.lng();

            this.zoom = 12;
         
          });
        });
      });

      setTimeout(function(){
       window.dispatchEvent(new Event("resize"));
        
      }, 2000);
  }

  setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.geoLocation.latitude = position.coords.latitude;
        this.geoLocation.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  mapClicked($event: any) {
    this.geoLocation.latitude= $event.coords.lat;
    this.geoLocation.longitude = $event.coords.lng;
    this.geoLocation.draggable = true;
  }
  
  markerDragEnd($event: any) {
    this.geoLocation.latitude= $event.coords.lat;
    this.geoLocation.longitude = $event.coords.lng;
    this.geoLocation.draggable = true;
    
  }
  
  public hasChange(ev){


  }

    

}
