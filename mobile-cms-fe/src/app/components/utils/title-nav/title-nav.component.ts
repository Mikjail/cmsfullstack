import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-title-nav',
  templateUrl: './title-nav.component.html',
  styleUrls: ['./title-nav.component.scss']
})
export class TitleNavComponent implements OnInit {
  @Input('titleComponent') titleComp;
  @Input('countryName') countryName;
  @Input('faIcon') faIcon;
  @Input('editionView') editionView;
  @Output('onEditView') onEditView = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  changeView(){
    this.onEditView.emit();
  }
}
