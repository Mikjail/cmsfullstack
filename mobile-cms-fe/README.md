# CMS Mobile Suite Front End
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.1.
**According to client needs this app isn't mobile responsive.**

### Requeriments
* Node Versions: 6.11.2
* bootstrap: 3.3.7
* font-awesome": 4.7.0
* jquery": 3.2.1,
* ng2-dnd: 4.2.0",
* ng2-select-compat: 1.3.1

## Installation/Serving/Building


### 1. npm install

### 2. Create Client and Generate Palette theme

* Run  **CLIENT=clientName PRIMARY=HexColor SECONDARY=HexColor npm run newClient** to create a new client folder with its own color theme in /src/cientTheme/$CLIENT/theme.scss

* Paste the brand logo into **/src/cientTheme/$CLIENT/**. File name should be main_logo.png 
       
### 3. Development Client
        
* Run **CLIENT=clientName npm run start** for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
     
### 4. Build Client

* Run **CLIENT=clientName npm run buildClient** to build the project. The build artifacts will be stored in the ../ISA-CMS-Backend/server/public/$CLIENT/dist directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
