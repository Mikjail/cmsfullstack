import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { Image, Constants, API_REST } from '../../../models';
import { PaginationComponent } from '../../utils/pagination/pagination.component';
import { AlertService, GenericService } from '../../../services';



@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})


export class ImageComponent implements OnInit {
  @ViewChild(PaginationComponent) changePagination: PaginationComponent;
  private submitted = false;
  /**
 * For variable to create formvalidation.
 * 
 * @type {FormGroup}
 * @memberOf LabelComponent
 */
  public myForm: FormGroup;
  public paginationList = { entityList: [], pagedItems: [], pager: {} };

  /**
   * File Selected/Remove
   * 
   * @type {Blob}
   * @memberOf ImageComponent
   */
  public selectedFile: File;
  public bulkAction;
  public files;
  public fileModalView;
  public multipleFile={fileNames:[]};
  public selectOptions;
  
  /**
   * Var states in order to show/hide elements.
   * 
   * 
   * @memberOf ImageComponent
   */
  public filesEdited = false;
  public imgSelected: boolean = false;
  public imageUrl: any;
  public defaultImage : string;

  /**
   * ROOT URL HOST
   * 
   * @type {string}
   * @memberOf CampaignEditComponent
   */
  public rootDir: string;
  public apiRest: API_REST;
  
  constructor(private genericService: GenericService, private alertService: AlertService, constants: Constants,) {
    this.rootDir = constants.root_dir;
    this.apiRest = constants.IApiRest;
    this.files = [];
    this.multipleFile.fileNames = [];
    this.defaultImage = "assets/imageLoad.gif";
  }
  ngOnInit() {
    this.myForm = new FormGroup({
      filesSelected: new FormControl(''),
      fileName: new FormControl(''),
      image: new FormControl(''),
    })
    this.selectOptions='Bulk Action';
    this.initFiles();
  }


  /**
   *  Bring available files from server
   * 
   * 
   * @memberOf ImageComponent
   */
  initFiles() {
    this.genericService.get(this.apiRest.API_FILES).subscribe(
      response => {
        // console.log(response);
        this.files = response.data.reverse();
        // console.log(this.files);
        this.paginationList.entityList = this.files;
      },
      error => {

      },
      () => {

      });
  }

  /**
  * Once form is complete and click on submit button it will 
  * action.
  * 
  * @memberOf LabelComponent
  */
  onSubmit(value: Image) {
    if (this.selectedFile != undefined) {
      let formData = new FormData();
      formData.append(this.selectedFile.name, this.selectedFile);
      this.imgSelected = false;
      this.imageUrl = "";
      this.genericService.create(formData,this.apiRest.API_FILES).subscribe(
        response => {
          this.files.unshift({ filename: this.selectedFile.name })
          this.paginationList.entityList = this.files;
          this.changePagination.setPage(1);
        },
        error => {
          // this.alertService.error(error);

        },
        () => {

        })
    }
  }


  /**
   * 
   * 
   * 
   * @memberOf ImageComponent
   */
  onDelete([...fileToDelete]) {
    // console.log(fileToDelete);
    let filesToSend = {
       fileNames: fileToDelete
    }
     this.genericService.create(filesToSend,this.apiRest.API_FILES_MULTIDELETE).subscribe(
       response =>{
          // console.log(response);
          if(response.message.code == 201){ 
            fileToDelete.forEach(elementToDelete => {
              let index = this.files.findIndex(element => element.filename ===elementToDelete);
              this.files.splice(index, 1);
            });

          }
       },
       error=>{

       },
       () =>{
        this.paginationList.entityList= this.files;
        this.changePagination.setPage(1);
       });
  }

  findInArray(element){
    return element.filename == "hoi"
  }
  /**
   * Push files into an array to be remove.
   * 
   * @param {any} fileSelected 
   * 
   * @memberOf ImageComponent
   */
  selectToRemoveFile(fileSelected) {
    this.fileModalView = fileSelected;
    let index = this.files.indexOf(fileSelected);

    //  this.files.splice(index,1);
    this.filesEdited = true;

  }

  /**
   * Will delete an array of files.
   * 
   * 
   * @memberOf ImageComponent
   */
  saveChanges() {
    this.filesEdited = false;
  }



  /**
   * Concat root to show the file.
   * 
   * @param {any} fileName 
   * @returns 
   * 
   * @memberOf ImageComponent
   */
  imagSource(fileName) {
    return this.rootDir +this.apiRest.API_FILES+"/"+ fileName;
  }

  /**
   * Once file is selected this will be save in selectedFile.
   * 
   * @param {any} event 
   * 
   * @memberOf ImageComponent
   */
  imageUpload(event) {
    this.imgSelected = true;
    let reader = new FileReader();
    //get the selected file from event
    this.selectedFile = event.target.files[0];
    if (this.selectedFile.type.match('image.*')) {
      reader.onloadend = () => {
        //Assign the result to variable for setting the src of image element
        this.imageUrl = reader.result;
      }
      reader.readAsDataURL(this.selectedFile);
    }
  }

  /**
   * Change filesToDelete values to true/false
   * 
   * 
   * @memberOf ImageComponent
   */
  // toggleDeleteSelection() {
  //   for (var i = 0; i < this.filesToDelete.length; i++) {
  //     this.filesToDelete[i] = this.bulkAction;
  //   }
  //   this.selectOptions='Bulk Action'
  // }

  /**
   * this will push/remove fileNames selected on table.
   * 
   * @param {any} event 
   * @param {any} file 
   * 
   * @memberOf ImageComponent
   */
  fileSelection(event,file){
    if(event.srcElement.checked){
      this.multipleFile.fileNames.push(file);
    }else{
      let index = this.multipleFile.fileNames.indexOf(file);
      this.multipleFile.fileNames.splice(index, 1);
    }
    // console.log(this.multipleFile);
  }


  /**
   * Once select option is clicked this will call
   * onDelete in order to erase multiple files.
   * 
   * @param {any} event 
   * 
   * @memberOf ImageComponent
   */
  deleteSelectedFiles(event){
    if(event.srcElement.value == 'true'){
      this.onDelete(this.multipleFile.fileNames);
    }
  }

  isChecked(fileName){
    return this.multipleFile.fileNames.indexOf(fileName) > -1;
  }
}
