const replyHandler = require('./private/modules/reply/replyHandler');
let serverReference = null;
const _ = require('underscore');
const options = {
    relativeTo: __dirname + '/private/modules'
};
const Joi = require('joi');
const validationSchemas = require('./private/modules/utils/validationSchemas');
const Glue = require('glue');
const Config = require('./config');
const constants = require('./private/modules/constants');

initEnvProperties = async () => {

    console.log("initializing environment properties..");
    let properties =require('./private/config/keys');

    console.log("variable is: " , properties);
    if(_.isEmpty(properties)) throw "API_CMS_CONTEXT env is not set. applications been interrupted";
    
    const validationErrors = Joi.validate(properties, validationSchemas.apiContextSchema);
    
    if(validationErrors.error) throw "environment variable context is wrong: " + validationErrors;

    constants.initProperties(properties);

    try{
        const server = await Glue.compose(Config.manifest, options)
    
        // add unexpected exception handler
        server.ext('onPreResponse',function (request, reply) {
            replyHandler.preResponseHandler(request, reply);
        });

        server.ext('onPostHandler', (request, reply) => {
            replyHandler.onPostResponseHandler(request, reply);
        });
        
       await server.start();

       console.log('Server running at:', server.info.uri);
    }catch(err){
        throw err;
    }
    
}
initEnvProperties();

module.exports = {
};