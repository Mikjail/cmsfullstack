import { Component, OnInit } from '@angular/core';
import {FormControl, FormBuilder} from '@angular/forms';
import { User, UserModel } from '../../../models';
import { AuthenticationService, AlertService } from '../../../services';
@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.scss']
})
export class EditPasswordComponent implements OnInit {
  public userForm;
  public user: UserModel;
  public formSubmitAttempt:boolean;
  public loading: boolean;


  constructor(private fb: FormBuilder, private authService: AuthenticationService, private alertService: AlertService) {
    this.formSubmitAttempt=false
    let currentUser =JSON.parse(localStorage.getItem("currentUser"));
    this.user = {
       username: currentUser.username,
       password: '',
       newPassword: ''
    }
  }

  ngOnInit() {
    this.initForm();
    
  }
  onSubmit(value){
    this.user.password = value.password;
    this.user.newPassword = value.newPassword;
    this.loading =true
    this.authService.updatePass(this.user).subscribe( 
      response =>{
        this.loading = false;
        console.log(response)
        this.alertService.success(["Password changed succesfully"]);
      },
      error =>{
        this.alertService.errorArray([error.message.description]);
        this.loading = false;
      })
    
  }
  initForm(): void {
    let user = JSON.parse(localStorage.getItem("currentUser"));
    this.userForm = this.fb.group({
      password: new FormControl(''),
      newPassword: new FormControl(''),
      confirmPassword: new FormControl(''),
    })
  }

}
