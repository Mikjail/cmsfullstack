//TODO add configuration service for each carrier eg G9 some service is not available
const { SERVER_URL_PREFIX, SERVER_PORT } = require('./private/config/keys');
const constants = require('./private/modules/constants');
const propertiesReader = require('properties-reader');
const log4js = require('log4js');
const logger = log4js.getLogger('server.js');
logger.level = 'debug';
const path = require('path');

const context = {
    applicationContext : SERVER_URL_PREFIX,
    serverPort : SERVER_PORT
};

const routeOptions = {
    routes : {
        prefix: "/" + SERVER_URL_PREFIX
    }
};

const manifest = {
    
    server:{
        connections: {
            routes: {
                files: {
                    relativeTo: path.join(__dirname,'public')
                },
                cors: {
                    origin: ['*'],
                    credentials: true,
                    additionalHeaders: ['headers']
                }
            },
            state: {
                strictHeader: false,
                ignoreErrors: true
            }
        }
    },
    connections: [
        {
            port: context.serverPort
        },
    ],
    registrations: [
        {
            plugin : {
                register : "inert"
            }
        },
        {
            plugin : {
                register : "vision"
            }
        },
        {
            plugin : {
                register : "h2o2"
            }
        },    
        {
            plugin : {
                register : "hapi-swaggered",
                options: {
                    tagging:{
                        mode: 'path',
                        pathLevel: 4
                    },
                    info: {
                        title: 'CMS API',
                        description: 'API-CMS documentation for mobile engine API',
                        version: '1.0',
                    }               
                }                
            },
        },
        {
            plugin : {
                register : "hapi-swaggered-ui",
                options: {
                    title: 'Mobile Engine CMS API',
                    path: '/docs',
                    authorization: {
                        scope: 'query', // header works as well
                      },
                    swaggerOptions: {
                        validatorUrl: null,
                    }               
                }                
            },
            options: routeOptions          
        },
        {
            plugin: {
                register: "./",
            },
            options: routeOptions
        }       
    ]
};

module.exports = {
    manifest: manifest,
    context: context
};


