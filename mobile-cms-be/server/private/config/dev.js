module.exports = {
        CLIENT:"g9",
        CLIENT_URL:"https://g9cmb8.isaaviations.com/service-app/controller",
        CMS_INSTANCE:"http://localhost:3150",
        HAS_PROXY:true,
        HTTPS_PROXY:"http://10.30.100.19:8080",
        MONGO_DB_NAME:"cms",
        MONGO_DB_ADDRESS:"10.30.26.52",//10.8.10.41
        MONGO_DB_CREDENTIALS:"",//G9:2017G9ISAAA
        MONGO_DB_PORT:27017,
        MONGO_DB_AUTHDB:"admin",
        SERVER_PORT:3150,
        SERVER_URL_PREFIX:"mobile-engine",
        PRIVATE_KEY: "isaPrivateKey",
        USERS: { "admin" :"$2a$04$BgNfIzQP1fBYsPlAzDPTwuMMFSMAUAYYJNPg7o.b7rPTK0s5IJBUK" }
}
