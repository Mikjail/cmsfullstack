const log4js = require('log4js');
const logger = log4js.getLogger('replyHandler.js');
logger.level = 'debug';
const boom = require('boom');
const constants = require('../constants');


const preResponseHandler = function(request,reply){
    const response = request.response;
    if(response.isBoom && !response.wasHandled){
        handleUnexpectedErrors(request,reply);
    }else{
        reply(response);
    }
};

const onPostResponseHandler = function( request, reply){
    const response = request.response;
    if (response.isBoom && response.output.statusCode === 404) {
      return reply.file( `client/${constants.CLIENT}/dist/index.html`);
    }
    return reply.continue();
}


const iterate = function(obj) {
    for (let property in obj) {
        if (obj.hasOwnProperty(property)) {
            if (typeof obj[property] === "object")
                iterate(obj[property]);
            else
                console.log(property + "   " + obj[property]);
        }
    }
}

const handleOk = function (data, reply) {
    reply({
            hash: data.hash,
            message: {
                code: 200,
                title: "success",
                description: ""
            },
            data: data.data
        }
    );
};


const handleException = function (error, reply) {
    logger.error("handling exception: " + error);
    reply(generateBoom(boom.internal(),"please ask administrator to check logs"));
};

const handleUnexpectedErrors = function(request,reply){
    //todo reveiw all possible unhandled exception
    //todo current covers only wrong json format error
    const message = request.response.data === null ? request.response.message :
        request.response.data.message;
    reply(generateBoom(boom.badRequest(),message));
};

const handleInvalidInputData = function (errors, reply) {
    reply(generateBoom(boom.badRequest(),errors));
};

const generateBoom = function(boom,description){
    boom.wasHandled = true;
    boom.output.payload = {
        message:{
            code: boom.output.statusCode,
            title: boom.message,
            description: description
        }
    };
    return boom;
};

const generateInvalidRequest = function (errors) {
    return generateBoom(boom.badRequest(),errors);
};
const handleOkWithCookie = function (request,data, reply) {
    if(request.headers.cookieSid){
        reply({code: 200, message: 'Ok', description: 'Hello world!'})
            .header('set-cookie', request.headers.cookieSid ? 'SID=' + request.headers.cookieSid : '');
    }else{
        reply({code: 200, message: 'Ok', description: 'Hello world!'})
    }
};

const handleError= function(response, reply){
    reply({
        code: response.statusCode || 500, 
        message: response.statusMessage || 'error',
    })
}

const handleAuth = function(request,reply, data){

    if (request.authStatus.isAccessGranted) {
        reply({
            code: 200, 
            message: request.authStatus.message,
            data: data ? data:'', 
            token: request.authStatus.token
        });
    } else {
        reply({
            code: request.authStatus.code || 400, 
            message: request.authStatus.message || "error"
        })
    }
};


module.exports = {
    handleInvalidInputData: handleInvalidInputData,
    handleOk: handleOk,
    handleException: handleException,
    preResponseHandler: preResponseHandler,
    generateBoom: generateInvalidRequest,
    handleOkWithCookie: handleOkWithCookie,
    handleAuth: handleAuth,
    onPostResponseHandler: onPostResponseHandler,
    handleError: handleError
};
