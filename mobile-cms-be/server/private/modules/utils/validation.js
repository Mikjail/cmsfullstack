const generatePayloadValidator = function(validator){
    return  {payload: validator,options: {allowUnknown: true}} 
};

const generateParamsValidator = function(validator){
    return  {params: validator,options: {allowUnknown: true}} 
};

const generateQueryValidator = function(validator){
    return  {query: validator,options: {allowUnknown: true}} 
};


const generateError = function(code,message){
    return {code:code,message:message};
};

module.exports = {
    generatePayloadValidator: generatePayloadValidator,
    generateParamsValidator: generateParamsValidator,
    generateQueryValidator:generateQueryValidator,
    generateError: generateError
};

