import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Constants, 
  API_REST, 
  CallCenter, 
  CityModel,
  CityProperty } from '../../../models';
import { GenericService } from '../../../services';
import { SearchFilterComponent} from '../../utils/search-filter';
import { ModuleListingComponent} from '../countryLangBased/module-listing';


@Component({
  selector: 'app-callcenter',
  templateUrl: './callcenter.component.html',
  styleUrls: ['./callcenter.component.scss']
})

export class CallcenterComponent implements OnInit {
  @Input('searchInput') searchFilter = { 
    apiRest:'', 
    availableSearch:{
      searchStatus: false,
      searchName:false,
      searchCountry: true,
      searchLanguage: true
    },
    searchStatus: true,
    countryFromServer:[],
    languageArray:[]
   };
  
  /**
   * Entity directive.
   * 
   * @type {ModuleListingComponent}
   * @memberof OfficeLocationComponent
   */
  @ViewChild(ModuleListingComponent) EntityComponent: ModuleListingComponent;
  /**
  * Select an Office and set @param this.callCenterToEdit.entityLocation to edit it.
  * 
  * @param {any} officeLocation 
  * 
  * @memberOf OfficeLocationComponent
  */
 editLocation({newLocationToEdit, cityIndex}): void {
       this.callCenterToEdit.edition = true;
       this.callCenterToEdit.cityIndexSelected = cityIndex;
       this.callCenterToEdit.entityLocation = newLocationToEdit;
        this.callCenterToEdit.apiService = this.apiRest.API_CALLCENTER;
        this.callCenterToEdit.module = 'CallCenter';
  }

 
  @ViewChild(SearchFilterComponent) searchFilterComponent: SearchFilterComponent;
  getFromSearch(callcentersFound){
    this.callCenterToEdit.edition = false;
    if(callcentersFound.value.length > 0){
      this.callCenters = callcentersFound.value.sort(this.compareByCountry);
      this.EntityComponent.refreshLocations(this.callCenters);
      this.entityLocation.resultsFound = true;
    }else{
      this.entityLocation.resultsFound = false;
    }

  } 

  /**
   * Filter Form.
   * 
   * @type {Array<string>}
   * @memberOf CallcenterComponent
   */
  public resultsFound = false;


  /**
   * Table list.
   * 
   * @param this.callCenters publish checkBox value.
   * @memberOf CallcenterComponent
   */
  public callCenters;

  /**
   * ROOT URL HOST
   * 
   * @type {string}
   * @memberOf CallcenterComponent
   */
  public apiRest: API_REST;

  /**
   * To enable Callcenter edit directive.
   * 
   * @memberof CallcenterComponent
   */
   public callCenterToEdit = { 
    cityIndexSelected: 0,
    edition: false, 
    entityLocation: new CallCenter(),
    module: 'CallCenter',
    apiService: '' 
   };

    /**
   * Module Listing/ Panel Creation Directive 
   * 
   * @memberof OfficeLocationComponent
   */
    public entityLocation = { 
      module: new CallCenter,
      locationList: [],
      apiService:'',
      apiServiceMulti: '', 
      resultsFound:false,
      editEntity: false ,
      countrySelectionSetting: false
    };


  /**
   * Pagination Directive
   * 
   * @private
   * @type {any[]}
   * @memberOf CallcenterComponent
   */
  public paginationList = {
    entityList: [],
    pagedItems: [],
    pager: {},
    setPage: {},
    customNumberPages: 8
  };


  /**
   * Edit Data Table.
   * 
   * 
   * @memberOf CallcenterComponent
   */
  public countrySelectionSetting;
  public existingCountLang=[];
  public officeLocations;
  public existingCountries;
  public buttonNewCountry = "Settings"

  constructor(
    private genServices: GenericService,
    private constants: Constants) {
    this.apiRest = constants.IApiRest;
    this.existingCountries = [];
    this.searchFilter.apiRest = this.apiRest.API_CALLCENTER;
    this.entityLocation.apiService=  this.apiRest.API_CALLCENTER;
    this.callCenterToEdit.apiService =  this.apiRest.API_CALLCENTER;
    this.entityLocation.apiServiceMulti = this.apiRest.API_CALLCENTER_MULTI;
    this.entityLocation.countrySelectionSetting = false;

    this.callCenters=[];

    
  }

  ngOnInit() {
  }


   /**
   * Helper to sort an Array of Campaigns 
   * 
   * @param {CallCenter} countryA 
   * @param {CallCenter} countryB 
   * @returns {number} 
   * 
   * @memberOf CampaignListComponent
   */
  compareByCountry(countryA:CallCenter,countryB:CallCenter):number{
    if(countryA.countryName < countryB.countryName){
      return -1
    }
    if(countryA.countryName > countryB.countryName){
      return 1
    }
    return 0;
  }


  /**
   * Add a new country with all it's default properties.
   * 
   * 
   * @memberOf OfficeLocationComponent
   */
  addNewCountry({newCountryToEdit, newLanguageToEdit, newTextToEdit, newLocationToEdit}): void {

      let countrySelected = this.searchFilter.countryFromServer.find(country => country.countryName == newCountryToEdit);
      if(!newLocationToEdit){
        newLocationToEdit = new CallCenter(countrySelected.countryName, countrySelected.countryCode);
      }
      newLocationToEdit.cities.push(new CityModel(newTextToEdit))
      let cityIndex = newLocationToEdit.cities.length-1;
      let properties = []
      properties[0] = new CityProperty(newLanguageToEdit, newTextToEdit);
      newLocationToEdit.cities[cityIndex].properties[0] =properties;
      this.editLocation({newLocationToEdit, cityIndex});
      this.entityLocation.countrySelectionSetting = false;
  }


}
