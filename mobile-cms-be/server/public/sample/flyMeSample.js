
db.campaigns.insert(
	[{
		"_id": "5b13d944207b3d6c5f346d5f",
		"countryName": "United States",
		"countryCode": "US",
		"campaigns": [{
			"properties": [{
				"language": "en",
				"campaignName": "50% off",
				"fileName": "nepal.png",
				"link": "http://www.flyme.mv/",
				"fromSchedule": "2018/06/03",
				"toSchedule": "2026/12/25",
				"modifiedDate": "2018/06/03"
			}, {
				"language": "es",
				"campaignName": "50% Descuento",
				"fileName": "nepal.png",
				"link": "http://www.flyme.mv/",
				"fromSchedule": "2018/06/04",
				"toSchedule": "2018/06/04",
				"modifiedDate": "2018/06/04"
			}],
			"name": "50% off"
		}, {
			"properties": [{
				"language": "en",
				"campaignName": "20% off",
				"fileName": "istanbul.png",
				"link": "http://www.flyme.mv/",
				"fromSchedule": "2018/06/03",
				"toSchedule": "2026/12/25",
				"modifiedDate": "2018/06/03"
			}, {
				"language": "es",
				"campaignName": "20% Descuento",
				"fileName": "istanbul.png",
				"link": "http://www.flyme.mv/",
				"fromSchedule": "2018/06/04",
				"toSchedule": "2018/06/04",
				"modifiedDate": "2018/06/04"
			}],
			"name": "20% off"
		}, {
			"properties": [{
				"language": "en",
				"campaignName": "10% Off",
				"fileName": "india.png",
				"link": "http://www.flyme.mv/",
				"fromSchedule": "2018/06/03",
				"toSchedule": "2026/12/25",
				"modifiedDate": "2018/06/03"
			}, {
				"language": "es",
				"campaignName": "10% Descuento",
				"fileName": "india.png",
				"link": "http://www.flyme.mv/",
				"fromSchedule": "2018/06/04",
				"toSchedule": "2018/06/04",
				"modifiedDate": "2018/06/04"
			}],
			"name": "10% Off"
		}]
	}, {
		"_id": "5b14c800207b3d6c5f346d61",
		"countryName": "Ukraine",
		"countryCode": "UA",
		"campaigns": [{
			"properties": [{
				"language": "en",
				"campaignName": "Campaign",
				"fileName": "nepal.png",
				"link": "sadasd",
				"fromSchedule": "2018/06/04",
				"toSchedule": "2026/12/25",
				"modifiedDate": "2018/06/04"
			}, {
				"language": "de",
				"campaignName": "Campaign in Deutch",
				"fileName": "india.png",
				"link": "sadasd",
				"fromSchedule": "2018/06/04",
				"toSchedule": "2018/06/04",
				"modifiedDate": "2018/06/04"
			}],
			"name": "Campaign"
		}]
	}]);

db.callcenters.insert( 
	[{
		"countryName": "Tuvalu",
		"countryCode": "TV",
		"cities": [{
			"properties": [
				[{
					"language": "en",
					"cityName": "Lost Ciy",
					"openingTime": "from 9am to 7pm",
					"telephone": "2312321"
				}, {
					"language": "es",
					"cityName": "Ciudad Perdida",
					"openingTime": "from 9am to 7pm",
					"telephone": "2312321"
				}],
				[{
					"language": "en",
					"cityName": "Lost Ciy",
					"openingTime": "from 9am to 7pm",
					"telephone": "555 666 888"
				}, {
					"language": "es",
					"cityName": "Ciudad Perdida",
					"openingTime": "from 9am to 7pm",
					"telephone": "2312321"
				}],
				[{
					"language": "en",
					"cityName": "Lost Ciy",
					"openingTime": "from 9am to 7pm",
					"telephone": "111 222 666"
				}, {
					"language": "es",
					"cityName": "Ciudad Perdida",
					"openingTime": "from 9am to 7pm",
					"telephone": "2312321"
				}]
			],
			"city": "Lost City",
			"published": true
		}]
	}, {
		"countryName": "United Kingdom",
		"countryCode": "GB",
		"cities": [{
			"properties": [
				[{
					"language": "en",
					"cityName": "London",
					"openingTime": "From 9am to 6pm",
					"telephone": "99 555 666 "
				}, {
					"language": "es",
					"cityName": "Londres",
					"openingTime": "From 9am to 6pm",
					"telephone": "99 555 666 "
				}],
				[{
					"language": "en",
					"cityName": "London",
					"openingTime": "From 9am to 6pm",
					"telephone": "99 444 666 "
				}, {
					"language": "es",
					"cityName": "Londres",
					"openingTime": "From 9am to 6pm",
					"telephone": "99 444 666 "
				}]
			],
			"city": "London",
			"published": true
		}]
	}]
);

db.locations.insert(
	[{
		"_id": "5b137c0e207b3d6c5f346d55",
		"countryName": "United Kingdom",
		"countryCode": "GB",
		"cities": [{
			"properties": [
				[{
					"language": "es",
					"cityName": "Londres",
					"openingTime": "from 9am to 6pm ",
					"telephone": "1111 2222",
					"address": "Direccion",
					"latitude": 25.3260488,
					"longitude": 55.508112
				}, {
					"language": "en",
					"cityName": "London",
					"openingTime": "from 9am to 6pm ",
					"telephone": "1111 2222",
					"address": "Address1",
					"latitude": 25.3260488,
					"longitude": 55.508112
				}]
			],
			"city": "London",
			"published": true
		}, {
			"properties": [
				[{
					"language": "en",
					"cityName": "Liverpool",
					"openingTime": "from 6am to 9pm",
					"telephone": "3213123",
					"address": "Adress2",
					"latitude": 25.3260488,
					"longitude": 55.508112
				}]
			],
			"city": "Liverpool",
			"published": true
		}]
	}, {
		"_id": "5b14c8c5207b3d6c5f346d62",
		"countryName": "United Arab Emirates",
		"countryCode": "AE",
		"cities": [{
			"properties": [
				[{
					"language": "en",
					"cityName": "Dubai",
					"openingTime": "from 9am to 7pm",
					"telephone": "111 222 55",
					"address": "Address2",
					"latitude": 25.3260488,
					"longitude": 55.508112
				}, {
					"language": "es",
					"cityName": "Dubai",
					"openingTime": "from 9am to 7pm",
					"telephone": "111 222 55",
					"address": "Direccion",
					"latitude": 25.3260488,
					"longitude": 55.508112
				}]
			],
			"city": "Dubai",
			"published": false
		}]
	}]
);

db.contactus.insert(
	[{
		"_id": "5ae59ec84847bb6ee4887f7a",
		"email": "example@airarabia.com",
		"published": true
	}]

);


db.createCollection("files");
