const ObjectId = require('mongodb').ObjectID;


let CriteriaBuilder =  function () {
    
    this.searchCriterion = {};
    this.propertyCriteria = {};

    this.build =  () => {
        return this.searchCriterion;
    }

    this.setPropertyPublished = (published) => {
        if(!this.searchCriterion.$and) this.searchCriterion.$and =[];
        const valuePublished = published == 'true'
        this.searchCriterion.$and=[{ published :valuePublished }];
    }   

    this.setId = (_id) => {

        this.searchCriterion =  {_id };
    }

    this.setCityPublished = (published) => {
        if (!this.searchCriterion.$and) this.searchCriterion.$and = [{ 
            cities: { $elemMatch: {  published:this.propertyCriteria.published } } }];
    }
    

    this.setEmail = function(email){
        if(!this.searchCriterion.$and) this.searchCriterion.$and =[];
        this.searchCriterion.$and.push({ email: {$regex : ".*"+email+".*"}}) ;
    }

    this.setFile = function(filename){
        this.searchCriterion = { filename };
    }
    this.setCountryAnd =  (countries) => {
        if (!this.searchCriterion.$or) this.searchCriterion.$or = [];
        let listCountries=[];
        listCountries =  listCountries.concat(countries);
        listCountries.forEach((country)=> {
            if(this.propertyCriteria.published != undefined){
            this.searchCriterion.$or.push({ countryCode : country.toUpperCase(), cities: { $elemMatch: {  published:this.propertyCriteria.published } } });
            }
            else{
                this.searchCriterion.$or.push({ countryCode : country.toUpperCase() });
            }
        });
        return this;
    };
    this.setLanguageAnd = (languages) => {
        if (!this.searchCriterion.$and) this.searchCriterion.$and = [];
        let listLanguages=[];
        listLanguages =  listLanguages.concat(languages);
        listLanguages.forEach((language)=> {
            if(this.propertyCriteria.published  != undefined){
                this.searchCriterion.$and.push(
                    {cities: { $elemMatch: {  published:this.propertyCriteria.published,  properties: { $elemMatch: { language: language}}}}});
            }
            else{
                this.searchCriterion.$and.push(
                    { cities: {  $elemMatch: { properties: { $elemMatch: { $elemMatch: { language: language}}}}}});
            }
        });
        return this;
    };

    this.setCountryOr = function (countries) {
        if (!this.searchCriterion.$or) this.searchCriterion.$or = [];
        let listCountries=[];
        listCountries =  listCountries.concat(countries);
        listCountries.forEach((country)=> {
                this.searchCriterion.$or.push({ countryCode : country.toUpperCase() });
        });
        return this;
    };

    this.setLanguageOr = function (languages) {
        if (!this.searchCriterion.$or) this.searchCriterion.$or = [];
        let listLanguages=[];
        listLanguages =  listLanguages.concat(languages);
        listLanguages.forEach((language)=> {
                this.searchCriterion.$or.push({ campaigns: {  $elemMatch: { properties: { $elemMatch: { language: language  } } } } });
        });
        return this;
    };


    this.setDefCampOr = function () {
        if (!this.searchCriterion.$or) this.searchCriterion.$or = [{ defaultCamp: true  }];
        return this;
    };


    this.setCountrLanguageCamp = function(countries, languages){
        if (!this.searchCriterion.$or) this.searchCriterion.$or=[];
        let listLanguages=[];
        let listCountries=[];
        listLanguages =  listLanguages.concat(languages);
        listCountries =  listCountries.concat(countries);
        listCountries.forEach((country)=>{
            listLanguages.forEach((language)=>{
                this.searchCriterion.$or.push({ countryCode : country.toUpperCase(), campaigns: { $elemMatch: { properties: { $elemMatch: {  language: language}}}}})
            })
        })
        return this;
    };


    this.setCountryLanguageLoc = (countries, languages) => {
        if (!this.searchCriterion.$or) this.searchCriterion.$or=[];
        let listLanguages=[];
        let listCountries=[];
        listLanguages =  listLanguages.concat(languages);
        listCountries =  listCountries.concat(countries);
        listCountries.forEach((country)=>{
            listLanguages.forEach((language)=>{
                $elemMatch:
                this.searchCriterion.$or.push(
                    {  countryCode : country.toUpperCase(), cities: { $elemMatch: { properties: { $elemMatch: { $elemMatch:  { language: language }}}}}});
            })
        })
        return this;
    };

    this.setEmailAndPublished = (email, published) => {
        this.searchCriterion = { email: {$regex : ".*"+email+".*"}, published: published=='true' };
    }

    this.setUsername = (username) =>{
        this.searchCriterion = { username: username }
    }

    this.setUserAndPassword = (username, password) =>{
        this.searchCriterion = { username, password }
    }

    this.filterByCountryLanguageCamp= (country, language, defCamp) => {
        
       if(country && language) this.setCountrLanguageCamp(country, language);
        else{
            if (country) this.setCountryOr(country);
            if (language) this.setLanguageOr(language);
            if (defCamp)  this.setDefCampOr();
        }
   }

    this.filterByCountryLanguageLoc = (country, language) => {
         
        if(country && language) this.setCountryLanguageLoc(country, language);
         else{
             if (country) this.setCountryAnd(country);
             if (language) this.setLanguageAnd(language);
         }
    }

    this.filterByEmailStatus = (email, published) =>{
        if(email && published) this.setEmailAndPublished(email, published);
        else{
            if (email) this.setEmail(email);
            if (published) this.setPropertyPublished(published);
        }
    }

    this.filterByUserPass = (username, password) => {
        if(username && password) this.setUserAndPassword(username,password);
        else {
           if(username) this.setUsername(username);
        } 
        
    }
    
    this.filters = ({country, language, name, published, filename, username, newHashPassword}, dbName) => {
        
        switch(dbName){
            case 'users':
                this.filterByUserPass(username, newHashPassword);
                break;
            case 'campaigns':
                this.filterByCountryLanguageCamp(country,language);
                break;
            case 'contactus':
                this.filterByEmailStatus(name, published);
                 break;
            case 'files':
                this.setFile(filename);
                 break;
            default:
                this.filterByCountryLanguageLoc(country,language);
            return
        }

    }

}

module.exports = CriteriaBuilder;