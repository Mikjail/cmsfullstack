import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Location } from '@angular/common';

//CMS MODULES
import {CountryLangBasedModule } from './components/main/countryLangBased/countrylang-based.module'
import { UtilsModule } from './components/utils/utils.module';

//CMS Components
import { Constants } from './models';
import {
  AuthenticationService,
  UserService,
  AlertService,
  GenericService,
  PagerService,
  LoaderService
} from './services';

import { 
  SidebarComponent,
  HeaderComponent,
  EmailsettingComponent,
  HomeComponent,
  ImageComponent,
  OfficeLocationComponent,
  CallcenterComponent,
  LoginComponent,
  RegisterComponent
} from './components';

import { ProfileComponent } from './components/profile/profile.component';
import { InfoComponent } from './components/profile/info/info.component';
import { EditPasswordComponent } from './components/profile/edit-password/edit-password.component';


import { AlertComponent } from './directives';
import { AuthGuard } from './guards';
import { routing } from './app.routing';
import { AppComponent } from './app.component';

//Third Party components
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { MainModule } from './components/main/main.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    AlertComponent,
    ProfileComponent,
    InfoComponent,
    EditPasswordComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  imports: [
    BrowserModule,
    HttpModule,
    CountryLangBasedModule,
    UtilsModule,
    MainModule,
    LazyLoadImageModule,
    CountryLangBasedModule,
    routing
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    GenericService,
    UserService,
    AlertService,
    LoaderService,
    Location,
    Constants
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
