webpackJsonp(["main"],{

/***/ "./src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_gendir lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-loader></app-loader>\n<app-header *ngIf=\"isLoggedIn()\"></app-header>\n\n<div class=\"app\">\n\n    <app-sidebar *ngIf=\"isLoggedIn()\" [init]=\"isOpen\"></app-sidebar>\n\n    <div class=\"page\" [class.pageFixed]=\"fixed\" [ngClass]=\"{'mainExpanded':!isOpen}\">\n        <router-outlet></router-outlet>\n    </div>\n\n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/***/ (function(module, exports) {

module.exports = ".header h5 {\n  padding-top: 2vh; }\n\n.app {\n  height: 93%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1;\n          flex: 1 1; }\n\n.page {\n  width: 96%;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-flow: column wrap;\n          flex-flow: column wrap;\n  -webkit-transition: 0.4s;\n  transition: 0.4s; }\n\n.top-nav {\n  background-color: #7dc5ad;\n  height: 50px;\n  border-bottom: 1px solid transparent;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  color: white; }\n\n.top-nav-item {\n  padding: 10px;\n  color: white; }\n\n.top-nav-item:hover {\n    background-color: #493087;\n    text-decoration: none;\n    color: white; }\n\n.mainExpanded {\n  width: 81.0%;\n  -webkit-transition: 0.4s;\n  transition: 0.4s; }\n\n.fixed {\n  position: fixed;\n  top: 0;\n  padding-top: 60px;\n  overflow-y: hidden; }\n\n.fixed .menuButton {\n    display: none; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
        this.isOpen = { isOpen: "" };
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent.prototype.isLoggedIn = function () {
        return localStorage.getItem('currentUser');
    };
    return AppComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('init'),
    __metadata("design:type", Object)
], AppComponent.prototype, "isOpen", void 0);
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("./src/app/app.component.html"),
        styles: [__webpack_require__("./src/app/app.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services__ = __webpack_require__("./src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components__ = __webpack_require__("./src/app/components/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__directives__ = __webpack_require__("./src/app/directives/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__guards__ = __webpack_require__("./src/app/guards/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_routing__ = __webpack_require__("./src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng_lazyload_image__ = __webpack_require__("./node_modules/ng-lazyload-image/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng_lazyload_image___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_ng_lazyload_image__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng2_select_compat__ = __webpack_require__("./node_modules/ng2-select-compat/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng2_select_compat___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_ng2_select_compat__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ngx_bootstrap__ = __webpack_require__("./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_main_countryLangBased_panel_creation_panel_creation_component__ = __webpack_require__("./src/app/components/main/countryLangBased/panel-creation/panel-creation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_utils_modal_card_view_modal_card_view_component__ = __webpack_require__("./src/app/components/utils/modal-card-view/modal-card-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_main_campaign_campaign_component__ = __webpack_require__("./src/app/components/main/campaign/campaign.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





//CMS Components







//Third Party components

 // Input selectors
 // Date Picker




var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["i" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["f" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["g" /* ImageComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["n" /* RegisterComponent */],
            __WEBPACK_IMPORTED_MODULE_8__directives__["a" /* AlertComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["h" /* LoaderComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["l" /* OfficeLocationComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["m" /* PaginationComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["o" /* SearchFilterComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["c" /* EmailsettingComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["d" /* GeolocationComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["a" /* CallcenterComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["k" /* ModuleListingComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["j" /* ModuleEditingComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["p" /* SidebarComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components__["e" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_16__components_main_countryLangBased_panel_creation_panel_creation_component__["a" /* PanelCreationComponent */],
            __WEBPACK_IMPORTED_MODULE_17__components_utils_modal_card_view_modal_card_view_component__["a" /* ModalCardViewComponent */],
            __WEBPACK_IMPORTED_MODULE_18__components_main_campaign_campaign_component__["a" /* CampaignComponent */]
        ],
        schemas: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_13_ng2_select_compat__["SelectModule"],
            __WEBPACK_IMPORTED_MODULE_14_ngx_bootstrap__["a" /* BsDatepickerModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_12_ng_lazyload_image__["LazyLoadImageModule"],
            __WEBPACK_IMPORTED_MODULE_15__agm_core__["a" /* AgmCoreModule */].forRoot({
                apiKey: 'AIzaSyCbqgtHD6CKZ2loD9w74EFIxLn9n5uWsE8',
                libraries: ["places"]
            }),
            __WEBPACK_IMPORTED_MODULE_10__app_routing__["a" /* routing */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_9__guards__["a" /* AuthGuard */],
            __WEBPACK_IMPORTED_MODULE_6__services__["b" /* AuthenticationService */],
            __WEBPACK_IMPORTED_MODULE_6__services__["c" /* GenericService */],
            __WEBPACK_IMPORTED_MODULE_6__services__["f" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_6__services__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_6__services__["e" /* PagerService */],
            __WEBPACK_IMPORTED_MODULE_6__services__["d" /* LoaderService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_5__models__["Constants"]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "./src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__guards__ = __webpack_require__("./src/app/guards/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components__ = __webpack_require__("./src/app/components/index.ts");



//canActivate: [AuthGuard], data: { roles: ['admin', 'user']} will be added once we have backend
var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__components__["f" /* HomeComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_1__guards__["a" /* AuthGuard */]] },
    // {path: 'campaignList', component: CampaignListComponent, canActivate: [AuthGuard]},
    { path: 'image', component: __WEBPACK_IMPORTED_MODULE_2__components__["g" /* ImageComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_1__guards__["a" /* AuthGuard */]] },
    { path: 'campaign', component: __WEBPACK_IMPORTED_MODULE_2__components__["b" /* CampaignComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_1__guards__["a" /* AuthGuard */]] },
    { path: 'officeLocation', component: __WEBPACK_IMPORTED_MODULE_2__components__["l" /* OfficeLocationComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_1__guards__["a" /* AuthGuard */]] },
    { path: 'callcenter', component: __WEBPACK_IMPORTED_MODULE_2__components__["a" /* CallcenterComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_1__guards__["a" /* AuthGuard */]] },
    { path: 'emailUs', component: __WEBPACK_IMPORTED_MODULE_2__components__["c" /* EmailsettingComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_1__guards__["a" /* AuthGuard */]] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_2__components__["n" /* RegisterComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_2__components__["i" /* LoginComponent */] },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid hidden-xs\">\n\n    <div class=\"row menuItems\">\n\n        <!-- Campaign  -->\n        <div class=\"col-sm-1 col-sm-offset-2\">\n                <a href=\"#\" routerLink=\"../campaign\" [queryParams]=\"{}\" >\n                    <i class=\"fa fa-file-text-o fa-5x\" aria-hidden=\"true\"></i>\n                </a>\n        </div>\n        <div class=\"col-sm-2\">\n                <a href=\"#\" routerLink=\"../campaign\" [queryParams]=\"{}\" >\n                    <h4>Campaigns</h4>\n                </a>\n        </div>\n\n        <!-- Office Location -->\n        <div class=\"col-sm-1 col-sm-offset-2\">\n            <a href=\"#\" routerLink=\"../officeLocation\" [queryParams]=\"{}\" >\n                <i class=\"fa fa-map-marker fa-5x\" aria-hidden=\"true\"></i>\n                </a>\n        </div>\n        <div class=\"col-sm-2\">\n            <a href=\"#\" routerLink=\"../officeLocation\" [queryParams]=\"{}\" >\n                <h4>Office Location</h4>\n            </a>\n        </div>\n\n    </div>\n\n\n\n\n    <div class=\"row menuItems\">\n\n\n        <!-- Labels -->\n        <div class=\"col-sm-1 col-sm-offset-2\">\n                <a href=\"#\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Under Construction!\" routerLink=\"/\" routerLink=\"/\" [queryParams]=\"{}\" >\n                    <i class=\"fa fa-tag fa-5x\" aria-hidden=\"true\"></i>\n                </a>\n        </div>\n        <div class=\"col-sm-2\">\n            \n                <a href=\"#\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Under Construction!\" routerLink=\"/\" [queryParams]=\"{}\" >\n                    <h4>Labels (Under Construction)</h4>\n                </a>\n        </div>\n\n         <!-- Location -->\n         <div class=\"col-sm-1 col-sm-offset-2\">\n                <a href=\"#\" routerLink=\"../callcenter\" [queryParams]=\"{}\" >\n                    <i class=\"fa fa-phone fa-5x\" aria-hidden=\"true\"></i>\n                </a>\n        </div>\n        <div class=\"col-sm-2\">\n                <a href=\"#\" routerLink=\"../callcenter\" [queryParams]=\"{}\">\n                    <h4>Call Center</h4>\n                </a>\n        </div>\n\n    </div>\n    \n    <div class=\"row menuItems\">\n        \n                <!-- Email Us -->\n                <div class=\"col-sm-1 col-sm-offset-2\">\n                        <a href=\"#\" routerLink=\"../emailUs\" [queryParams]=\"{}\" >\n                            <i class=\"fa fa-envelope-o fa-5x\" aria-hidden=\"true\"></i>\n                        </a>\n                </div>\n                <div class=\"col-sm-2\">\n                        <a href=\"#\" routerLink=\"../emailUs\" [queryParams]=\"{}\" >\n                            <h4>Email Us</h4>\n                        </a>\n                </div>\n        \n        \n                <!-- Images -->\n                <div class=\"col-sm-1 col-sm-offset-2\">\n                    <a href=\"#\" routerLink=\"../image\" [queryParams]=\"{}\" >\n                    <i class=\"fa fa-picture-o fa-5x\" aria-hidden=\"true\"></i>\n                </a>\n                </div>\n                <div class=\"col-sm-2\">\n                    <a href=\"#\" routerLink=\"../image\" [queryParams]=\"{}\" >\n                        <h4>Bulk Upload Images</h4>\n                    </a>\n                </div>\n            \n            </div>\n\n</div>\n\n<app-modal-card-view></app-modal-card-view>"

/***/ }),

/***/ "./src/app/components/home/home.component.scss":
/***/ (function(module, exports) {

module.exports = ".menuItems {\n  margin-top: 6vh;\n  width: 100%; }\n\na {\n  text-decoration: none;\n  color: black; }\n\nh1, h2, h3, h4, h5 {\n  color: black; }\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = (function () {
    // constructor(private userService : UserService) {}
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
        // let userData:any;
        //   userData = JSON.parse(localStorage.getItem('currentUser'));
        //   this.user =  userData.datosDB;
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-home',
        template: __webpack_require__("./src/app/components/home/home.component.html"),
        styles: [__webpack_require__("./src/app/components/home/home.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], HomeComponent);

//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "./src/app/components/home/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_component__ = __webpack_require__("./src/app/components/home/home.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__home_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home__ = __webpack_require__("./src/app/components/home/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_0__home__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__login__ = __webpack_require__("./src/app/components/login/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_1__login__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register__ = __webpack_require__("./src/app/components/register/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_2__register__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_emailsetting__ = __webpack_require__("./src/app/components/main/emailsetting/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_3__main_emailsetting__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__main_image__ = __webpack_require__("./src/app/components/main/image/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_4__main_image__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__main_office_location__ = __webpack_require__("./src/app/components/main/office-location/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_5__main_office_location__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_callcenter__ = __webpack_require__("./src/app/components/main/callcenter/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_6__main_callcenter__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__main_campaign__ = __webpack_require__("./src/app/components/main/campaign/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_7__main_campaign__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__main_countryLangBased__ = __webpack_require__("./src/app/components/main/countryLangBased/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_8__main_countryLangBased__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_8__main_countryLangBased__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__utils_nav__ = __webpack_require__("./src/app/components/utils/nav/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_9__utils_nav__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "p", function() { return __WEBPACK_IMPORTED_MODULE_9__utils_nav__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__utils_search_filter__ = __webpack_require__("./src/app/components/utils/search-filter/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "o", function() { return __WEBPACK_IMPORTED_MODULE_10__utils_search_filter__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__utils_geolocation__ = __webpack_require__("./src/app/components/utils/geolocation/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_11__utils_geolocation__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__utils_pagination__ = __webpack_require__("./src/app/components/utils/pagination/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_12__utils_pagination__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__utils_loader__ = __webpack_require__("./src/app/components/utils/loader/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_13__utils_loader__["a"]; });



// export * from './main/campaign-edit';
// export * from './main/campaign-list';











//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/login/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_component__ = __webpack_require__("./src/app/components/login/login.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__login_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row loginTemplate\">\n        <div class=\"col-md-6 col-sm-6 col-xs-12\">\n            <div class=\"col-sm-12\">\n                    <h2>Login</h2>\n                    <form name=\"form\" (ngSubmit)=\"f.valid && login()\" #f=\"ngForm\" novalidate>\n                        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !username.valid }\">\n                            <label for=\"username\">Username</label>\n                            <input type=\"text\" class=\"form-control\" name=\"username\" [(ngModel)]=\"userForm.username\" #username=\"ngModel\" required/>\n                            <div *ngIf=\"f.submitted && !username.valid\" class=\"help-block\">Username is required</div>\n                        </div>\n                        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\n                            <label for=\"password\">Password</label>\n                            <input type=\"password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"userForm.password\" #password=\"ngModel\" required/>\n                            <div *ngIf=\"f.submitted && !password.valid\" class=\"help-block\">Password is required</div>\n                        </div>\n                        <div class=\"form-group\">\n                            <button [disabled]=\"!f.valid || loading\" class=\"btn btn-primaryTheme\">Login</button>\n                            <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\"\n                            />\n                            <!-- <a class=\"col-2\" [routerLink]=\"['/register']\"    href=\"javascript:void(0)\">register</a>    -->\n                        </div>\n                    <alert></alert>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/login/login.component.scss":
/***/ (function(module, exports) {

module.exports = ".loginTemplate {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -ms-flex-line-pack: center;\n      align-content: center;\n  margin-top: 20vh; }\n\n.container {\n  height: 100%; }\n"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__("./src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = (function () {
    function LoginComponent(router, authenticationService, alertService) {
        this.router = router;
        this.authenticationService = authenticationService;
        this.alertService = alertService;
        this.userForm = {};
        this.loading = false;
        this.user = new __WEBPACK_IMPORTED_MODULE_2__models__["User"]();
    }
    LoginComponent.prototype.ngOnInit = function () {
        // get return url from route parameters or default to '/'
        this.returnUrl = '/';
        if (this.isSignedIn()) {
            this.router.navigateByUrl(this.returnUrl);
        }
        // reset login status
        this.authenticationService.logout();
    };
    LoginComponent.prototype.isSignedIn = function () {
        var user = localStorage.getItem('currentUser');
        return user != null && user != undefined && user != "";
    };
    LoginComponent.prototype.login = function (value) {
        var _this = this;
        this.loading = true;
        this.user.username = this.userForm.username;
        this.user.password = this.userForm.password;
        this.authenticationService.login(this.user).subscribe(function (response) {
            _this.user.password = "";
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(_this.user));
            localStorage.setItem('token', response.token);
            _this.router.navigateByUrl(_this.returnUrl);
            _this.loading = false;
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__("./src/app/components/login/login.component.html"),
        styles: [__webpack_require__("./src/app/components/login/login.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services__["b" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["b" /* AuthenticationService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services__["a" /* AlertService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["a" /* AlertService */]) === "function" && _c || Object])
], LoginComponent);

var _a, _b, _c;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "./src/app/components/main/callcenter/callcenter.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\" *ngIf=\"!callCenterToEdit.edition\">\n    <div class=\"col-sm-1 back-btn\" style=\"text-align:center;\">\n      <a href=\"#\" routerLink=\"/\">Back</a>\n    </div>\n    <div class=\"col-sm-1\">\n      <i class=\"fa fa-phone fa-4x\" aria-hidden=\"true\"></i>\n    </div>\n    <div class=\"col-sm-4\">\n      <h3>Call Center</h3>\n    </div>\n  </div>\n  <div class=\"row\" *ngIf=\"callCenterToEdit.edition\">\n    <div class=\"col-sm-1 back-btn\" style=\"text-align:center;\">\n      <a href=\"#\" (click)=\"callCenterToEdit.edition = false\">Back</a>\n    </div>\n    <div class=\"col-sm-1\">\n      <i class=\"fa fa-phone fa-4x\" aria-hidden=\"true\"></i>\n    </div>\n    <div class=\"col-sm-6\">\n      <h3>Call Center > Edit > {{callCenterToEdit.entityLocation.countryName}} </h3>\n    </div>\n  </div>\n</div>\n\n\n<ng-container *ngIf=\"!callCenterToEdit.edition\">\n\n  <app-search-filter [searchInput]=\"searchFilter\" (changeView)=\"getFromSearch($event);\">\n  </app-search-filter>\n\n  <div class=\"container-fluid borderFluid\">\n\n\n    <app-panel-creation \n      [countryField] = \"true\"\n      [countryList]=\"searchFilter.countryFromServer\" \n      [languageField] =\"true\"\n      [languageList] = \"searchFilter.languageArray\"\n      [textField] = \"true\"\n      [textLabel] =\"'City'\"\n      [locationInput]=\"entityLocation\"\n      [buttonName]=\"buttonNewCountry\"\n      (addNewCountry)=\"addNewCountry($event)\">\n    </app-panel-creation>\n\n    <app-module-listing \n      [locationInput]=\"entityLocation\" \n      (valueToEdit)=\"editLocation($event)\">\n    </app-module-listing> \n\n  </div>\n\n\n</ng-container>\n\n\n<app-module-editing \n[init]=\"callCenterToEdit\"\n*ngIf=\"callCenterToEdit.edition\"\n[nameModule]=\"'Call Center'\"\n(newListValue)=\"getFromSearch($event);\">\n</app-module-editing>"

/***/ }),

/***/ "./src/app/components/main/callcenter/callcenter.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/main/callcenter/callcenter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallcenterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("./src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_search_filter__ = __webpack_require__("./src/app/components/utils/search-filter/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__countryLangBased_module_listing__ = __webpack_require__("./src/app/components/main/countryLangBased/module-listing/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CallcenterComponent = (function () {
    function CallcenterComponent(genServices, constants) {
        this.genServices = genServices;
        this.constants = constants;
        this.searchFilter = {
            apiRest: '',
            availableSearch: {
                searchStatus: false,
                searchName: false,
                searchCountry: true,
                searchLanguage: true
            },
            searchStatus: true,
            countryFromServer: [],
            languageArray: []
        };
        /**
         * Filter Form.
         *
         * @type {Array<string>}
         * @memberOf CallcenterComponent
         */
        this.resultsFound = false;
        /**
         * To enable Callcenter edit directive.
         *
         * @memberof CallcenterComponent
         */
        this.callCenterToEdit = {
            cityIndexSelected: 0,
            edition: false,
            entityLocation: new __WEBPACK_IMPORTED_MODULE_1__models__["CallCenter"](),
            module: 'CallCenter',
            apiService: ''
        };
        /**
       * Module Listing/ Panel Creation Directive
       *
       * @memberof OfficeLocationComponent
       */
        this.entityLocation = {
            module: new __WEBPACK_IMPORTED_MODULE_1__models__["CallCenter"],
            locationList: [],
            apiService: '',
            apiServiceMulti: '',
            resultsFound: false,
            editEntity: false,
            countrySelectionSetting: false
        };
        /**
         * Pagination Directive
         *
         * @private
         * @type {any[]}
         * @memberOf CallcenterComponent
         */
        this.paginationList = {
            entityList: [],
            pagedItems: [],
            pager: {},
            setPage: {},
            customNumberPages: 8
        };
        this.existingCountLang = [];
        this.buttonNewCountry = "Settings";
        this.apiRest = constants.IApiRest;
        this.existingCountries = [];
        this.searchFilter.apiRest = this.apiRest.API_CALLCENTER;
        this.entityLocation.apiService = this.apiRest.API_CALLCENTER;
        this.callCenterToEdit.apiService = this.apiRest.API_CALLCENTER;
        this.entityLocation.apiServiceMulti = this.apiRest.API_CALLCENTER_MULTI;
        this.entityLocation.countrySelectionSetting = false;
        this.callCenters = [];
    }
    /**
    * Select an Office and set @param this.callCenterToEdit.entityLocation to edit it.
    *
    * @param {any} officeLocation
    *
    * @memberOf OfficeLocationComponent
    */
    CallcenterComponent.prototype.editLocation = function (_a) {
        var newLocationToEdit = _a.newLocationToEdit, cityIndex = _a.cityIndex;
        console.log(newLocationToEdit);
        this.callCenterToEdit.edition = true;
        this.callCenterToEdit.cityIndexSelected = cityIndex;
        this.callCenterToEdit.entityLocation = newLocationToEdit;
        this.callCenterToEdit.apiService = this.apiRest.API_CALLCENTER;
        this.callCenterToEdit.module = 'CallCenter';
    };
    CallcenterComponent.prototype.getFromSearch = function (callcentersFound) {
        this.callCenterToEdit.edition = false;
        if (callcentersFound.value.length > 0) {
            this.callCenters = callcentersFound.value.sort(this.compareByCountry);
            this.EntityComponent.refreshLocations(this.callCenters);
            this.resultsFound = true;
        }
        else {
            this.resultsFound = false;
        }
    };
    CallcenterComponent.prototype.ngOnInit = function () {
    };
    /**
    * Helper to sort an Array of Campaigns
    *
    * @param {CallCenter} countryA
    * @param {CallCenter} countryB
    * @returns {number}
    *
    * @memberOf CampaignListComponent
    */
    CallcenterComponent.prototype.compareByCountry = function (countryA, countryB) {
        if (countryA.countryName < countryB.countryName) {
            return -1;
        }
        if (countryA.countryName > countryB.countryName) {
            return 1;
        }
        return 0;
    };
    /**
     * Add a new country with all it's default properties.
     *
     *
     * @memberOf OfficeLocationComponent
     */
    CallcenterComponent.prototype.addNewCountry = function (_a) {
        var newCountryToEdit = _a.newCountryToEdit, newLanguageToEdit = _a.newLanguageToEdit, newTextToEdit = _a.newTextToEdit, newLocationToEdit = _a.newLocationToEdit;
        console.log("paso por aca");
        var countrySelected = this.searchFilter.countryFromServer.find(function (country) { return country.countryName == newCountryToEdit; });
        if (!newLocationToEdit) {
            newLocationToEdit = new __WEBPACK_IMPORTED_MODULE_1__models__["CallCenter"](countrySelected.countryName, countrySelected.countryCode);
        }
        newLocationToEdit.cities.push(new __WEBPACK_IMPORTED_MODULE_1__models__["CityModel"](newTextToEdit));
        var cityIndex = newLocationToEdit.cities.length - 1;
        var properties = [];
        properties[0] = new __WEBPACK_IMPORTED_MODULE_1__models__["CityProperty"](newLanguageToEdit, newTextToEdit);
        newLocationToEdit.cities[cityIndex].properties[0] = properties;
        this.editLocation({ newLocationToEdit: newLocationToEdit, cityIndex: cityIndex });
        this.entityLocation.countrySelectionSetting = false;
    };
    return CallcenterComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('searchInput'),
    __metadata("design:type", Object)
], CallcenterComponent.prototype, "searchFilter", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4__countryLangBased_module_listing__["a" /* ModuleListingComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__countryLangBased_module_listing__["a" /* ModuleListingComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__countryLangBased_module_listing__["a" /* ModuleListingComponent */]) === "function" && _a || Object)
], CallcenterComponent.prototype, "EntityComponent", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3__utils_search_filter__["a" /* SearchFilterComponent */]),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__utils_search_filter__["a" /* SearchFilterComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__utils_search_filter__["a" /* SearchFilterComponent */]) === "function" && _b || Object)
], CallcenterComponent.prototype, "searchFilterComponent", void 0);
CallcenterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-callcenter',
        template: __webpack_require__("./src/app/components/main/callcenter/callcenter.component.html"),
        styles: [__webpack_require__("./src/app/components/main/callcenter/callcenter.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* GenericService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* GenericService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__models__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models__["Constants"]) === "function" && _d || Object])
], CallcenterComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=callcenter.component.js.map

/***/ }),

/***/ "./src/app/components/main/callcenter/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__callcenter_component__ = __webpack_require__("./src/app/components/main/callcenter/callcenter.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__callcenter_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/campaign/campaign.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\" *ngIf=\"!campaignToEdit.edition\">\n    <div class=\"col-sm-1 back-btn\" style=\"text-align:center;\">\n      <a href=\"#\" routerLink=\"/\">Back</a>\n    </div>\n    <div class=\"col-sm-1\">\n      <i class=\"fa fa-file-text-o fa-4x\" aria-hidden=\"true\"></i>\n    </div>\n    <div class=\"col-sm-4\">\n      <h3>Campaign</h3>\n    </div>\n  </div>\n  <div class=\"row\" *ngIf=\"campaignToEdit.edition\">\n    <div class=\"col-sm-1 back-btn\" style=\"text-align:center;\">\n      <a href=\"#\" (click)=\"campaignToEdit.edition = false\">Back</a>\n    </div>\n    <div class=\"col-sm-1\">\n      <i class=\"fa fa-file-text-o fa-4x\" aria-hidden=\"true\"></i>\n    </div>\n    <div class=\"col-sm-6\">\n      <h3>Campaign > Edit > {{campaignToEdit.entityLocation.countryName}} </h3>\n    </div>\n  </div>\n</div>\n\n<ng-container *ngIf=\"!campaignToEdit.edition\">\n\n  <app-search-filter [searchInput]=\"searchFilter\" (changeView)=\"getFromSearch($event);\">\n  </app-search-filter>\n\n  <div class=\"container-fluid borderFluid\">\n\n    <app-panel-creation \n      [countryField] = \"true\"\n      [countryList]=\"searchFilter.countryFromServer\" \n      [languageField] =\"true\"\n      [languageList] = \"searchFilter.languageArray\"\n      [searchInput]=\"searchFilter\" \n      [locationInput]=\"entityLocation\"\n      [textField] = \"true\"\n      [textLabel] =\"'Campaign'\"\n      [nameModule]= \"'Campaigns'\"\n      (addNewCountry)=\"addNewCountry($event)\">\n    </app-panel-creation>\n\n    <app-module-listing \n      [locationInput]=\"entityLocation\" \n      (valueToEdit)=\"editLocation($event)\"\n      (parseDataCamp)=\"parseDataCamp($event)\"\n      >\n    </app-module-listing> \n\n  </div>\n\n\n</ng-container>\n\n\n<app-module-editing \n[init]=\"campaignToEdit\" \n*ngIf=\"campaignToEdit.edition\" \n[nameModule]=\"'Campaign'\"\n(newListValue)=\"getFromSearch($event);\"\n[newButton]=\"'Campaign'\">\n</app-module-editing>"

/***/ }),

/***/ "./src/app/components/main/campaign/campaign.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/main/campaign/campaign.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CampaignComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__countryLangBased_module_listing__ = __webpack_require__("./src/app/components/main/countryLangBased/module-listing/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__("./src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_search_filter__ = __webpack_require__("./src/app/components/utils/search-filter/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CampaignComponent = (function () {
    function CampaignComponent(genServices, constants) {
        this.genServices = genServices;
        this.constants = constants;
        this.searchFilter = {
            apiRest: '',
            availableSearch: {
                searchStatus: false,
                searchName: false,
                searchCountry: true,
                searchLanguage: true
            },
            countryFromServer: [],
            languageArray: []
        };
        /**
        * Filter Form.
        *
        * @type {Array<string>}
        * @memberOf CallcenterComponent
        */
        this.resultsFound = false;
        /**
        * To enable Campaign edit directive.
         *
         * @memberof CallcenterComponent
         */
        this.campaignToEdit = {
            edition: false,
            cityIndexSelected: 0,
            entityLocation: new __WEBPACK_IMPORTED_MODULE_1__models__["Campaign"](),
            module: 'Campaign',
            apiService: ''
        };
        /**
      * Module Listing/ Panel Creation Directive
      *
      * @memberof OfficeLocationComponent
      */
        this.entityLocation = {
            module: 'Campaign',
            locationList: [],
            locationParsed: [],
            apiService: '',
            apiServiceMulti: '',
            resultsFound: false,
            editEntity: false,
            countrySelectionSetting: false
        };
        /**
         * Pagination Directive
         *
         * @private
         * @type {any[]}
         * @memberOf CallcenterComponent
         */
        this.paginationList = {
            entityList: [],
            pagedItems: [],
            pager: {},
            setPage: {},
            customNumberPages: 8
        };
        this.existingCountLang = [];
        this.buttonNewCountry = "Settings";
        this.apiRest = constants.IApiRest;
        this.existingCountries = [];
        this.searchFilter.apiRest = this.apiRest.API_CAMPAIGN;
        this.entityLocation.apiService = this.apiRest.API_CAMPAIGN;
        this.campaignToEdit.apiService = this.apiRest.API_CAMPAIGN;
        this.campaigns = [];
        this.entityLocation.countrySelectionSetting = false;
    }
    /**
    * Select an Office and set @param this.campaignToEdit to edit it.
    *
    * @param {any} officeLocation
    *
    * @memberOf OfficeLocationComponent
    */
    CampaignComponent.prototype.editLocation = function (_a) {
        var newLocationToEdit = _a.newLocationToEdit, cityIndex = _a.cityIndex;
        console.log("va a editar");
        this.campaignToEdit.edition = true;
        this.campaignToEdit.cityIndexSelected = cityIndex;
        this.campaignToEdit.entityLocation = newLocationToEdit;
        this.campaignToEdit.apiService = this.apiRest.API_CAMPAIGN;
        this.campaignToEdit.module = 'Campaign';
    };
    CampaignComponent.prototype.getFromSearch = function (campaignFound) {
        this.campaignToEdit.edition = false;
        if (campaignFound.value.length > 0) {
            this.campaigns = campaignFound.value.sort(this.compareByCountry);
            this.ListingComponent.refreshLocations(this.campaigns);
            this.resultsFound = true;
        }
        else {
            this.resultsFound = false;
        }
    };
    ;
    CampaignComponent.prototype.ngOnInit = function () {
    };
    /**
    * Helper to sort an Array of Campaigns
    *
    * @param {CallCenter} countryA
    * @param {CallCenter} countryB
    * @returns {number}
    *
    * @memberOf CampaignListComponent
    */
    CampaignComponent.prototype.compareByCountry = function (countryA, countryB) {
        if (countryA.countryName < countryB.countryName) {
            return -1;
        }
        if (countryA.countryName > countryB.countryName) {
            return 1;
        }
        return 0;
    };
    /**
     * pARSE VALUES BEFORE SEND IT TO EDIT TEMPLATE
     *
     * @param {any} {newLocationToEdit , cityIndex}
     * @memberof CampaignComponent
     */
    CampaignComponent.prototype.editCampaign = function (_a) {
        var newLocationToEdit = _a.newLocationToEdit;
        console.log(newLocationToEdit);
        // let newCampaign = new Campaign(newLocationToEdit.countryName,newLocationToEdit.countryCode);
        // newCampaign.cities[0]= new CampaignBasedModel(newLocationToEdit.cities[0].name);
        // newLocationToEdit.cities.forEach(city => {
        //   newCampaign.cities[0].properties.push(city.properties[0]);
        // });
        // console.log(newCampaign)
        // this.editLocation({newLocationToEdit: newCampaign, cityIndex: 0});
        this.editLocation({ newLocationToEdit: newLocationToEdit, cityIndex: 0 });
    };
    /**
     * Add a new country with all it's default properties.
     *
     *
     * @memberOf OfficeLocationComponent
     */
    CampaignComponent.prototype.addNewCountry = function (_a) {
        var newCountryToEdit = _a.newCountryToEdit, newLanguageToEdit = _a.newLanguageToEdit, newLocationToEdit = _a.newLocationToEdit, newTextToEdit = _a.newTextToEdit;
        var countrySelected = this.searchFilter.countryFromServer.find(function (country) { return country.countryName == newCountryToEdit; });
        var cityIndex;
        if (!newLocationToEdit) {
            console.log("Existe New Location");
            newLocationToEdit = new __WEBPACK_IMPORTED_MODULE_1__models__["Campaign"](countrySelected.countryName, countrySelected.countryCode);
            newLocationToEdit = this.addNewProperty(newLocationToEdit, newLanguageToEdit, newTextToEdit);
            cityIndex = 0;
        }
        else {
            newLocationToEdit = this.entityLocation.locationList.find(function (location) { return location.countryName == newLocationToEdit.countryName; });
            newLocationToEdit = this.addNewProperty(newLocationToEdit, newLanguageToEdit, newTextToEdit);
            cityIndex = newLocationToEdit.cities.length - 1;
        }
        this.editLocation({ newLocationToEdit: newLocationToEdit, cityIndex: cityIndex });
        this.entityLocation.countrySelectionSetting = false;
    };
    CampaignComponent.prototype.addNewProperty = function (newLocationToEdit, newLanguageToEdit, newTextToEdit) {
        console.log("Paso por aca");
        newLocationToEdit.cities.push(new __WEBPACK_IMPORTED_MODULE_1__models__["CampaignBasedModel"](newTextToEdit));
        var cityIndex = newLocationToEdit.cities.length - 1;
        var properties = [];
        var newCampaign = properties[0] = new __WEBPACK_IMPORTED_MODULE_1__models__["CampaignProperty"](newLanguageToEdit, newTextToEdit);
        newLocationToEdit.cities[cityIndex].properties[0] = properties;
        return newLocationToEdit;
    };
    CampaignComponent.prototype.parseDataCamp = function (entityLocation) {
        var _this = this;
        console.log("parseara");
        return new Promise(function (resolve, reject) {
            var valueToSend = new __WEBPACK_IMPORTED_MODULE_1__models__["CampaignEntity"](entityLocation.countryName, entityLocation.countryCode);
            entityLocation.cities.forEach(function (city) {
                city.properties.forEach(function (properties) {
                    var newCampaign = new __WEBPACK_IMPORTED_MODULE_1__models__["CampaignBasedEntity"]();
                    newCampaign.name = properties[0].campaignName;
                    newCampaign.properties = properties;
                    valueToSend.campaigns.push(newCampaign);
                });
            });
            _this.entityLocation.locationParsed = valueToSend;
            resolve(valueToSend);
            console.log(_this.entityLocation.locationParsed);
        });
    };
    return CampaignComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('searchInput'),
    __metadata("design:type", Object)
], CampaignComponent.prototype, "searchFilter", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__countryLangBased_module_listing__["a" /* ModuleListingComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__countryLangBased_module_listing__["a" /* ModuleListingComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__countryLangBased_module_listing__["a" /* ModuleListingComponent */]) === "function" && _a || Object)
], CampaignComponent.prototype, "ListingComponent", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4__utils_search_filter__["a" /* SearchFilterComponent */]),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__utils_search_filter__["a" /* SearchFilterComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__utils_search_filter__["a" /* SearchFilterComponent */]) === "function" && _b || Object)
], CampaignComponent.prototype, "searchFilterComponent", void 0);
CampaignComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-campaign',
        template: __webpack_require__("./src/app/components/main/campaign/campaign.component.html"),
        styles: [__webpack_require__("./src/app/components/main/campaign/campaign.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services__["c" /* GenericService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["c" /* GenericService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__models__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models__["Constants"]) === "function" && _d || Object])
], CampaignComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=campaign.component.js.map

/***/ }),

/***/ "./src/app/components/main/campaign/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__campaign_component__ = __webpack_require__("./src/app/components/main/campaign/campaign.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__campaign_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/countryLangBased/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__panel_creation__ = __webpack_require__("./src/app/components/main/countryLangBased/panel-creation/index.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__module_listing__ = __webpack_require__("./src/app/components/main/countryLangBased/module-listing/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__module_listing__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__module_editing__ = __webpack_require__("./src/app/components/main/countryLangBased/module-editing/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__module_editing__["a"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/countryLangBased/module-editing/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__module_editing_component__ = __webpack_require__("./src/app/components/main/countryLangBased/module-editing/module-editing.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__module_editing_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/countryLangBased/module-editing/module-editing.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <h1>{{locationToEdit.entityLocation.countryName}}</h1>\n  <ul class=\"nav nav-tabs\">\n    <li class=\"dropdown\"> \n      <a class=\"dropdown-toggle\" \n      href=\"javascript:void(0)\" \n      (click)=\"toggleLangSelection()\"  \n      [ngClass]=\"{'btn-addLang-active':langSelectionSetting}\">\n        + Add new language \n        <span class=\"caret\"></span>\n      </a>\n    </li>\n    <ng-container *ngFor=\"let property of locationToEdit.entityLocation.cities[indexCity].properties[0]; let i = index;\">\n    <li data-toggle=\"tab\" *ngIf=\"i != 0\">\n      <a href=\"javascript:void(0)\" (click)=\"languageSelected=i;langSelectionSetting=false;\">\n        {{property.language}}\n      </a>\n    </li>\n    </ng-container>\n    <li data-toggle=\"tab\" \n      class=\"active\">\n      <a href=\"#\" (click)=\"languageSelected=0; langSelectionSetting=false;\">\n        {{locationToEdit.entityLocation.cities[indexCity].properties[0][0].language}}\n      </a>\n    </li>\n  </ul>\n  <div id=\"demo\" class=\"collapse-Selection\" *ngIf=\"langSelectionSetting\">\n      <h4>Settings</h4>\n      <div class=\"setting-margin\">\n        <div class=\"form-group\">\n          <label for=\"\"> Current Language</label>\n            <select name=\"\" [(ngModel)]=\"languageToCopy\" class=\"form-control select-Input\" id=\"\">\n                <option selected disabled>Copy Content From</option>\n              <option value={{property.language}} *ngFor=\"let property of locationToEdit.entityLocation.cities[indexCity].properties[0]; let i = index;\">\n                {{property.language}}\n              </option>\n            </select>\n        </div>\n        <div class=\"form-group\">\n          <label for=\"\">New Language</label>\n          <select name=\"\" [(ngModel)]=\"newLanguageCopied\" id=\"\" class=\"form-control select-Input rightSelect-Style\">\n              <option selected disabled>Select New Language</option>\n            <option value={{language}} *ngFor=\"let language of availableSelectionLanguage;\">\n              {{language}}\n            </option>\n          </select>\n        </div>\n      <br>\n      <button href=\"javascript:void(0);\" (click)=\"addNewLanguage()\" [disabled]=\"validSettingForm()\" class=\"btn btn-primaryTheme setting-margin\">\n          Add New Language\n      </button>\n    </div>\n</div>\n</div>\n<div class=\"container-fluid\"> \n<form [formGroup]=\"myForm\" (ngSubmit)=\"onSubmit(myForm.value)\">\n  <div class=\"container-fluid borderFluid\"> \n    <div class=\"row\">\n      <div class=\"col-sm-3 header-content\">\n        <h3 class=\"header-Panel\">{{nameModule}} List</h3>\n      </div>\n      <div class=\"pull-right add-btn\">\n        <a href=\"javascript:void(0);\" (click)=\"addCityForm()\" class=\"btn btn-primaryTheme\">\n              Add New {{newButton}}\n            </a>\n      </div>\n      <div class=\"row list-Component\">\n      <div class=\"col-sm-12\">\n        <table class=\"table table-condensed\">\n          <thead>\n            <tr>\n                <th *ngIf=\"locationToEdit.module === 'Campaign'\">\n                    Image <span class=\"errorColor\">*</span>\n                </th>\n                <th *ngIf=\"locationToEdit.module === 'Campaign'\">\n                    From\n                </th>\n                <th *ngIf=\"locationToEdit.module === 'Campaign'\">\n                    To \n                </th>\n              <th *ngIf=\"locationToEdit.module === 'Campaign'\">\n                  Name <span class=\"errorColor\">*</span>\n              </th>\n              <th  *ngIf=\"locationToEdit.module === 'Campaign'\" >\n                  Link <span class=\"errorColor\">*</span>\n              </th>\n              <th *ngIf=\"locationToEdit.module === 'CallCenter' || locationToEdit.module === 'OfficeLocation' \">\n                  Status\n                </th>\n              <th  *ngIf=\"locationToEdit.module === 'CallCenter' || locationToEdit.module === 'OfficeLocation' \">\n                City Translation<span class=\"errorColor\">*</span>\n              </th>\n              <th *ngIf=\"locationToEdit.module === 'OfficeLocation'\">\n                Address <span class=\"errorColor\">*</span>\n              </th>\n              <th *ngIf=\"locationToEdit.module === 'CallCenter' || locationToEdit.module === 'OfficeLocation' \">\n                Telephone\n              </th>\n              <th *ngIf=\"locationToEdit.module === 'CallCenter' || locationToEdit.module === 'OfficeLocation' \">\n                Opening Times\n              </th>\n              <th *ngIf=\"locationToEdit.module === 'OfficeLocation'\">\n               Latitude/\n                Longitude\n              </th>\n              <th *ngIf=\"locationToEdit.module != 'Campaign'\">\n                  Delete \n              </th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr formArrayName=\"arrayCityForm\" *ngFor=\"let arrayCity of arrayForm; let i = index;\">\n              <ng-container [formGroupName]=\"i\">\n                  \n                  <td style=\"width:15%\" *ngIf=\"locationToEdit.module === 'Campaign'\">\n                    <div class=\"col-sm-12 form-group\">\n                      <a href=\"#\" (click)=\"setIndexForm(i); toggleCampaignModal=true\" data-toggle=\"modal\" data-target=\"#campaignModal\">\n                          <i *ngIf=\"!hasValue(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].fileName)\"\n                          class=\"fa fa-upload\" aria-hidden=\"true\">\n                        </i>\n                        <img\n                        src=\"{{getUrlImage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].fileName)}}\" \n                        class=\"imgToShow\" \n                        alt=\"\" \n                        *ngIf=\"hasValue(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].fileName)\">\n                      </a>\n                      <input type=\"hidden\" \n                      class=\"form-control\" \n                      [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].fileName\"\n                      formControlName=\"fileName\">\n                    </div>\n                 </td>\n                      <td style=\"width:15%\" *ngIf=\"locationToEdit.module === 'Campaign'\">\n                          <div class=\"form-group\">\n                          <div class='input-group date' id='datetimepicker1'>\n                          <input type=\"text\" \n                          class=\"form-control\" \n                          [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].fromSchedule\"\n                          bsDatepicker \n                          #dpFrom=\"bsDatepicker\" \n                          [bsConfig]=\"bsConfig\" \n                          formControlName=\"fromSchedule\"\n                          dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                          <span class=\"input-group-addon\" (click)=\"dpFrom.toggle()\">\n                            <span class=\"fa fa-calendar\"></span>\n                         </span>  \n                        </div>\n                        </div>\n                    </td>\n                    <td style=\"width:15%\" *ngIf=\"locationToEdit.module === 'Campaign'\">\n                        <div class=\"form-group\">\n                        <div class='input-group date' id='datetimepicker1'>\n                        <input type=\"text\" \n                        class=\"form-control\" \n                        [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].toSchedule\"\n                        (change) =\"parseDate(indexCity,i,languageSelected, 'to')\"\n                        bsDatepicker \n                        #dpFrom=\"bsDatepicker\" \n                        [bsConfig]=\"bsConfig\" \n                        formControlName=\"toSchedule\"\n                        dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                        <span class=\"input-group-addon\" (click)=\"dpFrom.toggle()\">\n                          <span class=\"fa fa-calendar\"></span>\n                       </span>  \n                      </div>\n                      </div>\n                  </td>\n                  <td style=\"width:15%\" *ngIf=\"locationToEdit.module == 'Campaign'\">\n                      <div class=\"form-group\" \n                        [ngClass]=\"{ 'has-error': \n                        (locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].campaignName == '' && arrayCity.get('campaignName').touched) ||\n                        (arrayCity.get('campaignName').untouched && formSubmitAttempt)\n                        }\">\n                        <input type=\"text\"\n                        placeholder=\"Campaign Name\" \n                        class=\"form-control\" \n                        [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].campaignName\" \n                        formControlName=\"campaignName\"\n                        dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                        <div *ngIf=\"  (locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].campaignName == '' && arrayCity.get('campaignName').touched) ||\n                        (arrayCity.get('campaignName').untouched && formSubmitAttempt)\" class=\"help-block\">\n                            Campaign Name is required\n                        </div>\n                      </div>\n                    </td>\n                    <td style=\"width:15%\" *ngIf=\"locationToEdit.module == 'Campaign'\">\n                        <div class=\"form-group\" \n                          [ngClass]=\"{ 'has-error':(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].link == '' && arrayCity.get('link').touched) ||\n                          (arrayCity.get('link').untouched && formSubmitAttempt)}\">\n                          <input type=\"text\"\n                          placeholder=\"link\" \n                          class=\"form-control\" \n                          [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].link\" \n                          formControlName=\"link\"\n                          dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                          <div *ngIf=\"\n                          (locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].link == '' && arrayCity.get('link').touched) ||\n                           (arrayCity.get('link').untouched && formSubmitAttempt)\" class=\"help-block\">\n                              link is required\n                          </div>\n                        </div>\n                      </td>\n                    <td style=\"width:10%\" *ngIf=\"locationToEdit.module !== 'Campaign'\">\n                        <select formControlName=\"published\"\n                          *ngIf=\"i === 0\"\n                          name=\"Published\" \n                          [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].published\"\n                          (change)=\"changeStatusValue($event,i, languageSelected)\" \n                          class=\"form-control\">\n                          <option value=\"true\">Published</option>\n                          <option value=\"false\">Un-Published</option>\n                        </select>\n                      </td>\n                    <td style=\"width:15%\" *ngIf=\"locationToEdit.module !== 'Campaign'\">\n                      <div class=\"form-group\" \n                        *ngIf=\"i === 0\"\n                        [ngClass]=\"{ 'has-error': \n                        (locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].cityName == '' && arrayCity.get('city').touched) ||\n                        (arrayCity.get('city').untouched && formSubmitAttempt) }\">\n                        <input type=\"text\"\n                        placeholder=\"City\" \n                        class=\"form-control\" \n                        [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].cityName\" \n                        formControlName=\"city\"\n                        (change)=\"changePropertyValue(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].cityName, languageSelected, 'cityName')\"\n                        dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                        <div *ngIf=\"\n                        (locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].cityName == '' && arrayCity.get('city').touched) ||\n                        (arrayCity.get('city').untouched && formSubmitAttempt)\" class=\"help-block\">\n                            City is required\n                        </div>\n                      </div>\n                    </td>\n                  \n                  <td style=\"width:20%\" *ngIf=\"locationToEdit.module === 'OfficeLocation'\">\n                    <div class=\"form-group\" \n                      [ngClass]=\"{ 'has-error':  \n                      (locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].address == '' && arrayCity.get('address').touched) ||\n                      (arrayCity.get('address').untouched && formSubmitAttempt) }\"> \n                      <textarea (keyup)=\"onKey($event)\"\n                        placeholder=\"Address\" \n                        class=\"form-control\"\n                        [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].address\"\n                        formControlName=\"address\"\n                        dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                      </textarea>\n                      <div *ngIf=\"\n                      (locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].address == '' && arrayCity.get('address').touched) ||\n                      (arrayCity.get('address').untouched && formSubmitAttempt)\" class=\"help-block\">\n                          Address is required\n                        </div>\n                    </div>\n                  </td>\n                  <td style=\"width:20%\" *ngIf=\"locationToEdit.module !== 'Campaign'\">\n                    <textarea (keyup)=\"onKey($event)\"\n                      placeholder=\"Telephone\" \n                      class=\"form-control\"\n                      [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].telephone\"\n                      formControlName=\"telephone\"\n                    >\n                    </textarea>\n                  </td>\n                  <td style=\"width:20%\" *ngIf=\"locationToEdit.module !== 'Campaign'\">\n                    <textarea (keyup)=\"onKey($event)\"\n                     placeholder=\"Opening Time\" \n                     class=\"form-control\" \n                     [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].openingTime\" \n                     formControlName=\"openingTime\"\n                     dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                    </textarea>\n                  </td>\n                  <td style=\"width:15%\" *ngIf=\"locationToEdit.module === 'OfficeLocation'\">\n                    <label for=\"latitude\" class=\"latLongLabel\"> Lat </label> \n                    <input type=\"text\"\n                    placeholder=\"Latitude\"  \n                    class=\"form-control latLongInput\" \n                    [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].latitude\" \n                    formControlName=\"latitude\"\n                    dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                    <label for=\"latitude\" class=\"latLongLabel\"> Long </label> \n                    <input type=\"text\"\n                    placeholder=\"Longitude\" \n                    class=\"form-control latLongInput\"\n                    [(ngModel)]=\"locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].longitude\" \n                    formControlName=\"longitude\"\n                    dir={{inputLanguage(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected].language)}}>\n                    <a href=\"\" \n                    class=\"default-btn\"\n                    data-toggle=\"modal\" \n                    data-target=\"#locationModal\" \n                    (click)=\"toggleLocationModal=true; selectCityLocation(locationToEdit.entityLocation.cities[indexCity].properties[i][languageSelected])\"> \n                    Select new location</a>\n                  </td>\n               \n                  <td style=\"width:5%\" class=\"deleteBtn\">\n                      <a class=\"default-btn\" href=\"javascript:void(0);\" *ngIf=\"arrayForm.length>1\" (click)=\"removeCity(i)\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>\n                  </td>\n                  \n              </ng-container>\n            </tr>\n          </tbody>\n        </table>\n        </div>\n      </div>\n    </div>\n    <div [ngClass]=\"{'desactiveClass': langSelectionSetting}\">\n\n      </div>\n  </div>\n  <br>\n  <div class=\"row\">\n\n    <div class=\"col-sm-2 pull-right\">\n      <div class=\"form-group\">\n        <button type=\"submit\" class=\"btn btn-primaryTheme\" (click)=\"submitted=true;\">Save Changes</button>\n      </div>\n    </div>\n\n  </div>\n  <div class=\"row\">\n      <div class=\"col-sm-5 pull-right\">\n          <alert></alert>\n      </div>\n  </div>\n</form>\n</div>\n\n\n<div class=\"modal fade\" id=\"locationModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"locationModal\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-md\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Office Geolocation</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n      </div>\n      <div class=\"modal-body\">\n       <app-geolocation *ngIf=\"toggleLocationModal\"  [init]=\"locationCityToEdit\" >\n\n       </app-geolocation>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n        <button type=\"button\" class=\"btn btn-primaryTheme\" data-dismiss=\"modal\" (click)=\"saveLocation()\" >Save changes</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" id=\"campaignModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"campaignModal\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-md\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLabel\">Image List</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n              <span aria-hidden=\"true\">&times;</span>\n            </button>\n        </div>\n        <div class=\"modal-body\" *ngIf=\"toggleCampaignModal\" >\n          <form [formGroup]=\"fileForm\" (ngSubmit)=\"submitFile(fileForm.value)\">\n            <div class=\"col-sm-12 text-center\">\n              <label class=\"upload-btn\">\n                \n                          <i class=\"fa fa-upload\" aria-hidden=\"true\"></i>     \n                          <ng-container *ngIf=\"newImageSelected\">\n                            {{selectedUploadFile.name}}\n                          </ng-container>\n                          <ng-container *ngIf=\"!newImageSelected\">\n                              + New File\n                            </ng-container>\n                            <input type=\"file\" id=\"file\" class=\"upload-fileInput\" multiple accept=\"image/x-png,image/gif,image/jpeg\" (change)=\"imageUpload($event)\">\n              </label>\n            </div>\n            <div class=\"col-sm-12 upload-btn text-center\" *ngIf=\"newImageSelected\">\n              <button type=\"submit\" class=\"btn btn-primaryTheme\">Submit</button>\n            </div>\n          </form>\n          <br>\n          <hr>\n          <table class=\"table\">\n            <thead>\n              <tr>\n                <th>FileName</th>\n                <th>Preview</th>\n                <th>Select</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let file of paginationList.pagedItems\">\n                <td>{{file.filename}}</td>\n                <td><img class=\"img-md\"  src={{getUrlImage(file.filename)}} alt=\"\"></td>\n                <td>\n                  <div class=\"radio\">\n                    <label><input type=\"radio\" name=\"optradio\" (change)=\"selectImageFromList(file.filename)\" value={{file.filename}}></label>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n          <div class=\"col-sm-12\">\n          <ng-container *ngIf=\"paginationList.entityList.length>0\">\n              <app-pagination [init]=\"paginationList\">\n              </app-pagination>\n            </ng-container>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primaryTheme\" data-dismiss=\"modal\">Save changes</button>\n    \n        </div>\n      </div>\n    </div>\n  </div>\n\n"

/***/ }),

/***/ "./src/app/components/main/countryLangBased/module-editing/module-editing.component.scss":
/***/ (function(module, exports) {

module.exports = ".upload-file {\n  width: 10px; }\n\n.upload-fileInput {\n  opacity: 0;\n  position: absolute;\n  z-index: -1; }\n\n.upload-btn {\n  cursor: pointer; }\n\n.collapse-Selection {\n  right: 16px;\n  top: unset; }\n\n.collapse-Selection:after {\n    content: '';\n    display: block;\n    position: absolute;\n    width: 0px;\n    right: 0px;\n    top: -1px;\n    border: 1px solid white; }\n\n.nav-tabs li.active {\n  width: 50px; }\n"

/***/ }),

/***/ "./src/app/components/main/countryLangBased/module-editing/module-editing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModuleEditingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("./src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_pagination__ = __webpack_require__("./src/app/components/utils/pagination/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ModuleEditingComponent = (function () {
    function ModuleEditingComponent(fb, genServices, alertService, loader, constants) {
        this.fb = fb;
        this.genServices = genServices;
        this.alertService = alertService;
        this.loader = loader;
        this.constants = constants;
        this.formSubmitAttempt = false;
        this.nameModule = "";
        this.locationToEdit = {
            edition: false,
            entityLocation: new __WEBPACK_IMPORTED_MODULE_3__models__["Entity"](),
            cityIndexSelected: '',
            apiService: '',
            module: '',
            mainEntity: '',
            citiesEntity: ''
        };
        this.newButton = "Office";
        this.valueFromEdit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.colorTheme = 'theme-red';
        this.paginationList = {
            entityList: [],
            pagedItems: [],
            pager: {},
            setPage: {},
            customNumberPages: 4
        };
        this.langSelectionSetting = false;
        this.toggleLocationModal = false;
        /**
       * Variables used in new Language selection.
       *
       *
       * @memberOf OfficeLocationEditComponent
       */
        this.languageToCopy = 'Copy Content From';
        this.newLanguageCopied = 'Select New Language';
        this.rootDir = constants.root_dir;
        this.apiRest = constants.IApiRest;
        this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    }
    ModuleEditingComponent.prototype.ngOnInit = function () {
        this.languageSelected = 0;
        this.indexCity = this.locationToEdit.cityIndexSelected;
        this.initEditionForm();
        this.initLanguages();
        this.setExistingLanguages();
        if (this.locationToEdit.module == 'Campaign') {
            this.files = [];
            this.initFileForm();
            this.initFiles();
        }
    };
    /**
   * Listing all files from server - CREATE MODAL IMAGES COMP
   *
   *
   * @memberOf CampaignEditComponent
   */
    ModuleEditingComponent.prototype.initFiles = function () {
        var _this = this;
        this.genServices.get(this.apiRest.API_FILES).subscribe(function (response) {
            _this.files = response.data;
            _this.paginationList.entityList = _this.files.reverse();
        }, function (error) {
        }, function () {
        });
    };
    /**
    * Get all Languages from server
    *
    *
    * @memberOf CampaignEditComponent
    */
    ModuleEditingComponent.prototype.initLanguages = function () {
        var _this = this;
        this.genServices.get(this.apiRest.API_LANGUAGE).subscribe(function (response) {
            _this.languagesArray = response.data.languages.map(function (language) { return language.languageCode; });
        }, function (error) {
        });
    };
    /**
   * This will be used when user wants to add a new file.
   * - CREATE MODAL IMAGES COMP
   *
   *
   * @memberOf CampaignEditComponent
   */
    ModuleEditingComponent.prototype.initFileForm = function () {
        this.fileForm = this.fb.group({
            image: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('')
        });
    };
    /**
      * Initialize form in case it is for edit porpose.
      *
      *
      * @memberOf CampaignEditComponent
      */
    ModuleEditingComponent.prototype.initEditionForm = function () {
        this.myForm = this.fb.group({
            city: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](this.locationToEdit.entityLocation.countryName),
            arrayCityForm: this.fb.array([]),
        });
        this.arrayForm = this.myForm.controls.arrayCityForm.controls;
        //TODO ARRAY OF CITIES
        // this.arrayForm.push(this.fb.array[])
        if (this.locationToEdit.entityLocation.cities[this.indexCity].properties.length > 0) {
            this.addCityForm(this.locationToEdit.entityLocation.cities[this.indexCity]);
        }
    };
    /**
     * Adding a new City Form
     *
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.addCityForm = function (city) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var cityForm;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        cityForm = {};
                        console.log(city);
                        if (!!city) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.initCity()];
                    case 1:
                        city = _a.sent();
                        _a.label = 2;
                    case 2:
                        cityForm = this.createFormField(city);
                        city.properties.forEach(function (property) {
                            var control = _this.myForm.controls['arrayCityForm'];
                            var formGroup = _this.createForm(property);
                            formGroup = Object.assign(formGroup, cityForm);
                            control.push(_this.fb.group(formGroup));
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Create a new property and return it
     *
     * @param {MainProperties} properties
     * @returns
     * @memberof ModuleEditingComponent
     */
    ModuleEditingComponent.prototype.addPropertyInstance = function (properties) {
        if (properties[0] == undefined) {
            var newProp = [];
            var size = this.locationToEdit.entityLocation.cities[this.indexCity].properties[0].length;
            if (size >= 1) {
                for (var i = 0; i < size; i++) {
                    newProp[i] = this.initPropertyInstance(this.locationToEdit.module, this.locationToEdit.entityLocation.cities[this.indexCity].properties[0][i].language, i);
                }
                return newProp;
            }
        }
        return this.initPropertyInstance(this.locationToEdit.module);
    };
    /**
     * Return a new City Form.
     *
     * @param {*} [city=""]
     * @param {*} [address=""]
     * @param {*} [telephone=""]
     * @param {*} [openingTime=""]
     * @param {*} [latitude=""]
     * @param {*} [longitude=""]
     * @param {*} [published=""]
     * @returns
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.initCity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var newCity;
            var newOfficeProp;
            //If it is undefined it is because is a new Form.
            newCity = _this.initModelInstance(_this.locationToEdit.module);
            console.log(newCity.properties);
            newOfficeProp = _this.addPropertyInstance(newCity.properties);
            newCity.properties.push(newOfficeProp);
            _this.locationToEdit.entityLocation.cities[_this.indexCity].properties.push(newOfficeProp);
            resolve(newCity);
        });
    };
    /**
     * Instantiate Entity based on Edition component.
     *
     * @param {any} entityModule
     * @returns
     * @memberof ModuleEditingComponent
     */
    ModuleEditingComponent.prototype.initModelInstance = function (entityModule) {
        console.log("probando modelInstance");
        switch (entityModule) {
            case 'CallCenter':
                return new __WEBPACK_IMPORTED_MODULE_3__models__["CityModel"]();
            case 'OfficeLocation':
                return new __WEBPACK_IMPORTED_MODULE_3__models__["CityOffices"]();
            case 'Campaign':
                return new __WEBPACK_IMPORTED_MODULE_3__models__["CampaignBasedModel"]();
        }
    };
    /**
     * Instantiate properties depending of entityModule.
     *
     * @param {any} entityModule
     * @returns
     * @memberof ModuleEditingCompoaddNewLanguagenent
     */
    ModuleEditingComponent.prototype.initMainInstance = function (entityModule, countryName, countryCode) {
        console.log("probando mainInstance");
        switch (entityModule) {
            case 'CallCenter':
                var newInstance = Object.create(window[this.locationToEdit.mainEntity].prototype)(countryName, countryCode);
                newInstance.constructor.apply(countryName, countryCode);
            case 'OfficeLocation':
                return new __WEBPACK_IMPORTED_MODULE_3__models__["OfficeLocation"](countryName, countryCode);
            case 'Campaign':
                return new __WEBPACK_IMPORTED_MODULE_3__models__["Campaign"](countryName, countryCode);
        }
    };
    /**
     * Instantiate properties depending of entityModule.
     *
     * @param {any} entityModule
     * @returns
     * @memberof ModuleEditingCompoaddNewLanguagenent
     */
    ModuleEditingComponent.prototype.initPropertyInstance = function (entityModule, lang, i) {
        if (lang === void 0) { lang = "en"; }
        if (i === void 0) { i = 0; }
        switch (entityModule) {
            case 'CallCenter':
                return new __WEBPACK_IMPORTED_MODULE_3__models__["CityProperty"](lang, this.locationToEdit.entityLocation.cities[this.indexCity].properties[0][i].cityName);
            case 'OfficeLocation':
                return new __WEBPACK_IMPORTED_MODULE_3__models__["OfficeProperties"](lang, this.locationToEdit.entityLocation.cities[this.indexCity].properties[0][i].cityName);
            case 'Campaign':
                return new __WEBPACK_IMPORTED_MODULE_3__models__["CampaignProperty"](lang);
        }
    };
    /**
     *
     *
     * @param {any} value
     * @returns
     * @memberof ModuleEditingComponent
     */
    ModuleEditingComponent.prototype.createForm = function (property) {
        var _this = this;
        var propertyForm = {};
        property.forEach(function (office) {
            propertyForm = _this.createFormField(office);
        });
        return propertyForm;
    };
    ModuleEditingComponent.prototype.createFormField = function (fieldValue) {
        var newField = {};
        var newForm = {};
        for (var name in fieldValue) {
            newField = fieldValue[name];
            if (typeof newField != 'object') {
                newForm[name] = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](newField);
            }
        }
        return newForm;
    };
    /**
     * Sumbitting new Office Location.
     *
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var valueToSend;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(this.locationToEdit.entityLocation);
                        return [4 /*yield*/, this.parseDataToSend(this.locationToEdit.entityLocation)];
                    case 1:
                        valueToSend = _a.sent();
                        console.log(valueToSend);
                        this.formSubmitAttempt = true;
                        this.genServices.create(valueToSend, this.locationToEdit.apiService).subscribe(function (response) {
                            if (response.message.code != 501) {
                                _this.valueFromEdit.emit({
                                    value: _this.locationToEdit.entityLocation
                                });
                            }
                        }, function (error) {
                            _this.loader.hide();
                            _this.alertService.errorArray([error.message.description]);
                            console.log(error);
                        }, function () {
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ModuleEditingComponent.prototype.parseDataToSend = function (entityLocation) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            switch (_this.locationToEdit.module) {
                case 'Campaign':
                    var valueToSend_1 = new __WEBPACK_IMPORTED_MODULE_3__models__["CampaignEntity"](entityLocation.countryName, entityLocation.countryCode);
                    entityLocation.cities.forEach(function (city) {
                        city.properties.forEach(function (properties) {
                            var newCampaign = new __WEBPACK_IMPORTED_MODULE_3__models__["CampaignBasedEntity"]();
                            newCampaign.name = properties[0].campaignName;
                            newCampaign.properties = properties;
                            newCampaign.properties.forEach(function (property) {
                                property.fromSchedule = _this.parseDateAndReturn(property.fromSchedule);
                                property.toSchedule = _this.parseDateAndReturn(property.toSchedule);
                                property.modifiedDate = _this.parseDateAndReturn(property.modifiedDate);
                            });
                            valueToSend_1.campaigns.push(newCampaign);
                        });
                    });
                    resolve(valueToSend_1);
                    break;
                default:
                    resolve(entityLocation);
                    break;
            }
        });
    };
    ModuleEditingComponent.prototype.parseDateAndReturn = function (date) {
        if (typeof date == 'object') {
            return date.toJSON().slice(0, 10).replace(/-/g, '/');
        }
        return date;
    };
    ModuleEditingComponent.prototype.changePropertyValue = function (fieldValue, indexLang, propKey) {
        this.locationToEdit.entityLocation.cities[this.indexCity].city = fieldValue;
        this.locationToEdit.entityLocation.cities[this.indexCity].properties.forEach(function (property) {
            for (var key in property[indexLang]) {
                if (key === propKey) {
                    property[indexLang][key] = fieldValue;
                }
            }
        });
    };
    /**
     * ADD NEW OFFICE LOCATION BTN.
     * Set Langage to Copy and new Language availables in Select Input.
     *
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.setExistingLanguages = function () {
        this.existingLanguage = this.locationToEdit.entityLocation.cities[this.indexCity].properties[0]
            .map(function (property) { return property.language; });
        this.languageToCopy = 'Copy Content From';
        this.newLanguageCopied = 'Select New Language';
    };
    /**
     * Text area Customize
     *
     * @param {*} $event
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.onKey = function ($event) {
        $event.srcElement.style.height = "34px";
        $event.srcElement.style.marginTop = "";
        if ($event.srcElement.scrollHeight >= 52) {
            $event.srcElement.style.height = "62px";
        }
        if ($event.srcElement.scrollHeight >= 72) {
            $event.srcElement.style.height = "82px";
        }
    };
    /**
     * Adding Language translation
     *
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.addNewLanguage = function () {
        var _this = this;
        // this.langSelectionSetting =true;
        console.log(this.locationToEdit.entityLocation.cities);
        this.locationToEdit.entityLocation.cities[this.indexCity].properties.forEach(function (location) {
            var newProperty = location.find(function (property) { return property.language == _this.languageToCopy; });
            switch (_this.locationToEdit.module) {
                case 'OfficeLocation':
                    location.push(new __WEBPACK_IMPORTED_MODULE_3__models__["OfficeProperties"](_this.newLanguageCopied, newProperty.cityName, newProperty.openingTime, newProperty.telephone, newProperty.address, newProperty.latitude, newProperty.longitude));
                    ;
                    break;
                case 'CallCenter':
                    location.push(new __WEBPACK_IMPORTED_MODULE_3__models__["CityProperty"](_this.newLanguageCopied, newProperty.cityName, newProperty.openingTime, newProperty.telephone));
                    break;
                case 'Campaign':
                    location.push(new __WEBPACK_IMPORTED_MODULE_3__models__["CampaignProperty"](_this.newLanguageCopied, newProperty.campaignName, newProperty.fileName, newProperty.link));
                    break;
                default:
                    break;
            }
        });
        this.langSelectionSetting = false;
    };
    /**
     * This will enable/disable submit button.
     *
     * @returns
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.getCityFormValidation = function () {
        var _this = this;
        var isInvalid = false;
        var validatingArray = new Array();
        console.log(this.locationToEdit.entityLocation.cities);
        this.locationToEdit.entityLocation.cities.forEach(function (location) {
            location.properties.forEach(function (property) {
                property.forEach(function (element) {
                    validatingArray.push(_this.validateByModule(element));
                });
            });
        });
        console.log(validatingArray);
        return validatingArray.indexOf(isInvalid) > -1;
    };
    ModuleEditingComponent.prototype.validateByModule = function (property) {
        switch (this.locationToEdit.module) {
            case 'CallCenter':
                return property.cityName != '';
            case 'OfficeLocation':
                return property.address != '';
            case 'Campaign':
                return property.name != '';
        }
    };
    /**
     * Change Published value when status is selected.
     *
     * @param {any} event
     * @param {any} index
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.changeStatusValue = function (event, index, langIndex) {
        if (event.srcElement.value == 'true') {
            this.locationToEdit.entityLocation.cities[this.indexCity].published = true;
            return;
        }
        this.locationToEdit.entityLocation.cities[this.indexCity].published = false;
    };
    /**
     *Set @param langSelectionSetting to true in order to toggle to
     * a Modal to select new language.
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.toggleLangSelection = function () {
        var _this = this;
        if (this.langSelectionSetting) {
            this.langSelectionSetting = false;
        }
        else {
            this.langSelectionSetting = true;
        }
        this.availableSelectionLanguage = this.languagesArray.filter(function (lang) {
            return _this.existingLanguage.indexOf(lang) < 0;
        });
    };
    /**
     * This will enable/disable Bulk Selection.
     *
     * @returns
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.validSettingForm = function () {
        if (this.languageToCopy != 'Copy Content From' &&
            this.newLanguageCopied != 'Select New Language') {
            return false;
        }
        return true;
    };
    /**
     * Remove City Form.
     *
     * @param {any} indexCity
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.removeCity = function (indexCity) {
        this.locationToEdit.entityLocation.cities.splice(indexCity, 1);
        var countForm = this.myForm.controls['arrayCityForm'];
        countForm.removeAt(indexCity);
    };
    //GOOGLE MAP MODAL.
    /**
     * Once select new location button is clicked, this will set
     * @param this.locationCityToEdit in order to see it on googlemap
     * component
     *
     * @param {CityProperty} city
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.selectCityLocation = function (city) {
        console.log(city);
        this.cityToEdit = city;
        this.locationCityToEdit = {
            latitude: 0,
            longitude: 0,
            newLoc: false
        };
        this.locationCityToEdit.newLoc = true;
        this.locationCityToEdit.latitude = +city.latitude;
        this.locationCityToEdit.longitude = +city.longitude;
    };
    /**
     * Once location is selected, button saved will
     * save the locations on @param this.cityToEdit.
     *
     *
     * @memberOf OfficeLocationEditComponent
     */
    ModuleEditingComponent.prototype.saveLocation = function () {
        this.toggleLocationModal = true;
        this.cityToEdit.latitude = this.locationCityToEdit.latitude;
        this.cityToEdit.longitude = this.locationCityToEdit.longitude;
    };
    /**
     * input languageLanguage
     *
     * @param {any} arr
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleEditingComponent.prototype.inputLanguage = function (lang) {
        if (lang == 'ar') {
            return 'rtl';
        }
        else {
            return 'ltr';
        }
    };
    /**
     * Check if value is Empty
     *
     * @memberof ModuleEditingComponent
     */
    ModuleEditingComponent.prototype.hasValue = function (value) {
        return (value !== undefined && value != '' && value != null);
    };
    /**
   * Parse imgSource.
   *
   * @param {string} imageName
   * @returns
   *
   * @memberOf CampaignEditComponent
   */
    ModuleEditingComponent.prototype.getUrlImage = function (imageName) {
        return this.rootDir + this.apiRest.API_FILES + "/" + imageName;
    };
    /**
     * Upload new Image to server;
     *
     * @param {any} event
     *
     * @memberOf CampaignEditComponent
     */
    ModuleEditingComponent.prototype.imageUpload = function (event) {
        this.newImageSelected = true;
        var reader = new FileReader();
        this.selectedUploadFile = event.target.files[0];
        //get the selected file from event
        if (this.selectedUploadFile.type.match('image.*')) {
            reader.readAsDataURL(this.selectedUploadFile);
        }
    };
    /**
   * Uploading a new Image.
   *
   *
   * @memberOf CampaignEditComponent
   */
    ModuleEditingComponent.prototype.submitFile = function (value) {
        var _this = this;
        var formData = new FormData();
        formData.append(this.selectedUploadFile.name, this.selectedUploadFile);
        this.imageUrl = "";
        this.genServices.create(formData, this.apiRest.API_FILES).subscribe(function (response) {
            if (response.message.code == 200) {
                _this.files.unshift({ filename: _this.selectedUploadFile.name });
                _this.newImageSelected = false;
                _this.paginationList.entityList = _this.files;
                _this.changePagination.setPage(1);
            }
        }, function (error) {
            console.log(error);
            _this.alertService.error(error);
        }, function () {
        });
    };
    ModuleEditingComponent.prototype.setIndexForm = function (indexForm) {
        this.indexForm = indexForm;
    };
    /**
     * Once the image is selected it will save it on @param selectedFile
     *
     * @param {any} fileName
     *
     * @memberOf CampaignEditComponent
     */
    ModuleEditingComponent.prototype.selectImageFromList = function (fileName) {
        this.locationToEdit.entityLocation.cities[this.indexCity].properties[this.indexForm][this.languageSelected].fileName = fileName;
        console.log(this.locationToEdit.entityLocation.cities);
    };
    /**
   * When SaveChanges button is clicked it will save it con FormControl.Image.
   *
   *
   * @memberOf CampaignEditComponent
   */
    ModuleEditingComponent.prototype.saveImageChanges = function () {
        var form = this.myForm.controls['arrayCityForm'].controls[this.indexForm];
        form.controls.fileName.setValue(this.selectedFile);
        console.log(form);
        console.log(this.selectedFile);
        // form.controls.newImage.setValue(this.selectedFile);
    };
    return ModuleEditingComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('nameModule'),
    __metadata("design:type", Object)
], ModuleEditingComponent.prototype, "nameModule", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4__utils_pagination__["a" /* PaginationComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__utils_pagination__["a" /* PaginationComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__utils_pagination__["a" /* PaginationComponent */]) === "function" && _a || Object)
], ModuleEditingComponent.prototype, "changePagination", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('init'),
    __metadata("design:type", Object)
], ModuleEditingComponent.prototype, "locationToEdit", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('newButton'),
    __metadata("design:type", Object)
], ModuleEditingComponent.prototype, "newButton", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('newListValue'),
    __metadata("design:type", Object)
], ModuleEditingComponent.prototype, "valueFromEdit", void 0);
ModuleEditingComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-module-editing',
        template: __webpack_require__("./src/app/components/main/countryLangBased/module-editing/module-editing.component.html"),
        styles: [__webpack_require__("./src/app/components/main/countryLangBased/module-editing/module-editing.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* GenericService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* GenericService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["a" /* AlertService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["a" /* AlertService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* LoaderService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__models__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__models__["Constants"]) === "function" && _f || Object])
], ModuleEditingComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=module-editing.component.js.map

/***/ }),

/***/ "./src/app/components/main/countryLangBased/module-listing/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__module_listing_component__ = __webpack_require__("./src/app/components/main/countryLangBased/module-listing/module-listing.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__module_listing_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/countryLangBased/module-listing/module-listing.component.html":
/***/ (function(module, exports) {

module.exports = "\n    <div class=\"row list-Component\">\n      <div class=\"col-sm-12\">\n        <table class=\"table-condensed table-custom\" *ngIf=\"entityLocation.resultsFound\">\n          <thead>\n            <tr>\n              <th>\n                Country\n              </th>\n              <th>\n                Languages\n              </th>\n              <th>\n                  <span *ngIf=\"entityLocation.module != 'Campaign'\">\n                      City\n                  </span>\n                  <span *ngIf=\"entityLocation.module == 'Campaign'\">\n                      Campaign\n                  </span>\n              </th>\n              <th *ngIf=\"entityLocation.module === 'Campaign'\">\n                  From / To\n              </th>\n              <th *ngIf=\"entityLocation.module != 'Campaign'\">\n                Status\n              </th>\n              <th colspan=\"2\">\n                Options\n              </th>\n              <th *ngIf=\"entityLocation.module != 'Campaign'\">\n                <label class=\"checkbox-inline table-checkIn\">\n                  <input type=\"checkbox\" value=\"\" [(ngModel)]=\"bulkAction\" (change)=\"toggleStatusSelection()\">\n                </label>\n                <select *ngIf=\"bulkAction\" name=\"\" id=\"\" (change)=\"changePublishStatus($event)\" class=\"select-Input table-select-Input\">\n                  <option selected disabled>Bulk Action</option>\n                  <option value='true'>Published All</option>\n                  <option value='false' e>Un-Published All</option>\n                </select>\n                <span *ngIf=\"!bulkAction\">Bulk Action</span>\n              </th>\n            </tr>\n          </thead>\n          <ng-container *ngFor=\"let entity of paginationList.pagedItems; let i = index;\">\n            <tr *ngFor=\"let location of entity.cities; let j = index;\" [ngClass]=\"{'borderTable':j==0}\">\n              <td>\n                <p *ngIf=\"j==0\">\n                  {{ entity.countryName}}\n                </p>\n\n              </td>\n              <td>\n                <ng-container>\n                  <ng-container *ngFor=\"let city of location.properties[0]; let k = index;\">\n                   \n                    <br *ngIf=\"k % 3 == 0\">\n                    <a class=\"btn btn-default language-list\" *ngIf=\"location.properties[0].length > 1\" (click)=\"selectLanguageToDelete(entity, city.language, j)\"\n                      data-toggle=\"modal\" data-target=\"#deleteModal\">\n                     \n                        {{city.language}}\n                      <span class=\"label\">\n                        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>\n                      </span>\n                    </a>\n                    <a class=\"btn btn-default language-list\" *ngIf=\"location.properties[0].length == 1\">\n                      {{city.language}}\n                    </a>\n                    \n                    \n                  </ng-container>\n                </ng-container>\n              </td>\n              <td>\n                <p>{{location.city || location.name}}</p>\n              </td>\n              <td *ngIf=\"entityLocation.module === 'Campaign'\">\n                  <h5>\n                      From:\n                      {{location.properties[0][0].fromSchedule}}\n                  </h5>\n                  \n                  <h5>To:\n                      {{location.properties[0][0].toSchedule}}\n                  </h5>\n                  \n              </td>\n              <td *ngIf=\"entityLocation.module != 'Campaign'\">\n                <p *ngIf=\"location.published\">Published</p>\n                <p *ngIf=\"!location.published\">Un-Published</p>\n              </td>\n              <td>\n                <a href=\"javascript:void(0);\" (click)=\"editEntity(entity,j,i)\">\n                    <span class=\"default-btn\">\n                      <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>\n                      Edit\n                    </span>\n                  </a>\n              </td>\n              <td>\n                <a href=\"#\" data-toggle=\"modal\" data-target=\"#deleteModal\" (click)=\"selectPropertyToDelete(entity, j)\">\n                    <span class=\"default-btn\">\n                      <i class=\"fa fa-times\" aria-hidden=\"true\"></i>\n                      Delete\n                    </span>\n                  </a>\n  \n              </td>\n\n              <td *ngIf=\"entityLocation.module != 'Campaign'\">\n                <label class=\"checkbox-inline table-checkIn\">\n                  <input type=\"checkbox\" value=\"\" [(ngModel)]=\"entityToPublish[i]\" *ngIf=\"j == 0\" [disabled]=\"bulkAction\">\n                </label>\n                <button class=\"btn btn-primaryTheme\" href=\"javascript:void(0);\" [disabled]=\"!entityToPublish[i] || bulkAction\" (click)=\"setLocationPublishStatus(entity, true)\"\n                  *ngIf=\"j == 0 && getStatusPublish(entity)\">\n                  Publish\n                </button>\n                <button class=\"btn btn-primaryTheme\" href=\"javascript:void(0);\" [disabled]=\"!entityToPublish[i] || bulkAction\" (click)=\"setLocationPublishStatus(entity, false)\"\n                  *ngIf=\"j == 0 && !getStatusPublish(entity)\">\n                  un-Publish\n                </button>\n              </td>\n            </tr>\n          </ng-container>\n\n        </table>\n        <div *ngIf=\"!entityLocation.resultsFound\">\n          <h1> No Content Found</h1>\n        </div>\n\n      </div>\n      \n    </div>\n\n<div class=\"modal fade\" id=\"deleteModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"deleteModal\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-md\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n        <h2 class=\"modal-title\" id=\"exampleModalLabel\" *ngIf=\"toggleDelete\">Delete {{entityToDelete.countryName}}</h2>\n        <h2 class=\"modal-title\" id=\"exampleModalLabel\" *ngIf=\"toggleDeleteLanguage\">Delete Language</h2>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"toggleDelete\">\n        <table class=\"table-condensed table-custom\">\n          <thead>\n            <tr>\n              <th>Country</th>\n              <th>Languages</th>\n              <th >\n                <span *ngIf=\"entityLocation.module != 'Campaign'\">\n                    City\n                </span>\n                <span *ngIf=\"entityLocation.module == 'Campaign'\">\n                    Campaign\n                </span>\n              </th>\n              <th *ngIf=\"entityLocation.module != 'Campaign'\">Status</th>\n            </tr>\n          </thead>\n          <tbody>\n\n            <tr *ngFor=\"let city of entityToDelete.cities[indexCityToDelete].properties[0]; let j = index;\" [ngClass]=\"{'borderTable':j==0}\">\n              <td>\n                <p *ngIf=\"j==0\">\n                  {{ entityToDelete.countryName}}\n                </p>\n              </td>\n              <td>\n                  <span class=\"btn btn-default language-list\">{{city.language}}</span>\n              </td>\n              <td>\n                <p>\n                  {{city.campaignName || city.cityName }}\n                </p>\n              </td>\n              <td *ngIf=\"entityLocation.module != 'Campaign'\">\n                <p *ngIf=\"entityToDelete.cities[indexCityToDelete].published\">Published</p>\n                <p *ngIf=\"!entityToDelete.cities[indexCityToDelete].published\">Un-Published</p>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"toggleDeleteLanguage\">\n        Do you want to delete language       \n        <span class=\"btn btn-default language-list\">{{languageToDelete}}</span> from {{ entityToDelete.countryName }} ?\n      </div>\n      <div class=\"modal-footer\">\n        \n        <button type=\"button\" class=\"btn btn-primaryTheme\" data-dismiss=\"modal\" *ngIf=\"toggleDelete\" (click)=\"removePropertyAndSubmit(entityToDelete)\">Accept</button>\n        <button type=\"button\" class=\"btn btn-primaryTheme\" data-dismiss=\"modal\" *ngIf=\"toggleDeleteLanguage\" (click)=\"removeLangAndSubmit()\">Accept</button>\n       \n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancel</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n  <!-- pager -->\n  <ng-container *ngIf=\"paginationList.entityList.length>0\">\n    <app-pagination [init]=\"paginationList\">\n    </app-pagination>\n    <div [ngClass]=\"{'desactiveClass':entityLocation.countrySelectionSetting}\">\n\n      </div>\n  </ng-container>\n"

/***/ }),

/***/ "./src/app/components/main/countryLangBased/module-listing/module-listing.component.scss":
/***/ (function(module, exports) {

module.exports = "td p {\n  width: 100%;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n"

/***/ }),

/***/ "./src/app/components/main/countryLangBased/module-listing/module-listing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModuleListingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services__ = __webpack_require__("./src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_pagination__ = __webpack_require__("./src/app/components/utils/pagination/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_Entity__ = __webpack_require__("./src/app/models/Entity.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ModuleListingComponent = (function () {
    function ModuleListingComponent(genServices) {
        this.genServices = genServices;
        this.valueToEdit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.parseDataCamp = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.existingCountries = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.entityLocation = {
            module: '',
            locationList: [],
            locationParsed: [],
            apiService: '',
            apiServiceMulti: '',
            resultsFound: false,
            editEntity: false,
            countrySelectionSetting: false
        };
        /**
       * PAGINATION
       *
       * @private
       * @type {any[]}
       * @memberOf CampaignListComponent
       */
        this.paginationList = {
            entityList: [],
            pagedItems: [],
            pager: {},
            setPage: {},
            customNumberPages: 8
        };
        this.toggleDelete = false;
        this.toggleDeleteLanguage = false;
    }
    ModuleListingComponent.prototype.ngOnInit = function () {
        this.entityToPublish = new Array();
        this.initLocations();
    };
    /**
     * Get All Office Locations.
     *
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleListingComponent.prototype.initLocations = function () {
        var _this = this;
        this.genServices.get(this.entityLocation.apiService).subscribe(function (response) {
            // console.log(data);
            if (response.data.length > 0) {
                _this.entityLocation.resultsFound = true;
                // this.officeLocations = response.message;
                var newOfficeLocations = _this.parseLocations(response.data).then(function (newOfficeLocations) {
                    // this.officeLocations = newOfficeLocations.reverse();
                    _this.entityLocation.locationList = newOfficeLocations.reverse();
                    // console.log(newOfficeLocations);
                    _this.paginationList.entityList = _this.entityLocation.locationList;
                    if (_this.changePagination != undefined) {
                        _this.changePagination.setPage(1);
                    }
                    _this.entityLocation.resultsFound = true;
                    _this.setEntitySatusList(_this.entityLocation.locationList.length);
                    _this.existingCountries.emit();
                });
            }
            else {
                _this.entityLocation.resultsFound = false;
            }
        }, function (error) {
        });
    };
    /**
    * Instantiate properties depending of entityModule.
    *
    * @param {any} entityModule
    * @returns
    * @memberof ModuleEditingCompoaddNewLanguagenent
    */
    ModuleListingComponent.prototype.initMainInstance = function (element) {
        if (this.entityLocation.module === 'Campaign') {
            var newEntity_1 = new __WEBPACK_IMPORTED_MODULE_2__models__["Campaign"](element.countryName, element.countryCode);
            element.campaigns.forEach(function (entityCampaign) {
                var properties = [];
                properties[0] = entityCampaign.properties;
                newEntity_1.cities.push(new __WEBPACK_IMPORTED_MODULE_2__models__["CampaignBasedModel"](entityCampaign.name, properties));
            });
            return newEntity_1;
        }
        return new __WEBPACK_IMPORTED_MODULE_4__models_Entity__["c" /* Entity */](element.countryName, element.countryCode, element.cities);
    };
    /**
      * Will create/update an office location status
      *
      * @param {any} officeLocation
      *
      * @memberOf OfficeLocationComponent
      */
    ModuleListingComponent.prototype.onSubmit = function (entityLocation) {
        var _this = this;
        this.genServices.create(entityLocation, this.entityLocation.apiService).subscribe(function (response) {
            // console.log(response);
            _this.initLocations();
            // this.refreshLocations(response.data)
        }, function (error) {
        }, function () {
            // 
        });
    };
    /**
     * Will delete an specific country with all it's locations.
     *
     * @param {any} officeLocation
     *
     * @memberOf ModuleListingComponent
     */
    ModuleListingComponent.prototype.onDelete = function (entityLocation) {
        var _this = this;
        this.genServices.delete(entityLocation.countryCode, this.entityLocation.apiService).subscribe(function (response) {
            _this.initLocations();
        }, function (error) {
        }, function () {
            // EVENT EMITER TO SEARCH FILTER
            // this.searchFilterComponent.onSearch('');
        });
    };
    /**
     * Will Submit/Replace more than one location.
     *
     * @param {any} [officeLocations=Array<OfficeLocation>()]
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleListingComponent.prototype.onSubmitMultiLocations = function (locationList) {
        if (locationList === void 0) { locationList = new Array(); }
        this.genServices.create(locationList, this.entityLocation.apiServiceMulti).subscribe(function (response) {
            console.log(response);
        }, function (error) {
        }, function () {
            // EVENT EMITER TO SEARCH FILTER
            // this.searchFilterComponent.onSearch('');
        });
    };
    /**
    * Helper to sort an Array by Country
    *
    * @param {MainEntity} countryA
    * @param {MainEntity} countryB
    * @returns {number}
    *
    * @memberOf ModuleListingComponent
    */
    ModuleListingComponent.prototype.compareByCountry = function (countryA, countryB) {
        if (countryA.countryName < countryB.countryName) {
            return -1;
        }
        if (countryA.countryName > countryB.countryName) {
            return 1;
        }
        return 0;
    };
    /**
     * Set entity status checkbox to false (unselected)
     *
     * @param {any} sizeList
     *
     * @memberOf ModuleListingComponent
     */
    ModuleListingComponent.prototype.setEntitySatusList = function (sizeList) {
        for (var i = 0; i < sizeList; i++) {
            this.entityToPublish.push(false);
        }
    };
    /**
     *
     *
     * @param {any} entityLocations
     * @memberof ModuleListingComponent
     */
    ModuleListingComponent.prototype.refreshLocations = function (locationList) {
        return __awaiter(this, void 0, void 0, function () {
            var newLocationList;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(locationList);
                        this.indexCityToDelete = 0;
                        return [4 /*yield*/, this.parseLocations(locationList)];
                    case 1:
                        newLocationList = _a.sent();
                        this.entityLocation.locationList = newLocationList;
                        this.paginationList.entityList = newLocationList;
                        if (this.changePagination != undefined) {
                            this.changePagination.setPage(1);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     *
     *
     * @param {any} locationFromDB
     * @returns {Promise<Array<OfficeLocation>>}
     * @memberof ModuleListingComponent
     */
    ModuleListingComponent.prototype.parseLocations = function (locationFromDB) {
        var _this = this;
        return new Promise(function (resolve, promse) {
            var newOfficeLocations = new Array();
            locationFromDB.forEach(function (element) {
                newOfficeLocations.push(_this.initMainInstance(element));
            });
            if (newOfficeLocations.length == locationFromDB.length) {
                console.log(newOfficeLocations);
                resolve(newOfficeLocations);
            }
        });
    };
    /**
     * Event upon select input. This will toggle published property
     *
     * @param {any} event
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleListingComponent.prototype.changePublishStatus = function (event) {
        var _this = this;
        // console.log(this.paginationList.entityList);
        this.changeStatusForAll(event.srcElement.value == 'true').then(function (locationList) {
            _this.onSubmitMultiLocations(locationList);
        });
    };
    /**
     * Asynchronous to change officeLocation Published status.
     *
     * @param {any} published
     * @returns
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleListingComponent.prototype.changeStatusForAll = function (published) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var newofficeStatus = new Array();
            _this.entityLocation.locationList.forEach(function (location) {
                location.cities.forEach(function (location) {
                    location.published = published;
                });
                newofficeStatus.push(location);
            });
            if (newofficeStatus.length == _this.entityLocation.locationList.length) {
                resolve(newofficeStatus);
            }
        });
    };
    /**
   * Delete an entity with all it's properties.
   *
   * @param {any} officeLocation
   *
   * @memberOf OfficeLocationComponent
   */
    ModuleListingComponent.prototype.selectEntityToDelete = function (entity) {
        this.entityToDelete = entity;
        this.toggleDelete = true;
        this.toggleDeleteLanguage = false;
        // console.log(entity);
    };
    /**
   * Delete an entity with all it's properties.
   *
   * @param {any} officeLocation
   *
   * @memberOf OfficeLocationComponent
   */
    ModuleListingComponent.prototype.selectPropertyToDelete = function (entity, indexCity) {
        console.log(entity.cities[indexCity]);
        this.countryToDelete = entity.countryName;
        this.entityToDelete = entity;
        this.indexCityToDelete = indexCity;
        this.toggleDelete = true;
        this.toggleDeleteLanguage = false;
        // console.log(entity);
    };
    /**
     * Delete a language from a Location.
     *
     * @param {any} officeLocation
     * @param {any} location
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleListingComponent.prototype.selectLanguageToDelete = function (entity, language, indexCity) {
        // console.log(entity);
        this.indexCityToDelete = indexCity;
        this.entityToDelete = entity;
        this.languageToDelete = language;
        this.toggleDeleteLanguage = true;
        this.toggleDelete = false;
    };
    /**
     *This will take the property with the language selected and delete it from the entity
     *
     * @memberof ModuleListingComponent
     */
    ModuleListingComponent.prototype.removeLangAndSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(this.entityToDelete);
                        this.entityToDelete.cities[this.indexCityToDelete].properties.forEach(function (office) {
                            var indexToDelete;
                            for (var i = 0; i < office.length; i++) {
                                if (office[i].language == _this.languageToDelete) {
                                    indexToDelete = i;
                                }
                            }
                            office.splice(indexToDelete, 1);
                        });
                        if (!(this.entityLocation.module == 'Campaign')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.parseDataCamp.emit(this.entityToDelete)];
                    case 1:
                        _a.sent();
                        this.onSubmit(this.entityLocation.locationParsed);
                        return [3 /*break*/, 3];
                    case 2:
                        this.onSubmit(this.entityToDelete);
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ModuleListingComponent.prototype.removePropertyAndSubmit = function (entityToDelete) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(this.indexCityToDelete);
                        if (!(entityToDelete.cities.length > 1)) return [3 /*break*/, 4];
                        entityToDelete.cities.splice(this.indexCityToDelete, 1);
                        this.indexCityToDelete = 0;
                        if (!(this.entityLocation.module == 'Campaign')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.parseDataCamp.emit(this.entityToDelete)];
                    case 1:
                        _a.sent();
                        this.onSubmit(this.entityLocation.locationParsed);
                        return [3 /*break*/, 3];
                    case 2:
                        this.onSubmit(this.entityToDelete);
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        this.onDelete(entityToDelete);
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Set to publish an office location property.
     *
     * @param {any} officeLocation
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleListingComponent.prototype.setLocationPublishStatus = function (entityLocation, publishStatus) {
        if (publishStatus === void 0) { publishStatus = true; }
        // console.log(entityLocation);
        entityLocation.cities.forEach(function (location) {
            location.published = publishStatus;
        });
        this.onSubmit(entityLocation);
    };
    /**
     * This will enable to show publish/un-published button.
     *
     * @param {OfficeLocation} officeLocation
     * @returns
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleListingComponent.prototype.getStatusPublish = function (entityLocation) {
        var publishButton;
        publishButton = entityLocation.cities.map(function (location) { return !location.published; });
        return publishButton.indexOf(true) > -1;
    };
    /**
     * Togle selection from all country status.
     *
     *
     * @memberOf OfficeLocationComponent
     */
    ModuleListingComponent.prototype.toggleStatusSelection = function () {
        for (var i = 0; i < this.entityToPublish.length; i++) {
            this.entityToPublish[i] = this.bulkAction;
        }
    };
    ModuleListingComponent.prototype.editEntity = function (newLocationToEdit, cityIndex, countryIndex) {
        this.valueToEdit.emit({ newLocationToEdit: newLocationToEdit, cityIndex: cityIndex, countryIndex: countryIndex });
    };
    return ModuleListingComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('valueToEdit'),
    __metadata("design:type", Object)
], ModuleListingComponent.prototype, "valueToEdit", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('parseDataCamp'),
    __metadata("design:type", Object)
], ModuleListingComponent.prototype, "parseDataCamp", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('existingCountries'),
    __metadata("design:type", Object)
], ModuleListingComponent.prototype, "existingCountries", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('locationInput'),
    __metadata("design:type", Object)
], ModuleListingComponent.prototype, "entityLocation", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3__utils_pagination__["a" /* PaginationComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__utils_pagination__["a" /* PaginationComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__utils_pagination__["a" /* PaginationComponent */]) === "function" && _a || Object)
], ModuleListingComponent.prototype, "changePagination", void 0);
ModuleListingComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-module-listing',
        template: __webpack_require__("./src/app/components/main/countryLangBased/module-listing/module-listing.component.html"),
        styles: [__webpack_require__("./src/app/components/main/countryLangBased/module-listing/module-listing.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services__["c" /* GenericService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services__["c" /* GenericService */]) === "function" && _b || Object])
], ModuleListingComponent);

var _a, _b;
//# sourceMappingURL=module-listing.component.js.map

/***/ }),

/***/ "./src/app/components/main/countryLangBased/panel-creation/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__panel_creation_component__ = __webpack_require__("./src/app/components/main/countryLangBased/panel-creation/panel-creation.component.ts");
/* unused harmony namespace reexport */

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/countryLangBased/panel-creation/panel-creation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-3 col-xs-3 header-content\">\n      <h3 class=\"header-Panel\">Current {{nameModule}}</h3>\n    </div>\n    <div class=\"dropdown pull-right\" *ngIf=\"!entityLocation.countrySelectionSetting\">\n      <button class=\"add-btn btn btn-primaryTheme dropdown-toggle pull-right\" type=\"button\" data-toggle=\"dropdown\">  {{buttonName}}\n      <span class=\"caret\"></span></button>\n      <ul class=\"dropdown-menu\">\n        <li><a href=\"javascript:void(0);\"  *ngIf=\"countryField\" (click)=\"toggleCountrySelection()\">New Country</a></li>\n        <li><a href=\"javascript:void(0);\" *ngIf=\"textField\" (click)=\"toggleCitySelection()\">New {{textLabel}}</a></li>\n      </ul>\n    </div>\n    <div class=\"pull-right\">\n\n      <a href=\"javascript:void(0);\" (click)=\"togglePanel()\" class=\"btn btn-primaryTheme add-btn-newEntity\" *ngIf=\"entityLocation.countrySelectionSetting\">\n        Submit\n      </a>\n    </div>\n    <div id=\"demo\" class=\"collapse-Selection\" *ngIf=\"entityLocation.countrySelectionSetting\">\n      <h4>{{titleSeting}}</h4>\n\n        \n      <div class=\"setting-margin\">\n\n\n          <div class=\"form-group\" *ngIf=\"languageField\">\n              <label for=\"\">Language</label>\n              <select name=\"\" [(ngModel)]=\"newLanguageToEdit\" id=\"\" class=\"form-control  Select-Input rightSelect-Style\">\n                  <option selected disabled>Select New Language</option>\n                  <option value={{language}} *ngFor=\"let language of languageList;\">\n                    {{language}}\n                  </option>\n                </select>\n          </div>\n      \n        <div class=\"form-group\" *ngIf=\"countryField\">\n            <label for=\"\">Country</label>  \n            <input *ngIf=\"newCountryPannel\" list=\"countries\" [(ngModel)]=\"newCountryToEdit\" placeholder=\"Select New Country\" class=\"form-control  Select-Input rightSelect-Style\">\n            <datalist id=\"countries\">\n              <option value={{country}} *ngFor=\"let country of availableSelectionCountries;\">\n            </datalist>\n\n\n            <select *ngIf=\"!newCountryPannel\"  name=\"\" [(ngModel)]=\"newCountryToEdit\" id=\"\" class=\"form-control  Select-Input rightSelect-Style\">\n                <option selected disabled>Select New Country</option>\n                <option value={{country}} *ngFor=\"let country of existingCountries;\">\n                  {{country}}\n                </option>\n            </select>\n        </div>\n     \n\n\n          <div class=\"form-group\" *ngIf=\"textField\">\n            <label for=\"\">{{textLabel}}</label>\n              <input type=\"text\"\n              placeholder=\"{{textLabel}}\" \n              class=\"form-control\"\n              [(ngModel)]=\"newTextToEdit\"> \n          </div>\n          <alert></alert>\n \n        <a href=\"javascript:void(0);\" (click)=\"entityLocation.countrySelectionSetting=false\" class=\"btn btn-default setting-margin\">\n          Cancel\n        </a>\n        <!-- (click)=\"addNewCountry()\"  -->\n        <button href=\"javascript:void(0);\" class=\"btn btn-primaryTheme setting-margin\" (click)=\"addNewCountry()\">\n            Submit\n        </button>\n\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/components/main/countryLangBased/panel-creation/panel-creation.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/main/countryLangBased/panel-creation/panel-creation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PanelCreationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services__ = __webpack_require__("./src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PanelCreationComponent = (function () {
    function PanelCreationComponent(genServices, alertService) {
        this.genServices = genServices;
        this.alertService = alertService;
        this.entityLocation = {
            locationList: [],
            customProperties: {},
            apiService: '',
            apiServiceMulti: '',
            resultsFound: false,
            editEntity: false,
            countrySelectionSetting: false
        };
        this.languageList = [];
        this.countryList = [];
        this.countryField = false;
        this.textField = false;
        this.textLabel = "Name";
        this.languageField = false;
        this.buttonName = "Setting";
        this.nameModule = "Locations";
        this.newCountryEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.newCountryToEdit = 'Select New Country';
        this.existingCountries = new Array();
        this.newLanguageToEdit = 'Select New Language';
        this.newCountryPannel = false;
        this.newCityPannel = false;
        this.existingCities = [];
    }
    PanelCreationComponent.prototype.ngOnInit = function () {
        this.initExistingCountries();
    };
    PanelCreationComponent.prototype.initExistingCountries = function () {
        var _this = this;
        this.genServices.get(this.entityLocation.apiService).subscribe(function (response) {
            _this.existingCountries = response.data.map(function (location) { return location.countryName; });
            _this.existingLocations = response.data;
            if (_this.nameModule != 'Campaigns') {
                _this.existingLocations.forEach(function (location) {
                    location.cities.forEach(function (cityLoc) {
                        _this.existingCities.push(cityLoc.city.toLowerCase().replace(/\s/g, ""));
                        cityLoc.properties[0].forEach(function (office) {
                            _this.existingCities.push(office.cityName.toLowerCase().replace(/\s/g, ""));
                        });
                    });
                });
            }
        }, function (error) {
        }, function () {
        });
    };
    /**
     * Add a new country with all it's default properties.
     *
     *
     * @memberOf OfficeLocationComponent
     */
    PanelCreationComponent.prototype.addNewCountry = function () {
        var _this = this;
        if (!this.validForm()) {
            var newLocationToEdit = this.existingLocations.filter(function (location) { return location.countryName == _this.newCountryToEdit; });
            this.newCountryEvent.emit({
                newCountryToEdit: this.newCountryToEdit,
                newLanguageToEdit: this.newLanguageToEdit,
                newTextToEdit: this.newTextToEdit,
                newLocationToEdit: newLocationToEdit[0]
            });
            this.newCountryToEdit = '';
        }
        console.log("error");
    };
    /**
     * Disabled add new Country Setting if there is not
     * Country/Language selected.
     *
     * @returns
     *
     * @memberOf OfficeLocationComponent
     */
    PanelCreationComponent.prototype.validForm = function () {
        var errorArray = [];
        if (this.newLanguageToEdit == '' || this.newLanguageToEdit == 'Select New Language') {
            errorArray.push("Select one of the available Languages");
        }
        if (this.availableSelectionCountries.indexOf(this.newCountryToEdit) < 0 ||
            !this.hasValue(this.newCountryToEdit) ||
            this.newCountryToEdit == 'Select New Language') {
            errorArray.push("Select one of the available Countries");
        }
        if (this.nameModule != 'Campaigns') {
            if (!this.hasValue(this.newTextToEdit)) {
                errorArray.push(this.textLabel + " is required");
            }
            else {
                if (this.existingCities.indexOf(this.newTextToEdit.toLowerCase().replace(/\s/g, "")) > -1) {
                    errorArray.push(this.newTextToEdit + " already exist");
                }
            }
        }
        if (errorArray.length > 0) {
            this.alertService.errorArray(errorArray);
        }
        return errorArray.length > 0;
    };
    PanelCreationComponent.prototype.hasValue = function (value) {
        if (value !== undefined && value !== null) {
            return value.replace(/\s/g, "") != '';
        }
    };
    /**
     * Once add New Office Location is clicked
     * It change @param countrySelectionSetting variable to the opposite value (true -> false or false->true)
     * In order to open/close dropdown
     *
     * @memberOf OfficeLocationComponent
     */
    PanelCreationComponent.prototype.toggleCountrySelection = function () {
        var _this = this;
        this.togglePanel();
        this.newCountryPannel = true;
        var countriesArray = this.countryList.map(function (country) { return country.countryName; });
        this.titleSeting = "New Country";
        this.newCountryToEdit = "";
        if (this.existingCountries.length > 0) {
            this.availableSelectionCountries = countriesArray
                .filter(function (country) { return _this.existingCountries.indexOf(country) < 0; });
        }
        else {
            this.availableSelectionCountries = countriesArray;
        }
    };
    /**
     *
     *
     * @memberof PanelCreationComponent
     */
    PanelCreationComponent.prototype.toggleCitySelection = function () {
        this.newCountryPannel = false;
        this.togglePanel();
        var countriesArray = this.countryList.map(function (country) { return country.countryName; });
        this.newCountryToEdit = "Select New Country";
        this.titleSeting = "New " + this.textLabel;
        this.availableSelectionCountries = countriesArray;
    };
    PanelCreationComponent.prototype.toggleCampaignSelection = function () {
    };
    PanelCreationComponent.prototype.togglePanel = function () {
        this.entityLocation.countrySelectionSetting = !this.entityLocation.countrySelectionSetting;
    };
    return PanelCreationComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('locationInput'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "entityLocation", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('languageList'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "languageList", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('countryList'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "countryList", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('countryField'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "countryField", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('textField'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "textField", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('textLabel'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "textLabel", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('languageField'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "languageField", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('buttonName'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "buttonName", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('nameModule'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "nameModule", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('addNewCountry'),
    __metadata("design:type", Object)
], PanelCreationComponent.prototype, "newCountryEvent", void 0);
PanelCreationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-panel-creation',
        template: __webpack_require__("./src/app/components/main/countryLangBased/panel-creation/panel-creation.component.html"),
        styles: [__webpack_require__("./src/app/components/main/countryLangBased/panel-creation/panel-creation.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services__["c" /* GenericService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services__["c" /* GenericService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services__["a" /* AlertService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services__["a" /* AlertService */]) === "function" && _b || Object])
], PanelCreationComponent);

var _a, _b;
//# sourceMappingURL=panel-creation.component.js.map

/***/ }),

/***/ "./src/app/components/main/emailsetting/emailsetting.component.html":
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-1\" style=\"text-align:center;\">\n      <a href=\"#\" routerLink=\"/\" >Back</a>\n    </div>\n    <div class=\"col-sm-1\">\n      <i class=\"fa fa-envelope-o fa-4x\" aria-hidden=\"true\"></i>\n    </div>\n    <div class=\"col-sm-4\">\n      <h4>Email Us Setting</h4>\n    </div>\n  </div>\n</div>\n<br>\n<app-search-filter [searchInput]=\"searchFilter\" (changeView)=\"getFromSearch($event);\">\n</app-search-filter>\n\n<div class=\"container-fluid borderFluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-3 col-xs-3\">\n      <h3 class=\"header-Panel\">Current Emails</h3>\n    </div>\n    <div class=\"pull-right\">\n      <a href=\"javascript:void(0);\" (click)=\"emailCreationSetting=true\" class=\"add-btn btn btn-primaryTheme\" *ngIf=\"!emailCreationSetting\">\n            Add New Email\n            </a>\n\n      <a href=\"javascript:void(0);\" (click)=\"emailCreationSetting=false\" class=\"btn btn-primaryTheme add-btn-newEntity\" *ngIf=\"emailCreationSetting\">\n            Add New Email\n            </a>\n    </div>\n    <div id=\"demo\" class=\"collapse-Selection\" *ngIf=\"emailCreationSetting\">\n      <h4>Settings</h4>\n      <form name=\"emailForm\"\n      #emailForm=\"ngForm\">\n      <div class=\"setting-margin\">\n \n              <div class=\"row\">\n                <div class=\"col-sm-7\">\n                  <div class=\"form-group\"\n                    [ngClass]=\"{ 'has-error': !validSettingForm() && email.touched}\">\n                    <label class=\"filterLabel\" for=\"\">Email</label>\n                    <br>\n                    <input type=\"email\" \n                      [(ngModel)]=\"emailToCreate.email\" \n                      #email=\"ngModel\"\n                      name=\"email\"\n                      placeholder=\"example@email.com\" \n                      class=\"form-control\" required>\n                      <div *ngIf=\" !validSettingForm()  && email.touched\" class=\"valdMssg help-block\">\n                       Tip: example@email.com\n                      </div>\n                  </div>\n                </div>\n                <div class=\"col-sm-5\">\n                  <label class=\"filterLabel\" for=\"status\">Status</label>\n                  <br>\n                  <select [(ngModel)]=\"emailToCreate.published\" name=\"published\" (change)=\"toggleStatus(emailToCreate, $event)\" id=\"\" class=\"form-control\" required>\n                    <option value='true'>Published</option>\n                    <option selected value='false'>Un-Published</option>\n                    </select>\n                </div>\n              </div>\n      \n        <br>\n        <a href=\"javascript:void(0);\" (click)=\"emailCreationSetting=false\" class=\"btn btn-default setting-margin\">\n            Cancel\n          </a>\n\n        <button type=\"submit\" href=\"javascript:void(0);\" class=\"btn btn-primaryTheme setting-margin\" (click)=\"addNewEmail(emailToCreate)\" [disabled]=\"!validSettingForm()\">\n              Add New Email\n          </button>\n\n      </div>\n    </form>\n    </div>\n  </div>\n  <div class=\"row list-Component\">\n    <div class=\"col-sm-12\">\n      <table class=\"table-condensed table-custom\" *ngIf=\"resultsFound\">\n        <thead>\n          <tr>\n            <th>\n              Email\n            </th>\n            <th>\n              Status\n            </th>\n            <th colspan=\"2\">\n              Option\n            </th>\n            <th class=\"text-center\">\n              <!-- <label class=\"checkbox-inline table-checkIn\"><input type=\"checkbox\" value=\"\" [(ngModel)]=\"bulkAction\" (change)=\"toggleDeleteSelection()\"></label> -->\n              <select name=\"\" id=\"\" [(ngModel)]=\"selectOptions\" (change)=\"deleteSelectedFiles($event)\" class=\"select-Input table-select-Input\">\n              <option selected disabled>Bulk Action</option>\n              <option  *ngIf=\"multipleEmail.length > 0\" value='true'>Publish All</option>\n              <!-- <option *ngIf=\"bulkAction\"  value='false'e>Delete All</option> -->\n            </select>\n            </th>\n          </tr>\n        </thead>\n        <tr *ngFor=\"let contactUs of paginationList.pagedItems; let i = index;\">\n          <td>\n            <p>\n              {{ contactUs.email }}\n            </p>\n\n          </td>\n          <!-- *ngFor=\"let property of officeLocations.propertyModel\"> -->\n          <td>\n            <p *ngIf=\"contactUs.published\">Published</p>\n            <p *ngIf=\"!contactUs.published\">Un-Published</p>\n          </td>\n          <td>\n            <a href=\"#\" data-toggle=\"modal\" data-target=\"#editEmailModal\" (click)=\"editEmail(contactUs)\">\n                <span class=\"default-btn\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>\n               Edit\n             </span></a>\n          </td>\n          <td>\n            <a href=\"#\" data-toggle=\"modal\" data-target=\"#editEmailModal\" (click)=\"selectEmailToDelete(contactUs)\">\n                <span class=\"default-btn\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i>\n               Delete\n             </span></a>\n\n          </td>\n          <td class=\"text-center\">\n            <label class=\"checkbox-inline table-checkIn\"><input type=\"checkbox\" value=\"\" [(ngModel)]=\"mailToPublish[i]\"  [disabled]=\"bulkAction\"></label>\n          </td>\n        </tr>\n      </table>\n\n    \n      <div *ngIf=\"!resultsFound\">\n        <h1> No Content Found</h1>\n      </div>\n    </div>\n  </div>\n  <div [ngClass]=\"{'desactiveClass':emailCreationSetting}\"></div>\n</div>\n<div class=\"modal fade\" id=\"editEmailModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"locationModal\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-md\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n              <span aria-hidden=\"true\">&times;</span>\n           </button>\n           <h2 class=\"modal-title text-center\" id=\"exampleModalLabel\" *ngIf=\"editView\">Edit Email</h2>\n          <h2 class=\"modal-title text-center\" id=\"exampleModalLabel\" *ngIf=\"!editView\">Delete Email</h2>\n        </div>\n        <div class=\"modal-body\" *ngIf=\"editView && emailToEdit\">\n            <form>\n                <div class=\"row\">\n                  <div class=\"col-sm-5 col-sm-offset-2\">\n                    <div class=\"form-group\">\n                      <label class=\"filterLabel\" for=\"\">Email</label>\n                      <br>\n                      <input type=\"email\" [(ngModel)]=\"emailToEdit.email\" name=\"email\" class=\"form-control\">\n                    </div>\n                  </div>\n                  <div class=\"col-sm-3\">\n                    <label class=\"filterLabel\" for=\"status\">Status</label>\n                    <br>\n                    <select [(ngModel)]=\"emailToEdit.published\" name=\"Published\" id=\"\" (change)=\"toggleStatus(emailToEdit, $event)\" class=\"form-control\">\n                      <option selected value=\"\" disabled>Bulk Action</option>  \n                      <option value='true'>Published</option>\n                        <option value='false'>Un-Published</option>\n                      </select>\n                  </div>\n                </div>\n                <br>\n                <div class=\"row\">\n                  <div class=\"text-center\">\n                    <button type=\"submit\" data-dismiss=\"modal\" class=\"btn btn-primaryTheme\" (click)=\"onSubmit(emailToEdit)\">Submit</button>\n                  </div>\n                </div>\n              </form>\n        </div>\n        \n        <div class=\"modal-body text-center\" *ngIf=\"!editView && emailToDelete\">\n            <p>\n                Do you want to delete this email ?\n            </p> \n            <h4 > \n          {{emailToDelete.email}}\n          </h4>\n       \n      \n        </div>\n        <div class=\"modal-footer\" *ngIf=\"!editView\">\n          <button type=\"button\" class=\"btn btn-primaryTheme\" data-dismiss=\"modal\" (click)=\"onDelete([fileModalView])\">Accept</button>\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancel</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n<!-- pager -->\n<!-- pager -->\n<ng-container *ngIf=\"paginationList.entityList.length>0\">\n    <app-pagination [init]=\"paginationList\">\n    </app-pagination>\n</ng-container>"

/***/ }),

/***/ "./src/app/components/main/emailsetting/emailsetting.component.scss":
/***/ (function(module, exports) {

module.exports = ".modal {\n  top: 20%; }\n"

/***/ }),

/***/ "./src/app/components/main/emailsetting/emailsetting.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailsettingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_index__ = __webpack_require__("./src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_index__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_pagination_pagination_component__ = __webpack_require__("./src/app/components/utils/pagination/pagination.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_search_filter__ = __webpack_require__("./src/app/components/utils/search-filter/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};






var EmailsettingComponent = (function () {
    function EmailsettingComponent(fb, genServices, alertService, constants) {
        this.fb = fb;
        this.genServices = genServices;
        this.alertService = alertService;
        this.constants = constants;
        this.locationToEdit = { edition: false, emailList: new __WEBPACK_IMPORTED_MODULE_3__models_index__["ContactUs"]() };
        this.searchFilter = {
            apiRest: '',
            availableSearch: {
                searchStatus: true,
                searchName: true,
                searchCountry: false,
                searchLanguage: false
            },
            searchStatus: true,
            countryFromServer: [],
            languageArray: []
        };
        /**
          * PAGINATION
          *
          * @private
          * @type {any[]}
          * @memberOf CampaignListComponent
          */
        this.paginationList = {
            entityList: [],
            pagedItems: [],
            pager: {},
            customNumberPages: 20
        };
        this.rootDir = constants.root_dir;
        this.apiRest = constants.IApiRest;
        this.searchFilter.apiRest = this.apiRest.API_EMAILUS;
        this.multipleEmail = [];
        this.resultsFound = false;
        this.emailToEdit = new __WEBPACK_IMPORTED_MODULE_3__models_index__["ContactUs"]();
        this.emailToCreate = new __WEBPACK_IMPORTED_MODULE_3__models_index__["ContactUs"]();
        this.emailToSearch = new __WEBPACK_IMPORTED_MODULE_3__models_index__["ContactUs"]();
        this.emails = [];
    }
    EmailsettingComponent.prototype.getFromSearch = function (emailFound) {
        this.locationToEdit.edition = false;
        if (emailFound.value.length > 0) {
            this.emails = emailFound.value;
            this.paginationList.entityList = this.emails;
            this.resultsFound = true;
            if (this.changePagination != undefined) {
                this.changePagination.setPage(1);
            }
        }
        else {
            this.resultsFound = false;
        }
    };
    EmailsettingComponent.prototype.ngOnInit = function () {
        this.initForm();
        this.initEmailus();
        this.selectOptions = 'Bulk Action';
        this.emailToEdit.published = "false";
        this.emailToCreate.published = "false";
    };
    EmailsettingComponent.prototype.initForm = function () {
        this.emailForm = this.fb.group({
            email: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](''),
            published: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](''),
            publishedEdit: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('')
        });
    };
    /**
     * This get all Emails and save it in
     * @param emails
     * After that it will initialize entity list for pagination component
     *
     * @memberOf EmailsettingComponent
     */
    EmailsettingComponent.prototype.initEmailus = function () {
        var _this = this;
        this.genServices.get(this.apiRest.API_EMAILUS).subscribe(function (response) {
            if (response.data.length > 0) {
                _this.emails = response.data;
                _this.mailToPublish = new Array(_this.emails.length);
                _this.paginationList.entityList = _this.emails;
                if (_this.changePagination != undefined) {
                    _this.changePagination.setPage(1);
                }
                _this.resultsFound = true;
            }
            else {
                _this.resultsFound = false;
            }
        }, function (error) {
        }, function () {
        });
    };
    /**
     * This will create/update an email.
     *
     *
     * @memberOf EmailsettingComponent
     */
    EmailsettingComponent.prototype.onSubmit = function (contactUs) {
        var _this = this;
        // console.log(contactUs);
        contactUs.email = contactUs.email.toLowerCase();
        this.genServices.create(contactUs, this.apiRest.API_EMAILUS).subscribe(function (response) {
            _this.emailCreationSetting = false;
            _this.onSearch(new __WEBPACK_IMPORTED_MODULE_3__models_index__["ContactUs"]());
        }, function (error) {
        }, function () {
        });
    };
    /**
     * This will delete an email from list.
     *
     *
     * @memberOf EmailsettingComponent
     */
    EmailsettingComponent.prototype.onDelete = function () {
        var _this = this;
        this.genServices.delete(this.emailToDelete.email, this.apiRest.API_EMAILUS).subscribe(function (response) {
            _this.emails.splice(_this.emails.indexOf(_this.emailToDelete), 1);
        }, function (error) {
        }, function () {
            _this.onSearch("");
        });
    };
    EmailsettingComponent.prototype.onSearch = function (contactus) {
        var _this = this;
        var valueToSearch = new __WEBPACK_IMPORTED_MODULE_3__models_index__["ContactToSearch"]();
        if (contactus.email != '' && contactus.published != undefined) {
            valueToSearch.email = contactus.email.toLowerCase();
        }
        if (contactus.published != '' && contactus.published != undefined) {
            valueToSearch.published = contactus.published == 'true';
        }
        // console.log(valueToSearch);
        this.genServices.getBySearch(this.apiRest.API_EMAILUS, valueToSearch).subscribe(function (response) {
            if (response.data.length > 0) {
                _this.emails = response.data;
                _this.mailToPublish = new Array(_this.emails.length);
                _this.paginationList.entityList = _this.emails;
                if (_this.changePagination != undefined) {
                    _this.changePagination.setPage(1);
                }
                _this.resultsFound = true;
            }
            else {
                _this.resultsFound = false;
            }
        }, function (error) {
        }, function () {
        });
    };
    /**
     * Once the modal is toggle this will add value selected
     * into @param emailToEdit
     *
     * @param {any} emailToEdit
     *
     * @memberOf EmailsettingComponent
     */
    EmailsettingComponent.prototype.editEmail = function (_a) {
        var emailToEdit = __rest(_a, []);
        this.editView = true;
        this.emailToEdit = emailToEdit;
    };
    /**
     *Once the modal is toggle this will add value selected
     * into @param emailToDelete
     * @param {any} emailToDelete
     *
     * @memberOf EmailsettingComponent
     */
    EmailsettingComponent.prototype.selectEmailToDelete = function (emailToDelete) {
        this.editView = false;
        this.emailToDelete = emailToDelete;
    };
    EmailsettingComponent.prototype.toggleStatus = function (emailToEdit, event) {
        emailToEdit.published = event.srcElement.value == 'true';
    };
    EmailsettingComponent.prototype.validSettingForm = function () {
        var validation = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return validation.test(this.emailToCreate.email);
    };
    EmailsettingComponent.prototype.addNewEmail = function (emailToCreate) {
        this.emails.push(emailToCreate);
        this.onSubmit(new __WEBPACK_IMPORTED_MODULE_3__models_index__["ContactUs"](emailToCreate.email, emailToCreate.published));
    };
    return EmailsettingComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('init'),
    __metadata("design:type", Object)
], EmailsettingComponent.prototype, "locationToEdit", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4__utils_pagination_pagination_component__["a" /* PaginationComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__utils_pagination_pagination_component__["a" /* PaginationComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__utils_pagination_pagination_component__["a" /* PaginationComponent */]) === "function" && _a || Object)
], EmailsettingComponent.prototype, "changePagination", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('searchInput'),
    __metadata("design:type", Object)
], EmailsettingComponent.prototype, "searchFilter", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__utils_search_filter__["a" /* SearchFilterComponent */]),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__utils_search_filter__["a" /* SearchFilterComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__utils_search_filter__["a" /* SearchFilterComponent */]) === "function" && _b || Object)
], EmailsettingComponent.prototype, "searchFilterComponent", void 0);
EmailsettingComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-emailsetting',
        template: __webpack_require__("./src/app/components/main/emailsetting/emailsetting.component.html"),
        styles: [__webpack_require__("./src/app/components/main/emailsetting/emailsetting.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_index__["c" /* GenericService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_index__["c" /* GenericService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services_index__["a" /* AlertService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_index__["a" /* AlertService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__models_index__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__models_index__["Constants"]) === "function" && _f || Object])
], EmailsettingComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=emailsetting.component.js.map

/***/ }),

/***/ "./src/app/components/main/emailsetting/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__emailsetting_component__ = __webpack_require__("./src/app/components/main/emailsetting/emailsetting.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__emailsetting_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/image/image.component.html":
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-1 align-self-center\" style=\"text-align:center;\">\n      <a href=\"#\" routerLink=\"/\" >Back</a>\n    </div>\n    <div class=\"col-sm-1\">\n      <i class=\"fa fa-picture-o fa-4x\" aria-hidden=\"true\"></i>\n    </div>\n    <div class=\"col-sm-4 align-self-center\">\n      <h4>Bulk Upload Images</h4>\n    </div>\n  </div>\n</div>\n<br>\n<div class=\"container-fluid borderFluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-2\">\n      <h3 class=\"header-Panel\">Upload Image</h3>\n    </div>\n  </div>\n  <div class=\"conatiner\">\n    <div class=\"row text-center\">\n      <h1>New Image</h1>\n    </div>\n    <br>\n    <div class=\"row\">\n      <div class=\"col-sm-8 uplloadForm\">\n        <form [formGroup]=\"myForm\" (ngSubmit)=\"onSubmit(myForm.value)\">\n          <div class=\"row text-center\">\n            <div class=\"col-sm-6 col-sm-offset-4\">\n              <label class=\"btn btn-primaryTheme upload-btn\">\n                  <i class=\"fa fa-upload\" aria-hidden=\"true\"></i>     \n                  <ng-container *ngIf=\"imgSelected\">\n                    {{selectedFile.name}}\n                  </ng-container>\n                  <ng-container *ngIf=\"!imgSelected\">\n                      + New File\n                    </ng-container>\n                    <input type=\"file\" id=\"file\" class=\"upload-fileInput\" accept=\"image/x-png,image/gif,image/jpeg\" (change)=\"imageUpload($event)\">\n                </label>\n            </div>\n            <div class=\"col-sm-2\">\n              <button type=\"submit\" class=\"btn btn-primaryTheme\" [disabled]=\"!imgSelected\">Submit</button>\n            </div>\n          </div>\n        </form>\n      </div>\n      <div class=\"col-sm-2\">\n        <img [lazyLoad]=\"imageUrl\" [defaultImage]=\"defaultImage\" style=\"width:200px; height:200px\" class=\"imgSelected\" *ngIf=\"imgSelected\" />\n      </div>\n    </div>\n  </div>\n</div>\n<br>\n<div class=\"container-fluid borderFluid\">\n  <div class=\"row text-center\">\n    <div class=\"col-sm-6 col-sm-offset-3\">\n      <table class=\"table\">\n        <thead>\n          <tr>\n            <th>\n\n            </th>\n            <th colspan=\"2\">\n              <!-- <label class=\"checkbox-inline table-checkIn\"><input type=\"checkbox\" value=\"\" [(ngModel)]=\"bulkAction\" (change)=\"toggleDeleteSelection()\"></label> -->\n              <select name=\"\" id=\"\" [(ngModel)]=\"selectOptions\" (change)=\"deleteSelectedFiles($event)\" class=\"select-Input table-select-Input\">\n                  <option selected disabled>Bulk Action</option>\n                  <option  *ngIf=\"multipleFile.fileNames.length > 0\" value='true'>Delete Selected</option>\n                  <!-- <option *ngIf=\"bulkAction\"  value='false'e>Delete All</option> -->\n                </select>\n            </th>\n\n          </tr>\n          <tr>\n            <th>Preview</th>\n            <th> FileName</th>\n            <th>Options</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let file of paginationList.pagedItems; let i = index;\">\n            <td><img class=\"imgToShow\" src={{imagSource(file.filename)}} alt=\"\"></td>\n            <td>\n              <label class=\"checkbox-inline table-checkIn\"><input type=\"checkbox\" [checked]=\"isChecked(file.filename)\" value=\"\" [disabled]=\"bulkAction\" (click)=\"fileSelection($event,file.filename)\"></label>              \n              {{file.filename}}\n            </td>\n            <td>\n              <a href=\"#\" data-toggle=\"modal\" data-target=\"#deleteImageModal\" (click)=\"selectToRemoveFile(file.filename)\">\n                    <span class=\"default-btn\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i>\n                   Delete\n                 </span>\n                </a>\n              <!-- <div class=\"checkbox col-sm-md-auto\" style=\"margin-top: 2vh;\">\n                <a href=\"javascript:void(0)\" style=\"margin-right: 0.6vw; color:red;\" (click)=\"removeFile(file)\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></a>\n              </div> -->\n            </td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n  </div>\n  <div class=\"modal fade\" id=\"deleteImageModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"locationModal\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-md\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n              <span aria-hidden=\"true\">&times;</span>\n           </button>\n          <h2 class=\"modal-title text-center\" id=\"exampleModalLabel\">Delete Image</h2>\n        </div>\n        <div class=\"modal-body text-center\">\n            <div>\n                <img src={{imagSource(fileModalView)}} style=\"width:200px; height:200px\" *ngIf=\"fileModalView\"/>\n              </div>\n          Do you want to delete this image?\n     \n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-primaryTheme\" data-dismiss=\"modal\" (click)=\"onDelete([fileModalView])\">Accept</button>\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cancel</button>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<ng-container *ngIf=\"paginationList.entityList.length>0\">\n  <app-pagination [init]=\"paginationList\">\n  </app-pagination>\n</ng-container>"

/***/ }),

/***/ "./src/app/components/main/image/image.component.scss":
/***/ (function(module, exports) {

module.exports = ".align-center {\n  margin-top: 2.5vh; }\n\n.imgCenter {\n  margin: auto; }\n\n.imgSelected {\n  margin-top: -5vh; }\n\n.upload-fileInput {\n  opacity: 0;\n  position: absolute;\n  z-index: -1; }\n\n.table-checkIn input[type=checkbox] {\n  position: relative; }\n\n.imgToShow {\n  width: 200px;\n  height: 100px; }\n"

/***/ }),

/***/ "./src/app/components/main/image/image.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_pagination_pagination_component__ = __webpack_require__("./src/app/components/utils/pagination/pagination.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services__ = __webpack_require__("./src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ImageComponent = (function () {
    function ImageComponent(genericService, alertService, constants) {
        this.genericService = genericService;
        this.alertService = alertService;
        this.submitted = false;
        this.paginationList = { entityList: [], pagedItems: [], pager: {} };
        this.multipleFile = { fileNames: [] };
        /**
         * Var states in order to show/hide elements.
         *
         *
         * @memberOf ImageComponent
         */
        this.filesEdited = false;
        this.imgSelected = false;
        this.rootDir = constants.root_dir;
        this.apiRest = constants.IApiRest;
        this.files = [];
        this.multipleFile.fileNames = [];
        this.defaultImage = "assets/imageLoad.gif";
    }
    ImageComponent.prototype.ngOnInit = function () {
        this.myForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormGroup"]({
            filesSelected: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](''),
            fileName: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](''),
            image: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](''),
        });
        this.selectOptions = 'Bulk Action';
        this.initFiles();
    };
    /**
     *  Bring available files from server
     *
     *
     * @memberOf ImageComponent
     */
    ImageComponent.prototype.initFiles = function () {
        var _this = this;
        this.genericService.get(this.apiRest.API_FILES).subscribe(function (response) {
            // console.log(response);
            _this.files = response.data.reverse();
            // console.log(this.files);
            _this.paginationList.entityList = _this.files;
        }, function (error) {
        }, function () {
        });
    };
    /**
    * Once form is complete and click on submit button it will
    * action.
    *
    * @memberOf LabelComponent
    */
    ImageComponent.prototype.onSubmit = function (value) {
        var _this = this;
        if (this.selectedFile != undefined) {
            var formData = new FormData();
            formData.append(this.selectedFile.name, this.selectedFile);
            this.imgSelected = false;
            this.imageUrl = "";
            this.genericService.create(formData, this.apiRest.API_FILES).subscribe(function (response) {
                _this.files.unshift({ filename: _this.selectedFile.name });
                _this.paginationList.entityList = _this.files;
                _this.changePagination.setPage(1);
            }, function (error) {
                // this.alertService.error(error);
            }, function () {
            });
        }
    };
    /**
     *
     *
     *
     * @memberOf ImageComponent
     */
    ImageComponent.prototype.onDelete = function (_a) {
        var _this = this;
        var fileToDelete = _a.slice(0);
        // console.log(fileToDelete);
        var filesToSend = {
            fileNames: fileToDelete
        };
        this.genericService.create(filesToSend, this.apiRest.API_FILES_MULTIDELETE).subscribe(function (response) {
            // console.log(response);
            if (response.message.code == 201) {
                fileToDelete.forEach(function (elementToDelete) {
                    var index = _this.files.findIndex(function (element) { return element.filename === elementToDelete; });
                    _this.files.splice(index, 1);
                });
            }
        }, function (error) {
        }, function () {
            _this.paginationList.entityList = _this.files;
            _this.changePagination.setPage(1);
        });
    };
    ImageComponent.prototype.findInArray = function (element) {
        return element.filename == "hoi";
    };
    /**
     * Push files into an array to be remove.
     *
     * @param {any} fileSelected
     *
     * @memberOf ImageComponent
     */
    ImageComponent.prototype.selectToRemoveFile = function (fileSelected) {
        this.fileModalView = fileSelected;
        var index = this.files.indexOf(fileSelected);
        //  this.files.splice(index,1);
        this.filesEdited = true;
    };
    /**
     * Will delete an array of files.
     *
     *
     * @memberOf ImageComponent
     */
    ImageComponent.prototype.saveChanges = function () {
        this.filesEdited = false;
    };
    /**
     * Concat root to show the file.
     *
     * @param {any} fileName
     * @returns
     *
     * @memberOf ImageComponent
     */
    ImageComponent.prototype.imagSource = function (fileName) {
        return this.rootDir + this.apiRest.API_FILES + "/" + fileName;
    };
    /**
     * Once file is selected this will be save in selectedFile.
     *
     * @param {any} event
     *
     * @memberOf ImageComponent
     */
    ImageComponent.prototype.imageUpload = function (event) {
        var _this = this;
        this.imgSelected = true;
        var reader = new FileReader();
        //get the selected file from event
        this.selectedFile = event.target.files[0];
        if (this.selectedFile.type.match('image.*')) {
            reader.onloadend = function () {
                //Assign the result to variable for setting the src of image element
                _this.imageUrl = reader.result;
            };
            reader.readAsDataURL(this.selectedFile);
        }
    };
    /**
     * Change filesToDelete values to true/false
     *
     *
     * @memberOf ImageComponent
     */
    // toggleDeleteSelection() {
    //   for (var i = 0; i < this.filesToDelete.length; i++) {
    //     this.filesToDelete[i] = this.bulkAction;
    //   }
    //   this.selectOptions='Bulk Action'
    // }
    /**
     * this will push/remove fileNames selected on table.
     *
     * @param {any} event
     * @param {any} file
     *
     * @memberOf ImageComponent
     */
    ImageComponent.prototype.fileSelection = function (event, file) {
        if (event.srcElement.checked) {
            this.multipleFile.fileNames.push(file);
        }
        else {
            var index = this.multipleFile.fileNames.indexOf(file);
            this.multipleFile.fileNames.splice(index, 1);
        }
        // console.log(this.multipleFile);
    };
    /**
     * Once select option is clicked this will call
     * onDelete in order to erase multiple files.
     *
     * @param {any} event
     *
     * @memberOf ImageComponent
     */
    ImageComponent.prototype.deleteSelectedFiles = function (event) {
        if (event.srcElement.value == 'true') {
            this.onDelete(this.multipleFile.fileNames);
        }
    };
    ImageComponent.prototype.isChecked = function (fileName) {
        return this.multipleFile.fileNames.indexOf(fileName) > -1;
    };
    return ImageComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3__utils_pagination_pagination_component__["a" /* PaginationComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__utils_pagination_pagination_component__["a" /* PaginationComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__utils_pagination_pagination_component__["a" /* PaginationComponent */]) === "function" && _a || Object)
], ImageComponent.prototype, "changePagination", void 0);
ImageComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-image',
        template: __webpack_require__("./src/app/components/main/image/image.component.html"),
        styles: [__webpack_require__("./src/app/components/main/image/image.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services__["c" /* GenericService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services__["c" /* GenericService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services__["a" /* AlertService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services__["a" /* AlertService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__models__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models__["Constants"]) === "function" && _d || Object])
], ImageComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=image.component.js.map

/***/ }),

/***/ "./src/app/components/main/image/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__image_component__ = __webpack_require__("./src/app/components/main/image/image.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__image_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/office-location/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__office_location_component__ = __webpack_require__("./src/app/components/main/office-location/office-location.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__office_location_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/main/office-location/office-location.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\" *ngIf=\"!locationToEdit.edition\">\n    <div class=\"col-sm-1 back-btn\" style=\"text-align:center;\">\n      <a href=\"#\" routerLink=\"/\" >Back</a>\n    </div>\n    <div class=\"col-sm-1\">\n      <i class=\"fa fa-map-marker fa-4x\" aria-hidden=\"true\"></i>\n    </div>\n    <div class=\"col-sm-4\">\n      <h3>Office Location</h3>\n    </div>\n  </div>\n  <div class=\"row\" *ngIf=\"locationToEdit.edition\">\n    <div class=\"col-sm-1 back-btn\" style=\"text-align:center;\">\n      <a href=\"#\" (click)=\"locationToEdit.edition = false\" >Back</a>\n    </div>\n    <div class=\"col-sm-1\">\n      <i class=\"fa fa-map-marker fa-4x\" aria-hidden=\"true\"></i>\n    </div>\n    <div class=\"col-sm-6\">\n      <h3>Office Location > Edit > {{locationToEdit.entityLocation.countryName}} </h3>\n    </div>\n  </div>\n</div>\n<br>\n\n\n<ng-container *ngIf=\"!locationToEdit.edition\">\n     \n    <app-search-filter [searchInput]=\"searchFilter\" (changeView)=\"getFromSearch($event);\">\n    </app-search-filter>\n    \n    <div class=\"container-fluid borderFluid\">\n\n        <app-panel-creation \n          [countryField] = \"true\"\n          [countryList]=\"searchFilter.countryFromServer\" \n          [languageField] =\"true\"\n          [languageList] = \"searchFilter.languageArray\"\n          [textField] = \"true\"\n          [textLabel] =\"'City'\"\n          [locationInput]=\"entityLocation\"\n          (addNewCountry)=\"addNewCountry($event)\">\n      </app-panel-creation>\n\n      <app-module-listing [locationInput]=\"entityLocation\" (valueToEdit)=\"editLocation($event)\">\n      </app-module-listing>\n\n    </div>\n\n\n  <!-- Ends Panel List -->\n</ng-container>\n\n<app-module-editing \n[init]=\"locationToEdit\" \n*ngIf=\"locationToEdit.edition\" \n(newListValue)=\"getFromSearch($event);\"\n[nameModule]=\"'Call Center'\">\n</app-module-editing>\n\n<!-- office Location Edit Section  -->\n<!-- <app-office-location-edit [init]=\"locationToEdit\" *ngIf=\"locationToEdit.edition\" (newListValue)=\"getFromSearch($event);\">\n</app-office-location-edit> -->"

/***/ }),

/***/ "./src/app/components/main/office-location/office-location.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/main/office-location/office-location.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OfficeLocationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("./src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_search_filter__ = __webpack_require__("./src/app/components/utils/search-filter/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__countryLangBased_module_listing__ = __webpack_require__("./src/app/components/main/countryLangBased/module-listing/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OfficeLocationComponent = (function () {
    function OfficeLocationComponent(fb, alertService, constants) {
        this.fb = fb;
        this.alertService = alertService;
        this.constants = constants;
        this.searchFilter = {
            apiRest: '',
            availableSearch: {
                searchStatus: false,
                searchName: false,
                searchCountry: true,
                searchLanguage: true
            },
            countryFromServer: [],
            languageArray: [],
            makeAction: false
        };
        this.resultsFound = false;
        this.submitted = false;
        this.value = ["United Arab Emirates"];
        /**
        * To enable Callcenter edit directive.
        *
        * @memberof CallcenterComponent
        */
        this.locationToEdit = {
            edition: false,
            entityLocation: new __WEBPACK_IMPORTED_MODULE_3__models__["OfficeLocation"](),
            cityIndexSelected: '',
            apiService: '',
            module: ''
        };
        /**
         * Module Listing / Panel Creation Directive
         *
         * @memberof ModuleListingComponent
         */
        this.entityLocation = {
            locationList: [],
            customProperties: {},
            apiService: '',
            apiServiceMulti: '',
            resultsFound: false,
            editEntity: false,
            countrySelectionSetting: false,
        };
        /**
         * Pagination Directive
         *
         * @private
         * @type {any[]}
         * @memberOf CampaignListComponent
         */
        this.paginationList = {
            entityList: [],
            pagedItems: [],
            pager: {},
            setPage: {},
            customNumberPages: 8
        };
        this.toggleDelete = false;
        this.toggleDeleteLanguage = false;
        this.apiRest = constants.IApiRest;
        this.countriesSelected = [];
        this.officeLocations = new Array();
        this.entityLocation.apiService = this.apiRest.API_OFFICE_LOCATIONS;
        this.entityLocation.apiServiceMulti = this.apiRest.API_OFFICE_LOCATIONS_MULTI;
        this.searchFilter.apiRest = this.apiRest.API_OFFICE_LOCATIONS;
        this.officeToDelete = new __WEBPACK_IMPORTED_MODULE_3__models__["OfficeLocation"]();
        this.officeToPublish = new Array();
        this.locationToEdit = {
            edition: false,
            cityIndexSelected: '',
            entityLocation: new __WEBPACK_IMPORTED_MODULE_3__models__["OfficeLocation"](),
            apiService: this.apiRest.API_OFFICE_LOCATIONS,
            module: 'OfficeLocation'
        };
    }
    /**
    * Select an Office and set @param this.editOfficeLocation to edit it.
    *
    * @param {any} officeLocation
    *
    * @memberOf OfficeLocationComponent
    */
    OfficeLocationComponent.prototype.editLocation = function (_a) {
        var newLocationToEdit = _a.newLocationToEdit, cityIndex = _a.cityIndex;
        this.locationToEdit.edition = true;
        this.locationToEdit.cityIndexSelected = cityIndex;
        this.locationToEdit.entityLocation = newLocationToEdit;
        this.locationToEdit.apiService = this.apiRest.API_OFFICE_LOCATIONS;
        this.locationToEdit.module = 'OfficeLocation';
    };
    OfficeLocationComponent.prototype.getFromSearch = function (officeLocationsFound) {
        this.locationToEdit.edition = false;
        if (officeLocationsFound.value.length > 0) {
            this.officeLocations = officeLocationsFound.value.sort(this.compareByCountry);
            this.EntityComponent.refreshLocations(this.officeLocations);
            this.resultsFound = true;
        }
        else {
            this.resultsFound = false;
        }
    };
    OfficeLocationComponent.prototype.ngOnInit = function () {
    };
    /**
    * Helper to sort an Array of Campaigns
    *
    * @param {OfficeLocation} countryA
    * @param {OfficeLocation} countryB
    * @returns {number}
    *
    * @memberOf CampaignListComponent
    */
    OfficeLocationComponent.prototype.compareByCountry = function (countryA, countryB) {
        if (countryA.countryName < countryB.countryName) {
            return -1;
        }
        if (countryA.countryName > countryB.countryName) {
            return 1;
        }
        return 0;
    };
    /**
     * Add a new country with all it's default properties.
     *
     *
     * @memberOf OfficeLocationComponent
     */
    OfficeLocationComponent.prototype.addNewCountry = function (_a) {
        var newCountryToEdit = _a.newCountryToEdit, newLanguageToEdit = _a.newLanguageToEdit, newTextToEdit = _a.newTextToEdit, newLocationToEdit = _a.newLocationToEdit;
        var countrySelected = this.searchFilter.countryFromServer.find(function (country) { return country.countryName == newCountryToEdit; });
        if (!newLocationToEdit) {
            newLocationToEdit = new __WEBPACK_IMPORTED_MODULE_3__models__["OfficeLocation"](countrySelected.countryName, countrySelected.countryCode);
        }
        newLocationToEdit.cities.push(new __WEBPACK_IMPORTED_MODULE_3__models__["CityOffices"](newTextToEdit));
        var cityIndex = newLocationToEdit.cities.length - 1;
        var properties = [];
        properties[0] = new __WEBPACK_IMPORTED_MODULE_3__models__["OfficeProperties"](newLanguageToEdit, newTextToEdit);
        newLocationToEdit.cities[cityIndex].properties[0] = properties;
        this.editLocation({ newLocationToEdit: newLocationToEdit, cityIndex: cityIndex });
        this.entityLocation.countrySelectionSetting = false;
    };
    return OfficeLocationComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('searchInput'),
    __metadata("design:type", Object)
], OfficeLocationComponent.prototype, "searchFilter", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__countryLangBased_module_listing__["a" /* ModuleListingComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__countryLangBased_module_listing__["a" /* ModuleListingComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__countryLangBased_module_listing__["a" /* ModuleListingComponent */]) === "function" && _a || Object)
], OfficeLocationComponent.prototype, "EntityComponent", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4__utils_search_filter__["a" /* SearchFilterComponent */]),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__utils_search_filter__["a" /* SearchFilterComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__utils_search_filter__["a" /* SearchFilterComponent */]) === "function" && _b || Object)
], OfficeLocationComponent.prototype, "searchFilterComponent", void 0);
OfficeLocationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-office-location',
        template: __webpack_require__("./src/app/components/main/office-location/office-location.component.html"),
        styles: [__webpack_require__("./src/app/components/main/office-location/office-location.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["a" /* AlertService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["a" /* AlertService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__models__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__models__["Constants"]) === "function" && _e || Object])
], OfficeLocationComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=office-location.component.js.map

/***/ }),

/***/ "./src/app/components/register/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__register_component__ = __webpack_require__("./src/app/components/register/register.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__register_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"conatiner\">\n    <div class=\"row justify-content-center\">\n        <div class=\"col-sm-4 RegisterTemplate\">\n                <h2>Register</h2>\n            <form [formGroup]=\"registerForm\" (ngSubmit)=\"onSubmit(registerForm.value)\">\n                <div class=\"form-group\">\n                    <label for=\"username\">Username</label>\n                    <input type=\"text\" class=\"form-control\" formControlName=\"username\" required>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"password\">Password</label>\n                    <input type=\"password\" class=\"form-control\" formControlName=\"password\" required>\n                </div>\n                <div class=\"form-group\">\n                    <button type=\"submit\" class=\"btn btn-primaryTheme\" [disabled]=\"!registerForm.valid\">Submit</button>\n                    <a class=\"col-2\" [routerLink]=\"['/login']\"  href=\"javascript:void(0)\">Cancel</a>\n\n                </div>\n            </form>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/register/register.component.scss":
/***/ (function(module, exports) {

module.exports = ".registerTemplate {\n  margin-top: 25vh; }\n"

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__("./src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterComponent = (function () {
    function RegisterComponent(userService) {
        this.userService = userService;
        this.submitted = false;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.registerForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormGroup"]({
            username: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required),
            password: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required)
        });
    };
    RegisterComponent.prototype.onSubmit = function (_a) {
        var _b = _a.username, username = _b === void 0 ? "" : _b, _c = _a.password, password = _c === void 0 ? "" : _c;
        this.submittedUser = new __WEBPACK_IMPORTED_MODULE_2__models__["User"]("", username, password);
        this.userService.create(this.submittedUser).subscribe(function (response) {
        });
    };
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-register',
        template: __webpack_require__("./src/app/components/register/register.component.html"),
        styles: [__webpack_require__("./src/app/components/register/register.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services__["f" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["f" /* UserService */]) === "function" && _a || Object])
], RegisterComponent);

var _a;
//# sourceMappingURL=register.component.js.map

/***/ }),

/***/ "./src/app/components/utils/geolocation/geolocation.component.html":
/***/ (function(module, exports) {

module.exports = "\n  <div class=\"form-group\">\n    <input \n    placeholder=\"search for location\" \n    (change)=\"hasChange($event)\" \n    autocorrect=\"off\" autocapitalize=\"off\" \n    spellcheck=\"off\" \n    type=\"text\" \n    class=\"form-control\" \n    #search \n    [formControl]=\"searchControl\">\n  </div>\n  <button (click)=\"setCurrentPosition()\" class=\"default-btn label-location fa fa-map-marker\">\n    Set current location\n  </button>\n  <br>\n  <agm-map \n  [latitude]=\"geoLocation.latitude\" \n  [longitude]=\"geoLocation.longitude\" \n  [scrollwheel]=\"false\" \n  [zoom]=\"zoom\"\n  (mapClick)=\"mapClicked($event)\"\n  >\n    <agm-marker \n    [latitude]=\"geoLocation.latitude\" \n    [longitude]=\"geoLocation.longitude\"\n    [markerDraggable]=\"geoLocation.draggable\"\n    (dragEnd)=\"markerDragEnd(m, $event)\"></agm-marker>\n  </agm-map>\n  <input type=\"number\" class=\"form-control\" [(ngModel)]=\"geoLocation.latitude\" disabled>  \n  <input type=\"number\" class=\"form-control\" [(ngModel)]=\"geoLocation.longitude\" disabled>  \n"

/***/ }),

/***/ "./src/app/components/utils/geolocation/geolocation.component.scss":
/***/ (function(module, exports) {

module.exports = "agm-map {\n  height: 300px; }\n\n.label-location {\n  margin-bottom: 10px;\n  text-align: center; }\n"

/***/ }),

/***/ "./src/app/components/utils/geolocation/geolocation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeolocationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GeolocationComponent = (function () {
    function GeolocationComponent(mapsAPILoader, ngZone) {
        this.mapsAPILoader = mapsAPILoader;
        this.ngZone = ngZone;
        this.geoLocation = { draggable: true, latitude: 0, longitude: 0, newLoc: false };
    }
    GeolocationComponent.prototype.ngOnInit = function () {
        var _this = this;
        //set google maps defaults
        this.zoom = 10;
        //create search FormControl
        this.searchControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]();
        this.geoLocation.draggable = true;
        //set current position
        if (this.geoLocation.latitude == 0 &&
            this.geoLocation.longitude == 0) {
            this.setCurrentPosition();
        }
        //load Places Autocomplete
        this.mapsAPILoader.load().then(function () {
            var autocomplete = new google.maps.places.Autocomplete(_this.searchElementRef.nativeElement, {
                types: ["address"]
            });
            autocomplete.addListener("place_changed", function () {
                _this.ngZone.run(function () {
                    //get the place result
                    var place = autocomplete.getPlace();
                    //verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }
                    //set latitude, longitude and zoom
                    _this.geoLocation.latitude = place.geometry.location.lat();
                    _this.geoLocation.longitude = place.geometry.location.lng();
                    _this.zoom = 12;
                });
            });
        });
        setTimeout(function () {
            window.dispatchEvent(new Event("resize"));
        }, 2000);
    };
    GeolocationComponent.prototype.setCurrentPosition = function () {
        var _this = this;
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                _this.geoLocation.latitude = position.coords.latitude;
                _this.geoLocation.longitude = position.coords.longitude;
                _this.zoom = 12;
            });
        }
    };
    GeolocationComponent.prototype.mapClicked = function ($event) {
        this.geoLocation.latitude = $event.coords.lat;
        this.geoLocation.longitude = $event.coords.lng;
        this.geoLocation.draggable = true;
    };
    GeolocationComponent.prototype.markerDragEnd = function (m, $event) {
        this.geoLocation.latitude = $event.coords.lat;
        this.geoLocation.longitude = $event.coords.lng;
        this.geoLocation.draggable = true;
    };
    GeolocationComponent.prototype.hasChange = function (ev) {
    };
    return GeolocationComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('init'),
    __metadata("design:type", Object)
], GeolocationComponent.prototype, "geoLocation", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("search"),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], GeolocationComponent.prototype, "searchElementRef", void 0);
GeolocationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-geolocation',
        template: __webpack_require__("./src/app/components/utils/geolocation/geolocation.component.html"),
        styles: [__webpack_require__("./src/app/components/utils/geolocation/geolocation.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__agm_core__["b" /* MapsAPILoader */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__agm_core__["b" /* MapsAPILoader */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _c || Object])
], GeolocationComponent);

var _a, _b, _c;
//# sourceMappingURL=geolocation.component.js.map

/***/ }),

/***/ "./src/app/components/utils/geolocation/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__geolocation_component__ = __webpack_require__("./src/app/components/utils/geolocation/geolocation.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__geolocation_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/utils/loader/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__loader_component__ = __webpack_require__("./src/app/components/utils/loader/loader.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__loader_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/utils/loader/loader.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loader\" *ngIf=\"show\">\n<img src=\"assets/planeLoad.gif\" alt=\"\">\n<h4>Loading...</h4>\n</div>"

/***/ }),

/***/ "./src/app/components/utils/loader/loader.component.scss":
/***/ (function(module, exports) {

module.exports = ".loader {\n  position: absolute;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  background-color: white;\n  width: 100%;\n  height: 100%;\n  z-index: 1000;\n  opacity: 1;\n  position: fixed; }\n"

/***/ }),

/***/ "./src/app/components/utils/loader/loader.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_loader_service__ = __webpack_require__("./src/app/services/loader.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoaderComponent = (function () {
    function LoaderComponent(loaderService) {
        this.loaderService = loaderService;
        this.show = false;
    }
    LoaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.loaderService.loaderState
            .subscribe(function (state) {
            // console.log(state);
            _this.show = state.show;
        });
    };
    LoaderComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    return LoaderComponent;
}());
LoaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-loader',
        template: __webpack_require__("./src/app/components/utils/loader/loader.component.html"),
        styles: [__webpack_require__("./src/app/components/utils/loader/loader.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_loader_service__["a" /* LoaderService */]) === "function" && _a || Object])
], LoaderComponent);

var _a;
//# sourceMappingURL=loader.component.js.map

/***/ }),

/***/ "./src/app/components/utils/modal-card-view/modal-card-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"myModal\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-md\">\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n          <h4 class=\"modal-title\">{{selectedPrev.name}}</h4>\n        </div>\n        <div class=\"container-fluid modal-body modal-preview\">\n          <div class=\"card-preview\">\n              <img *ngIf=\"selectedPrev.fileName != '' && selectedPrev.fileName != undefined\" src={{selectedPrev.fileName}} alt=\"\">\n              <h4 class=\"text-center\"><strong>{{selectedPrev.name}}</strong></h4>\n              <table class=\"table\" style=\"margin: auto;\">\n                  <thead>\n                    <tr>\n                      <th>\n                        Country\n                      </th>\n                      <th>\n                        Language\n                      </th>\n                      <th>\n                        Schedule\n                      </th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr>\n                      <td style=\"width:33%\">{{selectedPrev.country}}</td>\n                      <td style=\"width:33%\">{{selectedPrev.language}}</td>\n                      <td style=\"width:33%\">\n                        <p>\n                            From:\n                        </p>\n                        <p>\n                            {{selectedPrev.fromSchedule}}\n                        </p>\n                         <p>\n                            To: \n                         </p>\n                       <p>\n                          {{selectedPrev.toSchedule}}\n\n                       </p>\n                      </td>\n                    </tr>\n                  </tbody>\n                </table>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n        </div>\n      </div>\n  \n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/components/utils/modal-card-view/modal-card-view.component.scss":
/***/ (function(module, exports) {

module.exports = ".card-preview {\n  width: 85%;\n  /* Add shadows to create the \"card\" effect */\n  -webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  -webkit-transition: 0.3s;\n  transition: 0.3s; }\n  .card-preview img {\n    width: 100%; }\n  .modal-preview {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n"

/***/ }),

/***/ "./src/app/components/utils/modal-card-view/modal-card-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalCardViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ModalCardViewComponent = (function () {
    function ModalCardViewComponent() {
        this.selectedPrev = {
            name: '',
            country: '',
            language: '',
            fromSchedule: '',
            toSchedule: ''
        };
    }
    ModalCardViewComponent.prototype.ngOnInit = function () {
    };
    return ModalCardViewComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('entitySelected'),
    __metadata("design:type", Object)
], ModalCardViewComponent.prototype, "selectedPrev", void 0);
ModalCardViewComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-modal-card-view',
        template: __webpack_require__("./src/app/components/utils/modal-card-view/modal-card-view.component.html"),
        styles: [__webpack_require__("./src/app/components/utils/modal-card-view/modal-card-view.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], ModalCardViewComponent);

//# sourceMappingURL=modal-card-view.component.js.map

/***/ }),

/***/ "./src/app/components/utils/nav/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row navBar-default\">\n      <div class=\"col-sm-3\">\n      <a routerLink=\"/\">\n          <img src=\"assets/main_logo.png\" alt=\"\">\n      </a>\n      </div>\n      <div class=\"col-sm-2 col-sm-offset-6 pull-right header\">\n          <h5 style=\"text-align:right;\"> \n                  User: <strong >{{user.username}}</strong>\n          </h5>    \n          \n      </div>\n      <div class=\"col-sm-1 pull-right header\">\n          <a href=\"javascript:void(0)\" (click)=\"logOut();\">\n              <h5>Logout</h5>\n          </a>\n      </div>\n  </div>\n</div>    \n\n"

/***/ }),

/***/ "./src/app/components/utils/nav/header/header.component.scss":
/***/ (function(module, exports) {

module.exports = ".container-fluid {\n  height: -1px; }\n\n.navBar-default {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: #493087;\n  -webkit-box-shadow: 0 8px 6px -6px #999;\n  box-shadow: 0 8px 6px -6px #999; }\n\n.navBar-default :first-child {\n    padding-left: 0; }\n\n.navBar-default h1, .navBar-default h2, .navBar-default h3, .navBar-default h4, .navBar-default h5, .navBar-default a, .navBar-default p {\n    color: #7dc5ad; }\n"

/***/ }),

/***/ "./src/app/components/utils/nav/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("./src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models__ = __webpack_require__("./src/app/models/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeaderComponent = (function () {
    function HeaderComponent(genericService, router, loader) {
        this.genericService = genericService;
        this.router = router;
        this.loader = loader;
        this.navBar = false;
        this.user = new __WEBPACK_IMPORTED_MODULE_3__models__["User"]();
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.user = JSON.parse(localStorage.getItem('currentUser'));
    };
    HeaderComponent.prototype.logOut = function () {
        var _this = this;
        this.loader.show();
        this.genericService.logout().subscribe(function (response) {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('token');
            _this.navBar = false;
            _this.router.navigateByUrl("/login");
            _this.loader.hide();
        }, function (error) {
        });
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-header',
        template: __webpack_require__("./src/app/components/utils/nav/header/header.component.html"),
        styles: [__webpack_require__("./src/app/components/utils/nav/header/header.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* AuthenticationService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* LoaderService */]) === "function" && _c || Object])
], HeaderComponent);

var _a, _b, _c;
//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ "./src/app/components/utils/nav/header/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__header_component__ = __webpack_require__("./src/app/components/utils/nav/header/header.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__header_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/utils/nav/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__header__ = __webpack_require__("./src/app/components/utils/nav/header/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__header__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sidebar__ = __webpack_require__("./src/app/components/utils/nav/sidebar/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__sidebar__["a"]; });


//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/utils/nav/sidebar/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sidebar_component__ = __webpack_require__("./src/app/components/utils/nav/sidebar/sidebar.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__sidebar_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/utils/nav/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" *ngIf=\"fixed\"> </div>\n<div class=\"sidebar\" [class.fixed]=\"fixed\" [ngClass]=\"{'expanded':isOpen}\">\n    <div class=\"sidebar-item no-hover menuButton\" id=\"burger\" (click)=\"isOpen=!isOpen\">\n        <i class=\"fa fa-bars hand\" aria-hidden=\"true\"></i>\n        <span class=\"sidebar-item-title\">CMS</span>\n    </div>\n    <a href=\"#\" routerLink=\"/\" >\n        <div class=\"sidebar-item\">\n\n            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n            <span class=\"sidebar-item-title\">Home</span>\n\n        </div>\n    </a>\n    <a href=\"#\" routerLink=\"../campaignList\" >\n        <div class=\"sidebar-item\">\n\n            <i class=\"fa  fa-file-text-o\" aria-hidden=\"true\"></i>\n            <span class=\"sidebar-item-title\">Campaign List</span>\n\n        </div>\n    </a>\n    <a href=\"#\" routerLink=\"../officeLocation\" >\n        <div class=\"sidebar-item\">\n\n            <i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>\n            <span class=\"sidebar-item-title\">Location</span>\n        </div>\n\n    </a>\n    <a href=\"#\" routerLink=\"../callcenter\" >\n        <div class=\"sidebar-item\">\n\n            <i class=\"fa fa-phone\" aria-hidden=\"true\"></i>\n            <span class=\"sidebar-item-title\">Call Center</span>\n        </div>\n\n    </a>\n    <a href=\"#\" routerLink=\"/\" >\n        <div class=\"sidebar-item\">\n\n            <i class=\"fa  fa-tag\" aria-hidden=\"true\"></i>\n            <span class=\"sidebar-item-title\">Labels</span>\n        </div>\n\n    </a>\n    <a href=\"#\" routerLink=\"../image\" >\n        <div class=\"sidebar-item\">\n\n            <i class=\"fa fa-picture-o\" aria-hidden=\"true\"></i>\n            <span class=\"sidebar-item-title\">Bulk Upload Images</span>\n\n        </div>\n    </a>\n    <a href=\"#\" routerLink=\"../emailUs\" >\n        <div class=\"sidebar-item\">\n            <i class=\"fa fa-envelope-o\" aria-hidden=\"true\"></i>\n            <span class=\"sidebar-item-title\">Email Us</span>\n\n        </div>\n    </a>\n</div>"

/***/ }),

/***/ "./src/app/components/utils/nav/sidebar/sidebar.component.scss":
/***/ (function(module, exports) {

module.exports = ".sidebar {\n  background-color: #7dc5ad;\n  -webkit-box-shadow: 0 8px 6px -6px #999;\n  box-shadow: 0 8px 6px -6px #999;\n  height: 50%;\n  width: 50px;\n  overflow-y: hidden;\n  -webkit-transition: 0.4s;\n  transition: 0.4s; }\n  .sidebar .sidebar-item-title {\n    opacity: 0; }\n  .expanded {\n  width: 236px;\n  -webkit-transition: 0.4s;\n  transition: 0.4s; }\n  .expanded .sidebar-item-title {\n    opacity: 1;\n    -webkit-transition: 2s;\n    transition: 2s; }\n  .sidebar-item {\n  cursor: pointer;\n  padding: 10px 15px 10px 15px;\n  color: white;\n  height: 50px;\n  overflow: hidden; }\n  .sidebar-item a {\n    text-decoration: none;\n    color: white; }\n  .sidebar-item .fa {\n    max-width: 15px;\n    max-height: 15px;\n    margin-right: 15px; }\n  .sidebar-item.active {\n    background-color: #493087; }\n  .sidebar-item.no-hover:hover {\n    background-color: #7dc5ad; }\n  .sidebar-item:hover {\n    background-color: #493087; }\n  .hand {\n  cursor: pointer; }\n  .fixed {\n  position: fixed;\n  top: 0;\n  padding-top: 60px;\n  overflow-y: hidden; }\n  .fixed .menuButton {\n    display: none; }\n"

/***/ }),

/***/ "./src/app/components/utils/nav/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarComponent = (function () {
    function SidebarComponent() {
        // @Input('init') isOpen= false;
        this.isOpen = false;
        this.fixed = false;
    }
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent.prototype.onWindowScroll = function () {
        var num = document.body.scrollTop;
        if (num > 60) {
            this.fixed = true;
            this.isOpen = false;
        }
        else if (this.fixed && num < 60) {
            this.fixed = false;
        }
    };
    return SidebarComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])("window:scroll", []),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], SidebarComponent.prototype, "onWindowScroll", null);
SidebarComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-sidebar',
        template: __webpack_require__("./src/app/components/utils/nav/sidebar/sidebar.component.html"),
        styles: [__webpack_require__("./src/app/components/utils/nav/sidebar/sidebar.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], SidebarComponent);

//# sourceMappingURL=sidebar.component.js.map

/***/ }),

/***/ "./src/app/components/utils/pagination/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pagination_component__ = __webpack_require__("./src/app/components/utils/pagination/pagination.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__pagination_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/utils/pagination/pagination.component.html":
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer\">\n      <div class=\"text-center\">\n        <ul *ngIf=\"pager.pages && pager.pages.length\" class=\"pagination\">\n          <li [ngClass]=\"{disabled:pager.currentPage === 1}\">\n            <a href=\"javascript:void(0);\" (click)=\"setPage(1)\">First</a>\n          </li>\n          <li [ngClass]=\"{disabled:pager.currentPage === 1}\">\n            <a href=\"javascript:void(0);\" (click)=\"setPage(pager.currentPage - 1)\">Previous</a>\n          </li>\n          <li *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n            <a href=\"javascript:void(0);\" (click)=\"setPage(page)\">{{page}}</a>\n          </li>\n          <li [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n            <a href=\"javascript:void(0);\" (click)=\"setPage(pager.currentPage + 1)\">Next</a>\n          </li>\n          <li [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n            <a href=\"javascript:void(0);\" (click)=\"setPage(pager.totalPages)\">Last</a>\n          </li>\n        </ul>\n      </div>\n  </footer>\n"

/***/ }),

/***/ "./src/app/components/utils/pagination/pagination.component.scss":
/***/ (function(module, exports) {

module.exports = ".row .text-center {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/components/utils/pagination/pagination.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_index__ = __webpack_require__("./src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PaginationComponent = (function () {
    function PaginationComponent(pagerService, loader) {
        this.pagerService = pagerService;
        this.loader = loader;
        this.paginationList = { entityList: [], pagedItems: [], pager: {}, customNumberPages: undefined };
        /**
         * PAGINATION
         *
         * @private
         * @type {any[]}
         * @memberOf CampaignListComponent
         */
        // pager object
        this.pager = {};
    }
    PaginationComponent.prototype.ngOnInit = function () {
        this.setPage(1);
    };
    /**
     * Pagination logic.
     *
     * @param {number} page
     * @returns
     *
     * @memberOf CampaignListComponent
     */
    PaginationComponent.prototype.setPage = function (page) {
        var _this = this;
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        this.paginationList.entityList;
        // get pager object from service
        this.loader.show();
        this.pagerService.getPager(this.paginationList.entityList.length, page, this.paginationList.customNumberPages || 8).then(function (pager) {
            _this.pager = pager;
            // get current page of items
            _this.pagedItems = _this.paginationList.entityList.slice(_this.pager.startIndex, _this.pager.endIndex + 1);
            _this.loader.hide();
            _this.paginationList.pager = _this.pager;
            _this.paginationList.pagedItems = _this.pagedItems;
        });
    };
    return PaginationComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('init'),
    __metadata("design:type", Object)
], PaginationComponent.prototype, "paginationList", void 0);
PaginationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-pagination',
        template: __webpack_require__("./src/app/components/utils/pagination/pagination.component.html"),
        styles: [__webpack_require__("./src/app/components/utils/pagination/pagination.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_index__["e" /* PagerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_index__["e" /* PagerService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_index__["d" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_index__["d" /* LoaderService */]) === "function" && _b || Object])
], PaginationComponent);

var _a, _b;
//# sourceMappingURL=pagination.component.js.map

/***/ }),

/***/ "./src/app/components/utils/search-filter/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__search_filter_component__ = __webpack_require__("./src/app/components/utils/search-filter/search-filter.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__search_filter_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/components/utils/search-filter/search-filter.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid borderFluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-2\">\n      <h3 class=\"header-Panel\">Filters</h3>\n    </div>\n  </div>\n  <br>\n  <form [formGroup]=\"myForm\" (ngSubmit)=\"onSearch(myForm.value)\">\n    <div class=\"row flex-container\">\n\n      <div class=\"col-sm-4  inputs\" *ngIf=\"searchFilter.availableSearch.searchName\">\n          <div class=\"form-group\">\n              <label class=\"filterLabel\" for=\"\">Name</label>\n              <br>\n              <input type=\"text\" [(ngModel)]=\"entityToSearch.name\" placeholder=\"example@email.com\" class=\"form-control\" formControlName=\"name\">\n          </div>\n      </div>\n\n      <div class=\"col-sm-4  inputs\"  *ngIf=\"searchFilter.availableSearch.searchCountry\">\n        <div class=\"form-group\">\n          <label class=\"filterLabel\" for=\"country\">Country</label>\n          <ng-select\n          placeholder=\"Country\" \n          formControlName=\"country\" \n          [multiple]=\"true\" \n          [items]=\"searchFilter.countriesArray\" \n          (data)=\"refreshValue($event)\" \n          (selected)=\"selectedCountry($event)\"\n          (removed)=\"removedCountry($event)\">\n          </ng-select>\n        </div>\n      </div>\n\n      <div class=\"col-sm-4 inputs\" *ngIf=\"searchFilter.availableSearch.searchLanguage\">\n        <div class=\"form-group\">\n          <label class=\"filterLabel\" for=\"languages\">Languages</label>\n          <ng-select \n          placeholder=\"Languages\" \n          formControlName=\"language\"\n          [allowClear]=\"true\" \n          [multiple]=\"true\" \n          [items]=\"searchFilter.languageArray\" \n          (data)=\"refreshValue($event)\" \n          (selected)=\"selectedLenguage($event)\"\n          (removed)=\"removedLanguage($event)\">\n          </ng-select>\n        </div>\n      </div>\n\n      <div class=\"col-sm-4 inputs\" *ngIf=\"searchFilter.availableSearch.searchStatus\">\n        <label class=\"filterLabel\" for=\"image\">Status</label>\n        <br>\n        <label class=\"radio-inline\">\n          <input type=\"radio\" \n          [(ngModel)]=\"entityToSearch.published\" \n          formControlName=\"published\" \n          value=\"\">All\n        </label>\n\n        <label class=\"radio-inline\">\n          <input type=\"radio\" \n          [(ngModel)]=\"entityToSearch.published\" \n          formControlName=\"published\" \n          value=\"true\">Published\n        </label>\n\n\n        <label class=\"radio-inline\">\n          <input type=\"radio\" \n          [(ngModel)]=\"entityToSearch.published\" \n          formControlName=\"published\" \n          value=\"false\">un-Published\n        </label>\n      </div>\n    </div>\n    <div class=\"row flex-container\">\n      <div class=\"col-sm-2 inputs text-center\">\n            <button type=\"submit\" class=\"btn btn-primaryTheme\">Search</button>\n      </div>\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/components/utils/search-filter/search-filter.component.scss":
/***/ (function(module, exports) {

module.exports = ".flex-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-flow: row wrap;\n          flex-flow: row wrap;\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1;\n          flex: 1 1;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n"

/***/ }),

/***/ "./src/app/components/utils/search-filter/search-filter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchFilterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__("./src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchFilterComponent = (function () {
    function SearchFilterComponent(fb, genServices, constants) {
        this.fb = fb;
        this.genServices = genServices;
        this.constants = constants;
        this.searchFilter = {
            apiRest: '',
            availableSearch: {
                searchStatus: false,
                searchName: false,
                searchCountry: false,
                searchLanguage: false
            },
            name: '',
            countriesArray: [],
            countryFromServer: [],
            languageArray: [],
            makeAction: false
        };
        this.valueFromSearch = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.resultsFound = false;
        this.entityToSearch = new __WEBPACK_IMPORTED_MODULE_2__models__["EntityToSearch"]();
        this.langSelected = new Array();
        this.entityToSearch.country = [];
        this.countriesSelected = [];
        this.entityToSearch.name = '';
        this.apiRest = constants.IApiRest;
    }
    SearchFilterComponent.prototype.ngOnInit = function () {
        this.initForm();
        this.initCountries();
    };
    /**
     * Initialize inputs inside filter panel view.
     *
     *
     * @memberOf CallcenterComponent
     */
    SearchFilterComponent.prototype.initForm = function () {
        this.myForm = this.fb.group({
            name: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](''),
            country: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](''),
            language: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](''),
            published: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('')
        });
    };
    /**
     * Get all Country from server
     *
     *
     * @memberOf CallcenterComponent
     */
    SearchFilterComponent.prototype.initCountries = function () {
        var _this = this;
        this.genServices.get(this.apiRest.API_COUNTRY).subscribe(function (response) {
            _this.searchFilter.countryFromServer = response.data.countries;
            _this.searchFilter.countriesArray = response.data.countries.map(function (country) { return country.countryName; });
        }, function (error) {
        }, function () {
            _this.initLanguages();
        });
    };
    /**
    * Get all Languages from server
    *
    *
    * @memberOf CallcenterComponent
    */
    SearchFilterComponent.prototype.initLanguages = function () {
        var _this = this;
        this.genServices.get(this.apiRest.API_LANGUAGE).subscribe(function (response) {
            _this.searchFilter.languageArray = response.data.languages.map(function (language) { return language.languageCode; });
        }, function (error) {
        }, function () {
        });
    };
    SearchFilterComponent.prototype.onSearch = function (value) {
        var _this = this;
        var valueToSearch = new __WEBPACK_IMPORTED_MODULE_2__models__["EntityToSearch"]();
        valueToSearch.name = this.entityToSearch.name.toLowerCase() || undefined;
        valueToSearch.country = this.entityToSearch.country || undefined;
        valueToSearch.language = this.langSelected || undefined;
        valueToSearch.published = this.entityToSearch.published || undefined;
        // ARREGLAR ACA Y ARREGLAR BACKEND
        this.genServices.getBySearch(this.searchFilter.apiRest, valueToSearch).subscribe(function (response) {
            _this.valueFromSearch.emit({
                value: response.data
            });
        }, function (error) {
        });
    };
    /**
    * Country Selected
    * Add each language selected toa an array of countrie.
    * @param {*} value
    *
    * @memberOf CallcenterComponent
    */
    SearchFilterComponent.prototype.selectedCountry = function (value) {
        this.countriesSelected.push(value.text);
        var countryFound = this.searchFilter.countryFromServer.find(function (country) {
            return country.countryName == value.text;
        });
        this.entityToSearch.country.push(countryFound.countryCode);
    };
    /**
     * Every time is the country remove from input it will remove from
     * @param this.countrySearch as well
     *
     * @param {*} value
     *
     * @memberOf CallcenterComponent
     */
    SearchFilterComponent.prototype.removedCountry = function (value) {
        var index = this.countriesSelected.indexOf(value.text);
        this.countriesSelected.splice(index, 1);
        var countryFound = this.searchFilter.countryFromServer.find(function (country) {
            return country.countryName == value.text;
        });
        index = this.entityToSearch.country.indexOf(countryFound);
        this.entityToSearch.country.splice(index, 1);
    };
    /**
      * Languages Selected
      * Add each language selected toa an array of languages.
      * @param {*} value
      *
      * @memberOf CallcenterComponent
      */
    SearchFilterComponent.prototype.selectedLenguage = function (value, i) {
        if (i === void 0) { i = null; }
        // console.log('Selected value is: ', value);
        this.langSelected.push(value.text);
    };
    /**
     * Every time is the language remove from input it will remove from
     * @param this.langSelected as well
     *
     * @param {*} value
     *
     * @memberOf CallcenterComponent
     */
    SearchFilterComponent.prototype.removedLanguage = function (value) {
        var index = this.langSelected.indexOf(value.text);
        this.langSelected.splice(index, 1);
    };
    SearchFilterComponent.prototype.typed = function (value) {
        // console.log('New search input: ', value);
    };
    SearchFilterComponent.prototype.refreshValue = function (value) {
        // this.value = value;
    };
    return SearchFilterComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('searchInput'),
    __metadata("design:type", Object)
], SearchFilterComponent.prototype, "searchFilter", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('changeView'),
    __metadata("design:type", Object)
], SearchFilterComponent.prototype, "valueFromSearch", void 0);
SearchFilterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-search-filter',
        template: __webpack_require__("./src/app/components/utils/search-filter/search-filter.component.html"),
        styles: [__webpack_require__("./src/app/components/utils/search-filter/search-filter.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services__["c" /* GenericService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["c" /* GenericService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__models__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models__["Constants"]) === "function" && _c || Object])
], SearchFilterComponent);

var _a, _b, _c;
//# sourceMappingURL=search-filter.component.js.map

/***/ }),

/***/ "./src/app/directives/alert.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div *ngIf=\"message\" [ngClass]=\"{ 'alert': message, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }\">\n    <ul>\n        <li *ngFor=\"let text of message.text; let i = index;\">\n            -  {{text}}\n        </li>\n    </ul>\n</div>"

/***/ }),

/***/ "./src/app/directives/alert.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_index__ = __webpack_require__("./src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlertComponent = (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getMessage().subscribe(function (message) { _this.message = message; });
    };
    return AlertComponent;
}());
AlertComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'alert',
        template: __webpack_require__("./src/app/directives/alert.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_index__["a" /* AlertService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_index__["a" /* AlertService */]) === "function" && _a || Object])
], AlertComponent);

var _a;
//# sourceMappingURL=alert.component.js.map

/***/ }),

/***/ "./src/app/directives/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__alert_component__ = __webpack_require__("./src/app/directives/alert.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__alert_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigateByUrl('/login', { queryParams: { returnUrl: state.url } });
        return false;
    };
    return AuthGuard;
}());
AuthGuard = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object])
], AuthGuard);

var _a;
//# sourceMappingURL=auth.guard.js.map

/***/ }),

/***/ "./src/app/guards/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__auth_guard__ = __webpack_require__("./src/app/guards/auth.guard.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__auth_guard__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/models/Entity.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Entity; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CityModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CityProperty; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__MainEntity__ = __webpack_require__("./src/app/models/MainEntity.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Entity = (function (_super) {
    __extends(Entity, _super);
    function Entity(countryName, countryCode, cities) {
        if (countryName === void 0) { countryName = ''; }
        if (countryCode === void 0) { countryCode = ''; }
        if (cities === void 0) { cities = new Array(); }
        var _this = _super.call(this, countryName, countryCode) || this;
        _this.cities = cities;
        return _this;
    }
    return Entity;
}(__WEBPACK_IMPORTED_MODULE_0__MainEntity__["b" /* MainEntity */]));

var CityModel = (function (_super) {
    __extends(CityModel, _super);
    function CityModel(city, properties, published) {
        if (city === void 0) { city = ""; }
        if (properties === void 0) { properties = new Array(); }
        if (published === void 0) { published = false; }
        var _this = _super.call(this, properties) || this;
        _this.city = city;
        _this.published = published;
        return _this;
    }
    return CityModel;
}(__WEBPACK_IMPORTED_MODULE_0__MainEntity__["a" /* MainBasedProps */]));

var CityProperty = (function (_super) {
    __extends(CityProperty, _super);
    function CityProperty(language, cityName, openingTime, telephone) {
        if (language === void 0) { language = ""; }
        if (cityName === void 0) { cityName = ""; }
        if (openingTime === void 0) { openingTime = ""; }
        if (telephone === void 0) { telephone = ""; }
        var _this = _super.call(this, language) || this;
        _this.cityName = cityName;
        _this.openingTime = openingTime;
        _this.telephone = telephone;
        return _this;
    }
    return CityProperty;
}(__WEBPACK_IMPORTED_MODULE_0__MainEntity__["c" /* MainProperties */]));

//# sourceMappingURL=Entity.js.map

/***/ }),

/***/ "./src/app/models/MainEntity.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MainEntity; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainBasedProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return MainProperties; });
var MainEntity = (function () {
    function MainEntity(countryName, countryCode) {
        if (countryName === void 0) { countryName = ''; }
        if (countryCode === void 0) { countryCode = ''; }
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.countryCode = countryCode;
    }
    return MainEntity;
}());

var MainBasedProps = (function () {
    function MainBasedProps(properties) {
        if (properties === void 0) { properties = new Array(); }
        this.properties = properties;
        this.properties = properties;
    }
    return MainBasedProps;
}());

var MainProperties = (function () {
    function MainProperties(language) {
        if (language === void 0) { language = ""; }
        this.language = language;
        this.language = language;
    }
    return MainProperties;
}());

//# sourceMappingURL=MainEntity.js.map

/***/ }),

/***/ "./src/app/models/SearchEntity.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntityToSearch; });
var EntityToSearch = (function () {
    function EntityToSearch() {
    }
    return EntityToSearch;
}());

//# sourceMappingURL=SearchEntity.js.map

/***/ }),

/***/ "./src/app/models/callcenter.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallCenter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Entity__ = __webpack_require__("./src/app/models/Entity.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var CallCenter = (function (_super) {
    __extends(CallCenter, _super);
    function CallCenter(countryName, countryCode, cities) {
        if (countryName === void 0) { countryName = ''; }
        if (countryCode === void 0) { countryCode = ''; }
        if (cities === void 0) { cities = new Array(); }
        return _super.call(this, countryName, countryCode, cities) || this;
    }
    return CallCenter;
}(__WEBPACK_IMPORTED_MODULE_0__Entity__["c" /* Entity */]));

//# sourceMappingURL=callcenter.js.map

/***/ }),

/***/ "./src/app/models/campaign.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Campaign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return CampaignBasedModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return CampaignProperty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return CampaignEntity; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CampaignBasedEntity; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__MainEntity__ = __webpack_require__("./src/app/models/MainEntity.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Campaign = (function (_super) {
    __extends(Campaign, _super);
    function Campaign(countryName, countryCode, cities) {
        if (countryName === void 0) { countryName = ''; }
        if (countryCode === void 0) { countryCode = ''; }
        if (cities === void 0) { cities = new Array(); }
        var _this = _super.call(this, countryName, countryCode) || this;
        _this.cities = cities;
        return _this;
    }
    return Campaign;
}(__WEBPACK_IMPORTED_MODULE_0__MainEntity__["b" /* MainEntity */]));

var CampaignBasedModel = (function (_super) {
    __extends(CampaignBasedModel, _super);
    function CampaignBasedModel(name, properties) {
        if (properties === void 0) { properties = new Array(); }
        var _this = _super.call(this, properties) || this;
        _this.name = name;
        return _this;
    }
    return CampaignBasedModel;
}(__WEBPACK_IMPORTED_MODULE_0__MainEntity__["a" /* MainBasedProps */]));

var CampaignProperty = (function (_super) {
    __extends(CampaignProperty, _super);
    function CampaignProperty(language, campaignName, fileName, link, fromSchedule, toSchedule, date) {
        if (language === void 0) { language = ""; }
        if (campaignName === void 0) { campaignName = ""; }
        if (fileName === void 0) { fileName = ""; }
        if (link === void 0) { link = ""; }
        if (fromSchedule === void 0) { fromSchedule = new Date(); }
        if (toSchedule === void 0) { toSchedule = new Date(); }
        if (date === void 0) { date = new Date(); }
        var _this = _super.call(this, language) || this;
        _this.campaignName = campaignName;
        _this.fileName = fileName;
        _this.link = link;
        _this.fromSchedule = _this.setDate(fromSchedule);
        _this.toSchedule = _this.setDate(toSchedule);
        _this.modifiedDate = _this.setDate(date);
        return _this;
    }
    ;
    CampaignProperty.prototype.setDate = function (date) {
        if (typeof date == 'object') {
            return date.toJSON().slice(0, 10).replace(/-/g, '/');
        }
        return date;
    };
    return CampaignProperty;
}(__WEBPACK_IMPORTED_MODULE_0__MainEntity__["c" /* MainProperties */]));

var CampaignEntity = (function (_super) {
    __extends(CampaignEntity, _super);
    function CampaignEntity(countryName, countryCode, campaigns) {
        if (countryName === void 0) { countryName = ''; }
        if (countryCode === void 0) { countryCode = ''; }
        if (campaigns === void 0) { campaigns = new Array(); }
        var _this = _super.call(this, countryName, countryCode) || this;
        _this.campaigns = campaigns;
        return _this;
    }
    return CampaignEntity;
}(__WEBPACK_IMPORTED_MODULE_0__MainEntity__["b" /* MainEntity */]));

var CampaignBasedEntity = (function () {
    function CampaignBasedEntity(name, properties) {
        if (name === void 0) { name = ""; }
        if (properties === void 0) { properties = new Array(); }
        this.properties = properties;
        this.name = name;
    }
    return CampaignBasedEntity;
}());

//# sourceMappingURL=campaign.js.map

/***/ }),

/***/ "./src/app/models/constants.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Constants; });
/* unused harmony export API_REST */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__environments_environment__ = __webpack_require__("./src/environments/environment.ts");

var Constants = (function () {
    function Constants() {
        console.log(__WEBPACK_IMPORTED_MODULE_0__environments_environment__["a" /* environment */].apiUrl);
        this.root_dir = __WEBPACK_IMPORTED_MODULE_0__environments_environment__["a" /* environment */].apiUrl;
        this.IApiRest = new API_REST();
    }
    return Constants;
}());

var API_REST = (function () {
    function API_REST() {
        this.API_COUNTRY = "/allCountries";
        this.API_LANGUAGE = "/allLanguages";
        this.API_CAMPAIGN = '/campaigns';
        this.API_CALLCENTER = '/callcenters';
        this.API_OFFICE_LOCATIONS = '/locations';
        this.API_OFFICE_LOCATIONS_MULTI = '/multiLocations';
        this.API_FILES = '/files';
        this.API_FILES_MULTIDELETE = '/removeMultiFiles';
        this.API_EMAILUS = "/contactUs";
        this.API_CALLCENTER_MULTI = "/multiCallcenters";
    }
    return API_REST;
}());

//# sourceMappingURL=constants.js.map

/***/ }),

/***/ "./src/app/models/contactUs.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ContactUs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactToSearch; });
var ContactUs = (function () {
    function ContactUs(email, published) {
        if (email === void 0) { email = ""; }
        if (published === void 0) { published = ''; }
        this.email = email;
        this.published = published;
    }
    return ContactUs;
}());

var ContactToSearch = (function () {
    function ContactToSearch() {
    }
    return ContactToSearch;
}());

//# sourceMappingURL=contactUs.js.map

/***/ }),

/***/ "./src/app/models/country.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Country */
var Country = (function () {
    function Country(countryName, countryCode) {
        if (countryName === void 0) { countryName = ""; }
        if (countryCode === void 0) { countryCode = ""; }
        this.countryName = countryName;
        this.countryCode = countryCode;
    }
    return Country;
}());

//# sourceMappingURL=country.js.map

/***/ }),

/***/ "./src/app/models/image.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Image */
var Image = (function () {
    function Image() {
    }
    return Image;
}());

//# sourceMappingURL=image.js.map

/***/ }),

/***/ "./src/app/models/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user__ = __webpack_require__("./src/app/models/user.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "User", function() { return __WEBPACK_IMPORTED_MODULE_0__user__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__campaign__ = __webpack_require__("./src/app/models/campaign.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "Campaign", function() { return __WEBPACK_IMPORTED_MODULE_1__campaign__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "CampaignBasedEntity", function() { return __WEBPACK_IMPORTED_MODULE_1__campaign__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "CampaignBasedModel", function() { return __WEBPACK_IMPORTED_MODULE_1__campaign__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "CampaignEntity", function() { return __WEBPACK_IMPORTED_MODULE_1__campaign__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "CampaignProperty", function() { return __WEBPACK_IMPORTED_MODULE_1__campaign__["e"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__image__ = __webpack_require__("./src/app/models/image.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__country__ = __webpack_require__("./src/app/models/country.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__constants__ = __webpack_require__("./src/app/models/constants.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "Constants", function() { return __WEBPACK_IMPORTED_MODULE_4__constants__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__officeLocation__ = __webpack_require__("./src/app/models/officeLocation.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "CityOffices", function() { return __WEBPACK_IMPORTED_MODULE_5__officeLocation__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "OfficeLocation", function() { return __WEBPACK_IMPORTED_MODULE_5__officeLocation__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "OfficeProperties", function() { return __WEBPACK_IMPORTED_MODULE_5__officeLocation__["c"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__contactUs__ = __webpack_require__("./src/app/models/contactUs.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "ContactToSearch", function() { return __WEBPACK_IMPORTED_MODULE_6__contactUs__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "ContactUs", function() { return __WEBPACK_IMPORTED_MODULE_6__contactUs__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__marker__ = __webpack_require__("./src/app/models/marker.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__marker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__marker__);
/* harmony namespace reexport (by used) */ if(__webpack_require__.o(__WEBPACK_IMPORTED_MODULE_7__marker__, "CallCenter")) __webpack_require__.d(__webpack_exports__, "CallCenter", function() { return __WEBPACK_IMPORTED_MODULE_7__marker__["CallCenter"]; });
/* harmony namespace reexport (by used) */ if(__webpack_require__.o(__WEBPACK_IMPORTED_MODULE_7__marker__, "CityModel")) __webpack_require__.d(__webpack_exports__, "CityModel", function() { return __WEBPACK_IMPORTED_MODULE_7__marker__["CityModel"]; });
/* harmony namespace reexport (by used) */ if(__webpack_require__.o(__WEBPACK_IMPORTED_MODULE_7__marker__, "CityProperty")) __webpack_require__.d(__webpack_exports__, "CityProperty", function() { return __WEBPACK_IMPORTED_MODULE_7__marker__["CityProperty"]; });
/* harmony namespace reexport (by used) */ if(__webpack_require__.o(__WEBPACK_IMPORTED_MODULE_7__marker__, "Entity")) __webpack_require__.d(__webpack_exports__, "Entity", function() { return __WEBPACK_IMPORTED_MODULE_7__marker__["Entity"]; });
/* harmony namespace reexport (by used) */ if(__webpack_require__.o(__WEBPACK_IMPORTED_MODULE_7__marker__, "EntityToSearch")) __webpack_require__.d(__webpack_exports__, "EntityToSearch", function() { return __WEBPACK_IMPORTED_MODULE_7__marker__["EntityToSearch"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__callcenter__ = __webpack_require__("./src/app/models/callcenter.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "CallCenter", function() { return __WEBPACK_IMPORTED_MODULE_8__callcenter__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__Entity__ = __webpack_require__("./src/app/models/Entity.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "CityModel", function() { return __WEBPACK_IMPORTED_MODULE_9__Entity__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "CityProperty", function() { return __WEBPACK_IMPORTED_MODULE_9__Entity__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "Entity", function() { return __WEBPACK_IMPORTED_MODULE_9__Entity__["c"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__SearchEntity__ = __webpack_require__("./src/app/models/SearchEntity.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "EntityToSearch", function() { return __WEBPACK_IMPORTED_MODULE_10__SearchEntity__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__MainEntity__ = __webpack_require__("./src/app/models/MainEntity.ts");
/* unused harmony namespace reexport */












//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/models/marker.ts":
/***/ (function(module, exports) {

//# sourceMappingURL=marker.js.map

/***/ }),

/***/ "./src/app/models/officeLocation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return OfficeLocation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CityOffices; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return OfficeProperties; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Entity__ = __webpack_require__("./src/app/models/Entity.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var OfficeLocation = (function (_super) {
    __extends(OfficeLocation, _super);
    function OfficeLocation(countryName, countryCode, cities) {
        if (countryName === void 0) { countryName = ""; }
        if (countryCode === void 0) { countryCode = ""; }
        if (cities === void 0) { cities = new Array(); }
        return _super.call(this, countryName, countryCode, cities) || this;
    }
    return OfficeLocation;
}(__WEBPACK_IMPORTED_MODULE_0__Entity__["c" /* Entity */]));

var CityOffices = (function (_super) {
    __extends(CityOffices, _super);
    function CityOffices(city, properties, published) {
        if (city === void 0) { city = ""; }
        if (properties === void 0) { properties = new Array(); }
        if (published === void 0) { published = false; }
        return _super.call(this, city, properties, published) || this;
    }
    return CityOffices;
}(__WEBPACK_IMPORTED_MODULE_0__Entity__["a" /* CityModel */]));

var OfficeProperties = (function (_super) {
    __extends(OfficeProperties, _super);
    function OfficeProperties(language, cityName, openingTime, telephone, address, latitude, longitude) {
        if (cityName === void 0) { cityName = ""; }
        if (openingTime === void 0) { openingTime = ""; }
        if (telephone === void 0) { telephone = ""; }
        if (address === void 0) { address = ""; }
        if (latitude === void 0) { latitude = ""; }
        if (longitude === void 0) { longitude = ""; }
        var _this = _super.call(this, language, cityName, openingTime, telephone) || this;
        _this.address = address;
        _this.latitude = latitude;
        _this.longitude = longitude;
        return _this;
    }
    return OfficeProperties;
}(__WEBPACK_IMPORTED_MODULE_0__Entity__["b" /* CityProperty */]));

//# sourceMappingURL=officeLocation.js.map

/***/ }),

/***/ "./src/app/models/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User(id, username, password, role, carrierCode, properties) {
        if (id === void 0) { id = ""; }
        if (username === void 0) { username = ""; }
        if (password === void 0) { password = ""; }
        if (role === void 0) { role = ""; }
        if (carrierCode === void 0) { carrierCode = ""; }
        if (properties === void 0) { properties = []; }
        this.id = id;
        this.username = username;
        this.password = password;
        this.properties = properties;
        this.role = role;
        this.carrierCode = carrierCode;
    }
    return User;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ "./src/app/services/alert.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AlertService = (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["a" /* Subject */]();
        this.keepAfterNavigationChange = false;
        // clear alert message on route change
        router.events.subscribe(function (event) {
            if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* NavigationStart */]) {
                if (_this.keepAfterNavigationChange) {
                    // console.log(this.keepAfterNavigationChange)
                    // only keep for a single location change
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    // console.log(this.keepAfterNavigationChange);
                    // clear alert
                    _this.subject.next();
                }
            }
        });
    }
    AlertService.prototype.success = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    };
    AlertService.prototype.error = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    };
    AlertService.prototype.errorArray = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    };
    AlertService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    return AlertService;
}());
AlertService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object])
], AlertService);

var _a;
//# sourceMappingURL=alert.service.js.map

/***/ }),

/***/ "./src/app/services/authentication.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_index__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__loader_service__ = __webpack_require__("./src/app/services/loader.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthenticationService = (function () {
    function AuthenticationService(http, cs, loader) {
        this.http = http;
        this.loader = loader;
        this.url = cs.root_dir;
    }
    AuthenticationService.prototype.login = function (user) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append("Authorization", "Basic " + btoa(user.username + ":" + user.password));
        return this.http.post(this.url + '/auth/login', "", { headers: headers, withCredentials: true }).map(function (response) { return _this.authentifySession(response.json()); });
    };
    AuthenticationService.prototype.logout = function () {
        var _this = this;
        // remove user from local storage to log user out
        return this.http.post(this.url + '/auth/logout', { withCredentials: true }).map(function (response) { return _this.authentifySession(response.json()); });
    };
    AuthenticationService.prototype.authentifySession = function (response) {
        if (response.token != undefined, response.token != null, response != undefined) {
            localStorage.removeItem('currentUser');
        }
        return response;
    };
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__models_index__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models_index__["Constants"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__loader_service__["a" /* LoaderService */]) === "function" && _c || Object])
], AuthenticationService);

var _a, _b, _c;
//# sourceMappingURL=authentication.service.js.map

/***/ }),

/***/ "./src/app/services/generic.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GenericService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_index__ = __webpack_require__("./src/app/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loader_service__ = __webpack_require__("./src/app/services/loader.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__alert_service__ = __webpack_require__("./src/app/services/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_observable_throw__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var GenericService = (function () {
    function GenericService(http, cs, alertService, requestOption, loader, router) {
        this.http = http;
        this.alertService = alertService;
        this.requestOption = requestOption;
        this.loader = loader;
        this.router = router;
        this.url = cs.root_dir;
        this.requestOption.withCredentials = true;
    }
    GenericService.prototype.create = function (entity, apiService) {
        var _this = this;
        this.loader.show();
        return this.http.post(this.url + apiService, entity, this.userAuth()).map(function (response) { return _this.authentifySession(response.json()); }).catch(this.errorHandler);
    };
    GenericService.prototype.get = function (apiService) {
        var _this = this;
        this.loader.show();
        return this.http.get(this.url + apiService, this.userAuth()).map(function (response) { return _this.authentifySession(response.json()); }).catch(function (err) { return _this.errorHandler(err); });
    };
    GenericService.prototype.getBySearch = function (apiService, parameters) {
        var _this = this;
        this.loader.show();
        this.requestOption.params = parameters;
        return this.http.get(this.url + apiService, this.userAuth()).map(function (response) { return _this.authentifySession(response.json()); }).catch(function (err) { return _this.errorHandler(err); });
    };
    GenericService.prototype.delete = function (entity, apiService) {
        var _this = this;
        this.loader.show();
        return this.http.delete(this.url + apiService + '/' + entity, this.userAuth()).map(function (response) { return _this.authentifySession(response.json()); }).catch(function (err) { return _this.errorHandler(err); });
    };
    GenericService.prototype.update = function (entity, apiService) {
        var _this = this;
        this.loader.show();
        return this.http.put(this.url + apiService, entity, this.userAuth()).map(function (response) { return _this.authentifySession(response.json()); }).catch(function (err) { return _this.errorHandler(err); });
    };
    GenericService.prototype.userAuth = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append("Authorization", localStorage.getItem('token'));
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser) {
            return {
                headers: headers,
                withCredentials: true
            };
        }
    };
    // TO DO
    GenericService.prototype.authentifySession = function (response) {
        this.requestOption.params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* URLSearchParams */]('');
        this.loader.hide();
        if (response.code == 400 ||
            response.message.code < 200 ||
            response.message.code >= 300 ||
            response.message == "Session expired" ||
            response.message == "No session id") {
            localStorage.removeItem('currentUser');
            localStorage.removeItem('token');
            this.router.navigateByUrl("/");
        }
        else {
            return response;
        }
    };
    GenericService.prototype.errorHandler = function (error) {
        if (error.status < 200 || error.status >= 300) {
            return __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__["Observable"].throw(error.json());
        }
    };
    return GenericService;
}());
GenericService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__models_index__["Constants"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models_index__["Constants"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__alert_service__["a" /* AlertService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__alert_service__["a" /* AlertService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__loader_service__["a" /* LoaderService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _f || Object])
], GenericService);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=generic.service.js.map

/***/ }),

/***/ "./src/app/services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__alert_service__ = __webpack_require__("./src/app/services/alert.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__alert_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__authentication_service__ = __webpack_require__("./src/app/services/authentication.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__authentication_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_service__ = __webpack_require__("./src/app/services/user.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_2__user_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__generic_service__ = __webpack_require__("./src/app/services/generic.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_3__generic_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pager_service__ = __webpack_require__("./src/app/services/pager.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_4__pager_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loader_service__ = __webpack_require__("./src/app/services/loader.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_5__loader_service__["a"]; });






//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/services/loader.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoaderService = (function () {
    function LoaderService() {
        this.loaderSubject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["a" /* Subject */]();
        this.loaderState = this.loaderSubject.asObservable();
        this.times = 0;
    }
    LoaderService.prototype.show = function () {
        this.times++;
        this.loaderSubject.next({ show: true });
    };
    LoaderService.prototype.hide = function () {
        this.times--;
        if (this.times == 0) {
            this.loaderSubject.next({ show: false });
        }
    };
    return LoaderService;
}());
LoaderService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], LoaderService);

//# sourceMappingURL=loader.service.js.map

/***/ }),

/***/ "./src/app/services/pager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagerService; });
var PagerService = (function () {
    function PagerService() {
    }
    PagerService.prototype.getPager = function (totalItems, currentPage, pageSize) {
        if (currentPage === void 0) { currentPage = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        return new Promise(function (resolve, reject) {
            // calculate total pages
            var totalPages = Math.ceil(totalItems / pageSize);
            var startPage, endPage;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            }
            else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                }
                else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                }
                else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }
            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
            // create an array of pages to ng-repeat in the pager control
            //    let pages = _.range(startPage, endPage + 1);
            var pages = Array.from(Array(endPage), function (x, i) { return startPage + i; });
            // return object with all pager properties required by the view
            resolve({
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            });
        });
    };
    return PagerService;
}());

//# sourceMappingURL=pager.service.js.map

/***/ }),

/***/ "./src/app/services/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserService = (function () {
    function UserService(http) {
        this.http = http;
        this.url = 'http://localhost:3055/CMS1/api';
    }
    UserService.prototype.getAll = function () {
        return this.http.get(this.url + '/user', this.jwt()).map(function (response) { return response.json(); });
    };
    UserService.prototype.getById = function (id) {
        return this.http.get(this.url + '/user/' + id, this.jwt()).map(function (response) { return response.json(); });
    };
    UserService.prototype.create = function (user) {
        return this.http.post(this.url + '/signup', user).map(function (response) { return response.json(); });
    };
    // private helper methods
    UserService.prototype.jwt = function () {
        // create authorization header with jwt token
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Authorization': 'Bearer ' + currentUser.token });
            return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        }
    };
    return UserService;
}());
UserService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], UserService);

var _a;
//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    apiUrl: '/mobile-engine/api/CMS'
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map