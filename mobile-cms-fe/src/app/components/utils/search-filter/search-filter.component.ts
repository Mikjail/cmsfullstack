import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Constants, API_REST, EntityToSearch } from '../../../models';
import { GenericService } from '../../../services';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss']
})
export class SearchFilterComponent implements OnInit {
  @Input('searchInput') searchFilter = {
    apiRest: '',
    availableSearch:{
      searchStatus: false,
      searchName:false,
      searchCountry: false,
      searchLanguage: false
    },
    name:'',
    countriesArray: [],
    countryFromServer: [],
    languageArray: [],
    makeAction:false
  };
  @Output('changeView') valueFromSearch = new EventEmitter();

  /**
   * For variable to create formvalidation.
   * @param myForm
   * @type {FormGroup}
   * @memberOf LabelComponent
   *
   * @param submitted
   * @type {boolean}
   */
  public myForm: FormGroup;

  /**
   * Filter Form.
   * 
   * @type {Array<string>}
   * @memberOf CallcenterComponent
   */
  public langSelected: Array<string>;
  public entityToSearch;
  public resultsFound = false;
  public countriesSelected;

  /**
 * ROOT URL HOST
 * 
 * @type {string}
 * @memberOf CallcenterComponent
 */
  public apiRest: API_REST;

  constructor(private fb: FormBuilder,
    private genServices: GenericService,
    private constants: Constants) {
    this.entityToSearch = new EntityToSearch();
    this.langSelected = new Array<string>();
    this.entityToSearch.country = [];
    this.countriesSelected = [];
    this.entityToSearch.name= '';
    this.apiRest = constants.IApiRest;
  }

  ngOnInit() {
    this.initForm();
    this.initCountries();


  }

  /**
   * Initialize inputs inside filter panel view.
   * 
   * 
   * @memberOf CallcenterComponent
   */
  initForm(): void {
    this.myForm = this.fb.group({
      name: new FormControl(''),
      country: new FormControl(''),
      language: new FormControl(''),
      published: new FormControl('')
    })
  }

  /**
   * Get all Country from server
   *
   *
   * @memberOf CallcenterComponent
   */
  initCountries(): void {
    this.genServices.get(this.apiRest.API_COUNTRY).subscribe(
      response => {
        this.searchFilter.countryFromServer = response.data.countries;
        this.searchFilter.countriesArray = response.data.countries.map(country => country.countryName);
      },
      error => {

      },
      () => {
        this.initLanguages();
      });
  }

  /**
  * Get all Languages from server
  *
  *
  * @memberOf CallcenterComponent
  */
  initLanguages(): void {
    this.genServices.get(this.apiRest.API_LANGUAGE).subscribe(
      response => {
        this.searchFilter.languageArray = response.data.languages.map(language => language.languageCode);
      },
      error => {

      },
      ()=>{
        
      });
  }


  onSearch(value) {
    let valueToSearch = new EntityToSearch();
    valueToSearch.name = this.entityToSearch.name.toLowerCase() || undefined;
    valueToSearch.country = this.entityToSearch.country || undefined;
    valueToSearch.language = this.langSelected || undefined;
    valueToSearch.published = this.entityToSearch.published || undefined;
    // ARREGLAR ACA Y ARREGLAR BACKEND
    this.genServices.getBySearch(this.searchFilter.apiRest, valueToSearch).subscribe(
      response => {
        this.valueFromSearch.emit({
          value: response.data
        });
      },
      error => {

      })
  }

  /**
  * Country Selected
  * Add each language selected toa an array of countrie.
  * @param {*} value
  *
  * @memberOf CallcenterComponent
  */
  public selectedCountry(value: any): void {
    this.countriesSelected.push(value.text);
    let countryFound = this.searchFilter.countryFromServer.find((country) => {
      return country.countryName == value.text;
    })
    this.entityToSearch.country.push(countryFound.countryCode);
  }

  /**
   * Every time is the country remove from input it will remove from 
   * @param this.countrySearch as well
   * 
   * @param {*} value 
   * 
   * @memberOf CallcenterComponent
   */
  public removedCountry(value: any): void {
    let index = this.countriesSelected.indexOf(value.text);
    this.countriesSelected.splice(index, 1);
    let countryFound = this.searchFilter.countryFromServer.find((country) => {
      return country.countryName == value.text;
    });

    index = this.entityToSearch.country.indexOf(countryFound);
    this.entityToSearch.country.splice(index, 1);
  }

  /**
    * Languages Selected
    * Add each language selected toa an array of languages.
    * @param {*} value
    *
    * @memberOf CallcenterComponent
    */
  public selectedLenguage(value: any, i = null): void {
    // console.log('Selected value is: ', value);
    this.langSelected.push(value.text);
  }

  /**
   * Every time is the language remove from input it will remove from 
   * @param this.langSelected as well
   * 
   * @param {*} value 
   * 
   * @memberOf CallcenterComponent
   */
  public removedLanguage(value: any) {
    let index = this.langSelected.indexOf(value.text);
    this.langSelected.splice(index, 1);
  }

  public typed(value: any): void {
    // console.log('New search input: ', value);
  }

  public refreshValue(value: any): void {
    // this.value = value;
  }

}
