import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards';
import { 
    RegisterComponent,
    HomeComponent,
    LoginComponent,
    ImageComponent,
    OfficeLocationComponent, 
    EmailsettingComponent,
    CampaignComponent,
    CallcenterComponent,
    ProfileComponent } from './components';

//canActivate: [AuthGuard], data: { roles: ['admin', 'user']} will be added once we have backend
const appRoutes: Routes = [

    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    
    {path: 'image', component: ImageComponent, canActivate: [AuthGuard]},

    {path: 'campaign', component: CampaignComponent, canActivate: [AuthGuard]},

    {path: 'officeLocation', component: OfficeLocationComponent, canActivate: [AuthGuard]},
    
    {path: 'callcenter', component: CallcenterComponent, canActivate: [AuthGuard]},
    
    {path: 'emailUs', component: EmailsettingComponent, canActivate: [AuthGuard]},

    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },

    { path: 'register', component: RegisterComponent },
    
    { path: 'login', component: LoginComponent },
    
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);