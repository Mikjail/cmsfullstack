FROM node:8.9.1

COPY mobile-cms-fe /build/mobile-cms-fe
COPY mobile-cms-be /build/mobile-cms-be

ARG CARRIER_CODE
ARG HTTP_PROXY
ARG HTTPS_PROXY

RUN cd /build/mobile-cms-fe ; npm install --loglevel verbose
RUN cd /build/mobile-cms-fe ; CLIENT=$CARRIER_CODE npm run buildClient  --loglevel verbose
RUN cd /build/mobile-cms-be ; npm install --loglevel verbose

WORKDIR /build

COPY mobile-cms-be/package.json /app/package.json
COPY mobile-cms-be/server /app/server

RUN cd /app; npm install --production --loglevel verbose

WORKDIR /app

EXPOSE 3055
ENTRYPOINT [ "node", "server/server.js" ]