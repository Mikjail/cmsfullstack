'use strict';
const Joi = require('joi');

const noValidation = Joi.validate(undefined, Joi.string()); // validates fine

const apiContextSchema = Joi.object().keys({
    HAS_PROXY: Joi.boolean().required(),
    HTTPS_PROXY: Joi.string().allow('').required(),
    SERVER_PORT:Joi.number().required(),
    SERVER_URL_PREFIX:Joi.string().allow('').required(),
    MONGO_DB_NAME: Joi.string(),
    MONGO_DB_ADDRESS:Joi.string(),
    MONGO_DB_CREDENTIALS: Joi.string().allow('').required(),
    MONGO_DB_PORT: Joi.number(),
    MONGO_DB_AUTHDB: Joi.string(),
    CMS_INSTANCE: Joi.string(),
    CLIENT: Joi.string(),
    CLIENT_URL: Joi.string(),
    PRIVATE_KEY: Joi.string(),
    USERS: Joi.object().allow('')
})

const loginSchema = Joi.object().keys({
    username: 'admin',
    password: 'admin'
})

const fileSchema = Joi.object().keys({
    filename: Joi.string()
})

const multiFileSchema = Joi.object().keys({
    fileNames: Joi.array()
})

const createCampaign = Joi.object().keys({
    countryName: Joi.string().required(),
    countryCode: Joi.string().required(),
    campaigns: Joi.array().items(
        Joi.object().keys({
            name: Joi.string().required(),
            properties: Joi.array().items(
            Joi.object().keys({
                language: Joi.string().required(),
                campaignName: Joi.string().required(),
                priority: Joi.string(),
                fileName: Joi.any(),
                link: Joi.string().allow(''),
                modifiedDate: Joi.string().required(),
                fromSchedule: Joi.string().required(),
                toSchedule: Joi.string().required()
            }).meta({ className: 'Properties' })).required()
        }).meta({ className: 'Campaigns Object' })),
}).description('Campaign Object')
.meta({ className: 'Campaign Object' }).error(new Error('Name and Link are required'));

const readCampaign = Joi.object().keys({
    country: Joi.any().description('Campaign country'),
    language: Joi.any().description('Campaign language')
})
    
const deleteCampaign = Joi.object().keys({
    countryCode: Joi.string().description('If name exist it will be deleted').required()
})

//LOCATION
const createLocation = Joi.object().keys({
    countryName: Joi.string().required(),
    countryCode: Joi.string().required(),
    cities: Joi.array().items(
        Joi.object().keys({
            city: Joi.string().required(),
            published: Joi.boolean().required(),
            properties: Joi.array().items(
                Joi.array().items(
                    Joi.object().keys({
                        cityName: Joi.string().required(),
                        language: Joi.string().required(),
                        address: Joi.string().required(''),
                        telephone: Joi.string().allow(''),
                        openingTime: Joi.string().allow(''),
                        latitude: Joi.number().allow(''),
                        longitude: Joi.number().allow('')
                    }).meta({ className: 'City Properties Object' })).required()
                )
        }).meta({ className: 'Cities Object' })),
}).description('To replace/create one location')
.meta({ className: 'Location Object' }).error(new Error('city and address are required'));

const createMultipleLocation = Joi.array().items(createLocation)
.description('To replace/create locations array')
.meta({ className: 'Location Array Object' });


const readLocation = Joi.object().keys({
    country: Joi.any().description('Location country'),
    language: Joi.string().description('Location language'),
    published: Joi.boolean().description('Location published')
});


const deleteLocation =  Joi.object().keys({
    countryCode:  Joi.string().description('If name exist it will be deleted').required()
})

// CALL CENTRE

const createCallCenter =Joi.object().keys({
    countryName: Joi.string().required(),
    countryCode: Joi.string().required(),
    cities: Joi.array().items(
        Joi.object().keys({
            city: Joi.string().required(),
            published: Joi.boolean().required(),
            properties: Joi.array().items(
                Joi.array().items(
                    Joi.object().keys({
                        cityName: Joi.string().required(),
                        language: Joi.string().required(),
                        telephone: Joi.string().allow(''),
                        openingTime: Joi.string().allow(''),
                    }).meta({ className: 'City Properties Object' })).required()
                )
        }).meta({ className: 'Cities Object' })),
}).description('To replace/create one location')
.meta({ className: 'Location Object' }).error(new Error('City is required'));

const createMultipleCallcenter =Joi.array().items(createCallCenter)
.description('To replace/create locations array')
.meta({ className: 'Location Array Object' });

const readCallCenter = Joi.object().keys({
    country: Joi.any().description('Callcenter country'),
    language: Joi.string().description('Callcenter language'),
    published: Joi.boolean().description('Callcenter published')
})
const deleteCallcenter =  Joi.object().keys({
    countryCode:  Joi.string().description('If name exist it will be deleted').required()
})
//CONTACT US
const createEmail =  Joi.object().keys({
    _id: Joi.any(),
    email: Joi.string().required(),
    published: Joi.boolean().required()
}).description('To Create a new Email')
.meta({className: 'Email Object'})

const readEmail =  Joi.object().keys({
    email: Joi.string().description('Contact Us Email'),
    published: Joi.boolean().description('Contact Us published/un-published'),
})

const deleteEmail = Joi.object().keys({
    email: Joi.string()
    .description('If name exist it will be deleted')
    .required()
})

//MOBILE

const mobileRequest = Joi.object().keys({
    app:Joi.object().keys({
        language:Joi.string().required()
    }),
    countryCode : Joi.string().allow('')
})

module.exports = {
    //Login
    loginSchema: loginSchema,
    fileSchema: fileSchema,
    multiFileSchema:multiFileSchema,
    //Campaign
    createCampaign : createCampaign,
    readCampaign: readCampaign,
    deleteCampaign: deleteCampaign,
    //Locations
    createLocation: createLocation,
    createMultipleLocation: createMultipleLocation,
    readLocation: readLocation,
    deleteLocation: deleteLocation,
    //Callcenters
    createCallCenter: createCallCenter,
    createEmail: createEmail,
    createMultipleCallcenter: createMultipleCallcenter,
    readCallCenter:readCallCenter,
    deleteCallcenter:deleteCallcenter,
    //Contact us
    readEmail: readEmail,
    deleteEmail: deleteEmail,
    //CONTEXT ENV VAR
    apiContextSchema:apiContextSchema,
    //Mobile
    mobileRequest:mobileRequest,
    //Novalidation
    noValidation: noValidation
}