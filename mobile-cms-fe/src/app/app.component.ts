import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  @Input('init') isOpen= { isOpen: ""};
  isAuth = false;
  authSubscription: Subscription;

  constructor(private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.isAuth = this.authService.isAuth();
    this.authSubscription = this.authService.authChange.subscribe(authState => {
      this.isAuth = authState;
    })
  }

  ngOnDestroy(){
    this.authSubscription.unsubscribe();
  }
}
