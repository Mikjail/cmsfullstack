import { Injectable, } from '@angular/core';
import { Http, Headers, RequestOptions,URLSearchParams, Response } from '@angular/http';
import { User, Constants } from '../models/index';
import { Router } from '@angular/router';
import { LoaderService } from './loader.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class GenericService {
    
    private url;
    constructor(private http: Http, cs: Constants,    
        private requestOption:RequestOptions,
        private loader: LoaderService, 
        private router: Router) { 
        this.url = cs.root_dir ;
        this.requestOption.withCredentials = true;
    }


    create(entity: any, apiService:string) {
        this.loader.show();
        return this.http.post(this.url + apiService, entity,  this.userAuth()).map((response: Response) =>  this.authentifySession(response.json())).catch(this.errorHandler); 
    }

    get(apiService){
        this.loader.show();
        return this.http.get(this.url + apiService, this.userAuth()).map((response: Response) =>  this.authentifySession(response.json())).catch(err=>this.errorHandler(err));
    }

    getBySearch(apiService, parameters){
        this.loader.show();
        this.requestOption.params = parameters;
        return this.http.get(this.url + apiService, this.userAuth()).map((response: Response) => this.authentifySession(response.json())).catch(err=>this.errorHandler(err));
    }
    
    delete(entity:any, apiService:any){
        this.loader.show();
        return this.http.delete(this.url + apiService+'/'+entity, this.userAuth()).map((response:Response)=> this.authentifySession(response.json())).catch(err=>this.errorHandler(err));
    }

    update(entity:any, apiService:any){
        this.loader.show();
        return this.http.put(this.url + apiService, entity,  this.userAuth()).map((response:Response)=> this.authentifySession(response.json())).catch(err=>this.errorHandler(err));
    }

    userAuth(){
        let headers: Headers = new Headers();
        headers.append("Authorization", localStorage.getItem('token')); 
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser){
            return {
                headers: headers,
                withCredentials:true
            }
        }
    }
    // TO DO
    authentifySession(response){
        this.requestOption.params= new URLSearchParams('');   
        this.loader.hide();
        if( response.code == 400 ||
            response.message.code < 200 ||
            response.message.code >= 300 || 
            response.message == "Session expired" ||
            response.message == "No session id"){
            localStorage.removeItem('currentUser');
            localStorage.removeItem('token');
            this.router.navigateByUrl("/");
        }
        else{
            return response;
        }   
    }

    errorHandler(error: Response | any){     
        if(error.status < 200 || error.status >= 300 ){
            return Observable.throw(error.json());
        }
    }
}