const path = require('path');
const propertiesReader = require('properties-reader');
const MongoClient = require('mongodb').MongoClient;
const replyHandler = require('../reply/replyHandler');

const { 
    MONGO_DB_NAME,
    MONGO_DB_CREDENTIALS, 
    MONGO_DB_ADDRESS,
    MONGO_DB_PORT,
    MONGO_DB_AUTHDB 
} = require('../constants');


class MongoDataBase{
    constructor(){
        console.log(`mongodb://${MONGO_DB_CREDENTIALS}@${MONGO_DB_ADDRESS}:${MONGO_DB_PORT}/${MONGO_DB_NAME}?authSource=${MONGO_DB_AUTHDB}`)
        this.uri = `mongodb://${MONGO_DB_CREDENTIALS}@${MONGO_DB_ADDRESS}:${MONGO_DB_PORT}/${MONGO_DB_NAME}?authSource=${MONGO_DB_AUTHDB}`;
        this.db={}
        return this;
    }   

    getDbUrl(){
        return this.uri;
    };

    async connect(){
        try{
            this.db = await MongoClient.connect(this.getDbUrl());
        }
        catch (error) {      
            throw error;
        }
    
    }

    addUpdateQuery (payload, dbName, propertyCriteria) {
       return new Promise((resolve, reject)=>{
            this.db.collection(dbName)
            .replaceOne(propertyCriteria, 
                payload,
                { 
                    safe: true,
                    upsert: true 
                },
            (error, dataUpdated)=>{
                if(error)throw new Error(error)
                else{
                    resolve(dataUpdated.ops[0]);
                }  
            })
            
       })
    }
    
    addMultipleQuery (payload, dbName) {
        return new Promise((resolve, reject)=>{
            this.db.collection(dbName)
            .bulkWrite(payload,
                (error, response) => {
                    
                    if(error)throw new Error(error)
                    else{
                        resolve(response);
                    }  
            })
        })
    }

    findQuery(propertyCriteria, dbName, limit){
        return new Promise((resolve, reject)=>{
            this.db.collection(dbName)
            .find(
                propertyCriteria
            ).limit(limit).toArray(
                (error, response) =>{
                    if(error)throw new Error(error)
                    else{
                        resolve(response);
                    }  
            })
        })
    }

    deleteQuery(payload, dbName){
        return new Promise((resolve, reject)=>{
            this.db.collection(dbName)
            .deleteOne(payload, 
                {
                    safe: true
                },
            (error, response) => {
                if(error)throw new Error(error)
                else{
                    resolve(response);
                }
            })  
        })
    }
    
}


module.exports = { MongoDataBase };