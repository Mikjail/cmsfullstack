const foundMobileResponse = function (err, data) {
    return {
        message: err ? err : {
             code: err ? 501 : 200,
             title: "success", 
             description: ""},
        data : err ? '' :{ campagins: data} 
    }
};

const mobileResponse = function (err,data) {
    return {
        message:{
            code: err ? 501 : 200,
            title:  err ? err: 'success',
            description: ''
        },
        data: data
        
    }
};

const foundResponse = function (err, data) {
    return {
        message: err ? err : {
            code: err ? 501 : 200,
            title: err ? 'error': "success", 
            description: ""
        },
        data: err ? []: data
    }
};

const createdResponse = function (err, data) {
    return {
        message: err ? err : {
            code: err ? 501 : 201,
            title: err ? 'error': "success", 
            description: ""
        },
        data: err ? []: data
    }
};

const deletedResponse = function (err,data) {
    return {
        message: err ? err : {
            code: err ? 501 : 204,
            title: "success", 
            description: ""
        },
        data: err ? []: data
    }
};



const replyAuth = function(err, credentialsMatch, token,data){
    return{
        message: {
            code:  !credentialsMatch ? 400: 200,
            title: !credentialsMatch ? "error" : "success", 
            description:!credentialsMatch ? 'Invalid username or Password': ''
        },
        data,
        token: token || null
    }
}


module.exports = {
    foundMobileResponse: foundMobileResponse,
    foundResponse: foundResponse,
    createdResponse: createdResponse,
    deletedResponse: deletedResponse,
    mobileResponse: mobileResponse,
    replyAuth:replyAuth
};
