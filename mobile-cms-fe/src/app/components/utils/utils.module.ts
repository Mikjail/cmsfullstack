import { CommonModule } from '@angular/common';
import { NgModule , CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap'; // Date Picker
import { BrowserModule } from '@angular/platform-browser';
import { 
    LoaderComponent,
    PaginationComponent,
    PriorityListComponent,
    GeolocationComponent,
    ModalCardViewComponent,
    SearchFilterComponent, 
    TitleNavComponent } from '.';
import { PagerService } from '../../services';
import { routing } from '../../app.routing';
//Third Party components
import { SelectModule } from 'ng2-select-compat'; // Input selectors
import { NestableModule } from 'ngx-nestable';

@NgModule({
    declarations:[
        PaginationComponent,
        PriorityListComponent,
        ModalCardViewComponent,
        GeolocationComponent,
        SearchFilterComponent,
        TitleNavComponent,
        LoaderComponent
    ],
    imports: [
        NestableModule,
        BrowserModule,
        CommonModule,
        routing,
        ReactiveFormsModule,
        FormsModule,
        SelectModule,
        BsDatepickerModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyCbqgtHD6CKZ2loD9w74EFIxLn9n5uWsE8',
            libraries: ["places"]
          }),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    exports: [
        BrowserModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        BsDatepickerModule,
        PaginationComponent,
        PriorityListComponent,
        ModalCardViewComponent,
        GeolocationComponent,
        SearchFilterComponent,
        TitleNavComponent,
        LoaderComponent
    ],
    providers:[
        PagerService
    ]
})

export class UtilsModule {}