import { Component, OnInit, Input } from '@angular/core';
import { Constants, API_REST } from '../../../models';

@Component({
  selector: 'app-modal-card-view',
  templateUrl: './modal-card-view.component.html',
  styleUrls: ['./modal-card-view.component.scss']
})
export class ModalCardViewComponent implements OnInit {
  @Input('entitySelected') selectedPrev;
    /**
   * ROOT URL HOST
   * 
   * @type {string}
   * @memberOf CampaignEditComponent
   */
  public rootDir: string;
  public apiRest: API_REST;

  constructor(  private constants: Constants) { 
    this.rootDir = constants.root_dir;
    this.apiRest = constants.IApiRest;
  }

  ngOnInit() {
  }
   /**
   * Parse imgSource.
   *
   * @param {string} imageName
   * @returns
   *
   * @memberOf CampaignEditComponent
   */
  getUrlImage(imageName: string): string {
    return this.rootDir + this.apiRest.API_FILES +"/"+ imageName;
  }
}
