import { Component, OnInit, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  // @Input('init') isOpen= false;
  isOpen=false;
  fixed = false;
  
  constructor() { }

  ngOnInit() {
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let num = document.body.scrollTop;
    if(num > 60){
      this.fixed = true;
      this.isOpen= false;
    }
    else if(this.fixed && num < 60){
      this.fixed=false;
      
    }
  }


}
