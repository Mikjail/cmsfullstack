import { TestBed, async } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AuthenticationService } from './services/index'
import { MockBackend} from '@angular/http/testing'
import { AppComponent } from './app.component';
import { Constants } from './models/index';
import { Router, RouterModule } from '@angular/router';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        // HttpModule
      ],
      declarations: [
        AppComponent
      ],
     schemas:[
          CUSTOM_ELEMENTS_SCHEMA
      ],
      providers: [
        // AuthenticationService,
        // Constants,
        // {provide: Router, useClass: RouterStub}
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have navBar 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.navBar).toEqual(true);
  }));

  it(`should have fixed property to enable disables fixed styles`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.navBar).toEqual(true);
  }));

  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  // }));
});

class RouterStub {
  navigateByUrl(url: string) { return url; }
}