const initConstant = function(name,value){
    if(this[name] !== undefined) throw ('constant: ' + name + ' was already initialized!');
    this[name] = value;
    console.log(name+":"+value);
};

const getFromPool = function(key){
    return this[key];
};

const initProperties = function(contextProperties){
    const hasProxy = contextProperties.HAS_PROXY;
    if(hasProxy){
        this.initConstant('HTTPS_PROXY',contextProperties.HTTPS_PROXY)
    }
    this.initConstant('SERVER_PORT', contextProperties.SERVER_PORT);
    this.initConstant('SERVER_URL_PREFIX', contextProperties.SERVER_URL_PREFIX);
    this.initConstant('MONGO_DB_NAME',contextProperties.MONGO_DB_NAME);
    this.initConstant('MONGO_DB_ADDRESS',contextProperties.MONGO_DB_ADDRESS);
    this.initConstant('MONGO_DB_CREDENTIALS',contextProperties.MONGO_DB_CREDENTIALS);
    this.initConstant('MONGO_DB_PORT',contextProperties.MONGO_DB_PORT);
    this.initConstant('MONGO_DB_AUTHDB',contextProperties.MONGO_DB_AUTHDB);
    this.initConstant('CMS_INSTANCE',contextProperties.CMS_INSTANCE);
    this.initConstant('CLIENT',contextProperties.CLIENT);
    this.initConstant('CLIENT_URL',contextProperties.CLIENT_URL);
    this.initConstant('PRIVATE_KEY',contextProperties.PRIVATE_KEY);
    this.initConstant('USERS',contextProperties.USERS);
}


module.exports = {
    initConstant : initConstant,
    initProperties: initProperties,
    USERS: getFromPool('USERS'),
    PRIVATE_KEY: getFromPool('PRIVATE_KEY'),
    CLIENT: getFromPool('CLIENT'),
    CLIENT_URL : getFromPool('CLIENT_URL'),
    CMS_INSTANCE: getFromPool('CMS_INSTANCE'),
    ALL_LANGUAGES_SET:require('./data_json/backupData.js').languageList,
    ALL_COUNTRIES_SET: JSON.stringify(require('./data_json/backupData.js').allCountries),
    HTTPS_PROXY : getFromPool('HTTPS_PROXY'),
    SERVER_PORT: getFromPool('SERVER_PORT'),
    SERVER_URL_PREFIX: getFromPool('SERVER_URL_PREFIX'),
    MONGO_DB_NAME: getFromPool('MONGO_DB_NAME'),
    MONGO_DB_IP: getFromPool('MONGO_DB_IP'),
    MONGO_DB_CREDENTIALS: getFromPool('MONGO_DB_CREDENTIALS'),
    MONGO_DB_PORT: getFromPool('MONGO_DB_PORT'),
    MONGO_DB_AUTHDB: getFromPool('MONGO_DB_AUTHDB'),
    SESSION_TIME: 900000, //15 mins
    SESSION_TIME_TEST_10_SEC: 10000 //15 sec
};