'use strict';
const replyHandler = require('./reply/replyHandler');
const auth = require('./auth/authefication');
const { CampaignController } = require('./controllers/Campaigns');
const { LocationController } = require('./controllers/Locations');
const { ContactUsController } = require('./controllers/ContactUs');
const { FileController } = require('./controllers/Files');
const { LoginController } = require('./controllers/LoginAuth.js')
const Joi = require('joi');
const validate = require('./utils/validation');
const validationSchema = require('./utils/validationSchemas');
const constants = require('./constants');
const docs = require('./documentation/swaggerdocs');
const path = require('path');


const configGenerator = (ctrlType, handler,validateName,typeOfValidation=null, auth=null, dbName=null) => {
    return {
        pre:[auth],
        handler:(request,reply) =>{
            if(request.authStatus.isAccessGranted){
                dbName ? ctrlType[handler](request,reply, dbName) :  ctrlType[handler](request,reply)
            }else{
                replyHandler.handleAuth(request,reply)
            }
        },
        tags: ['api'],
        validate: typeOfValidation ? typeOfValidation(validationSchema[validateName]): validationSchema.noValidation,
        description: docs[validateName].description,
        notes: docs[validateName].notes
    }
};

module.exports = [
    // SERVE INDEX
    {
        method: 'GET',
        path: '/CMS/{param*}',
        config: {
            auth: false,
            handler: {
                directory: {
                    path: path.join(`client/${constants.CLIENT}/dist`),
                    listing: false,
                    index: ['index.html']
                }
            }
    
        }
    },
    // LOGIN / SIGNUP
    {
        method: 'POST',
        path: '/api/CMS/auth/logout',
        handler: function (request, reply) {
             auth.logout(request,reply);
        }
    },
    {
        method: 'POST',
        path: '/api/CMS/auth/login',
        config: configGenerator(new LoginController, 'login', 'authLogin', validate.generateParamsValidator, auth.notAuth , 'users')
    },
    {
        method: 'POST',
        path: '/api/CMS/auth/changePass',
        config: configGenerator(new LoginController, 'changePass', 'authLogin', validate.generateParamsValidator, auth.authToken , 'users')
    },
    {
        method: 'POST',
        path: '/api/CMS/auth/updateUser',
        config: configGenerator(new LoginController, 'updateUserDetails', 'authLogin', validate.generateParamsValidator, auth.authToken , 'users')
    },
    // IMAGES
    {
        method: 'GET',
        path: '/api/CMS/files',
        config: configGenerator(new FileController, 'getFiles','mobileGetImages', null,auth.notAuth )
    },
    {
        method: 'GET',
        path: '/api/CMS/files/{filename}',
        config: configGenerator(new FileController,'getByQueryCriteria','fileSchema', validate.generateParamsValidator, auth.notAuth, 'files')
    },
    {
        method: 'POST',
        path: '/api/CMS/files',
        config: configGenerator(new FileController,'storeImages','fileStore', null, auth.authToken, 'files')

    },
    {
        method: 'POST',
        path: '/api/CMS/removeMultiFiles',
        config: configGenerator(new FileController,'removeMultiFile','fileDelete', null, auth.authToken, 'files')

    },
    // COUNTRY/LANGUAGE.
    {
        method: 'GET',
        path: '/api/CMS/allCountries',
        config: configGenerator(new CampaignController, 'getAllCountries', 'countryList',null, auth.notAuth)
    },
    {
        method: 'GET',
        path: '/api/CMS/allLanguages',
        config: configGenerator(new CampaignController, 'getAllLanguages', 'languageList', null, auth.notAuth)
    },
    // // CAMPAIGN
    {
        method: 'POST',
        path: '/api/CMS/campaigns',
        config: configGenerator(new CampaignController, 'addUpdate', 'createCampaign', validate.generatePayloadValidator, auth.authToken, 'campaigns')
    },
    {
        method: 'GET',
        path: '/api/CMS/campaigns',
        config: configGenerator(new CampaignController, 'getByQueryCriteria', 'readCampaign',validate.generateQueryValidator, auth.notAuth,'campaigns')
    },
    {
        method: 'DELETE',
        path: '/api/CMS/campaigns/{countryCode}',
        config: configGenerator(new CampaignController, 'removeByParams', 'deleteCampaign',validate.generateParamsValidator, auth.authToken, 'campaigns')

    }
    ,
    // // LOCATION
    {
        method: 'POST',
        path: '/api/CMS/locations',
        config: configGenerator(new LocationController, 'addUpdate', 'createLocation',validate.generatePayloadValidator, auth.authToken, 'locations')
    },
    {
        method: 'POST',
        path: '/api/CMS/multiLocations',
        config: configGenerator(new LocationController, 'addMultipleLocation', 'createMultipleLocation',validate.generatePayloadValidator, auth.authToken, 'locations')
    },
    {
        method: 'GET',
        path: '/api/CMS/locations',
        config: configGenerator(new LocationController, 'getByQueryCriteria', 'readLocation', validate.generateQueryValidator, auth.notAuth, 'locations')
    },
    {
        method: 'DELETE',
        path: '/api/CMS/locations/{countryCode}',
        config: configGenerator(new LocationController, 'removeByParams', 'deleteLocation', validate.generateParamsValidator, auth.authToken, 'locations')
    },
    // //  //CALL CENTER
     {
        method: 'POST',
        path: '/api/CMS/callcenters',
        config: configGenerator(new LocationController, 'addUpdate', 'createCallCenter', validate.generatePayloadValidator, auth.authToken, 'callcenters')

    },
    {
        method: 'POST',
        path: '/api/CMS/multiCallcenters',
        config: configGenerator(new LocationController, 'addMultipleLocation', 'createMultipleLocation', validate.generatePayloadValidator, auth.authToken, 'callcenters')
    },
    {
        method: 'GET',
        path: '/api/CMS/callcenters',
        config : configGenerator(new LocationController, 'getByQueryCriteria', 'readCallCenter', validate.generateQueryValidator, auth.notAuth, 'callcenters')
    },
    {
        method: 'DELETE',
        path: '/api/CMS/callcenters/{countryCode}',
        config: configGenerator(new LocationController, 'removeByParams', 'deleteCallcenter', validate.generateParamsValidator, auth.authToken, 'callcenters')
    },
    // // // Emails
    {
        method: 'POST',
        path: '/api/CMS/contactUs',
        config: configGenerator(new ContactUsController, 'addUpdateContactUs', 'createEmail', validate.generatePayloadValidator, auth.authToken, 'contactus')
    },
    {
        method: 'GET',
        path: '/api/CMS/contactUs',
        config: configGenerator(new ContactUsController, 'getByQueryCriteria', 'readEmail', validate.generateQueryValidator, auth.notAuth, 'contactus')
    },
    {
        method: 'DELETE',
        path: '/api/CMS/contactUs/{email}',
        config: configGenerator(new ContactUsController, 'removeByParams', 'deleteEmail', validate.generateParamsValidator, auth.authToken, 'contactus')
    },
   
    // // // MOBILE
    // // // Campaign-Mobile
    {
        method: 'POST',
        path: '/api/CMS/mobileCampaigns',
        config: configGenerator(new CampaignController, 'getMobileCampaings', 'mobileRequest', validate.generatePayloadValidator, auth.notAuth)
    },
    {
        method: 'POST',
        path: '/api/CMS/mobileLocations',
        config: configGenerator(new LocationController, 'getMobileLocations', 'mobileRequest', validate.generatePayloadValidator, auth.notAuth, "locations")
    },
    {
        method: 'POST',
        path: '/api/CMS/mobileCallCenter',
        config: configGenerator(new LocationController, 'getMobileLocations', 'mobileRequest', validate.generatePayloadValidator, auth.notAuth, "callcenters")
    },
];