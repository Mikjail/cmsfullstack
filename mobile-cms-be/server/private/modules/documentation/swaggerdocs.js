const authLogin = {
    description: 'CMS Website - User Login ',
    notes: 'Provides two inputs to login with credentials.'
}

// IMAGES

const mobileGetImages = {
    description: 'CMS Website - Files',
    notes: 'Route to get available links to files.'
}

const fileSchema = {
    description: 'Files - Get spicific file',
    notes: 'Route to get an specific file'
}
const fileStore = {
    description: 'Files- Insert a specific file',
    notes: 'Route to save an new image.'
}

const fileDelete = {
    description: 'Files - Remove One or More Files',
    notes: 'Route to get all Available files.'
}
//Country/Language
const countryList = {
    description: 'Countries - Available Countries',
    notes: 'Route to get available Countries'
}

const languageList ={
    description: 'Languages - Available Languages',
    notes: 'Route to get available Languages'
}

//CAMPAIGNS

const createCampaign = {
    description: 'Campaigns - Create a Campaign',
    notes: 'Route to create a custom campaign',
}

const readCampaign = {
    description: 'Campaigns - Available Campaigns By Criteria',
    notes: 'Route to get available campaigns by search criteria'
}

const deleteCampaign = {
    description: 'Campaigns - Delete a Campaign',
    notes: 'Route to delete a custom campaign',
}

const createLocation = {
    description: 'Location - Create one Location',
    notes: 'Route to create a custom location',
}

const createMultipleLocation = {
    description: 'Location - Create/Replace multiple Locations',
    notes: 'Route to create a custom location'
}

const readLocation = {
    description: 'Locations - Available Locations By Criteria',
    notes: 'Route to get available locations by search criteria',
}

const deleteLocation = {
    description: 'Locations - Delete Location',
    notes: 'Route will delete a location depending of its country code.'
}

const createCallCenter = {
    description: 'CallCenter Us - Create/replace callcenter',
    notes: 'Route will create/replace an callcenter'
}

const createMultipleCallcenter ={
    description: 'Location - Create/Replace multiple Locations',
    notes: 'Route to create a custom location'
}

const readCallCenter = {
    description: 'Contact Us - Available call centers by criteria',
    notes: 'Route to get available call centers by search criteria',
}

const deleteCallcenter = {
    description: 'Contact Us - Delete callcenters',
    notes: 'Route will delete a callcenter depending of its country code.'
}

//EMAILS
const createEmail = {
    description: 'Contact Us - Create/replace Email',
    notes: 'Route will create/replace an email'
}

const readEmail ={
    description: 'Contact Us - Available emails by criteria',
    notes: 'Route to get available emails by search criteria',
}

const deleteEmail = {
    description: 'Contact Us - Delete emails',
    notes: 'Route will delete a location depending of its country code.',
}

// Mobile Requests

const mobileRequest = {
    description: 'Mobile - Get Campaigns, Locations or Office',
    notes: 'Route will delete a location depending of its country code.',
}

const mobileContactUs= {
    description: 'Mobile - Get Current Mails',
    notes: 'Route will delete a location depending of its country code.'
}

module.exports = {
    //LOGIN
    authLogin : authLogin,
    //IMAGES
    mobileGetImages: mobileGetImages,
    fileSchema: fileSchema,
    fileStore: fileStore,
    fileDelete: fileDelete,
    //COUNTRY - LANGUAGES
    countryList:countryList,
    languageList: languageList,
    //CAMPAIGNS 
    createCampaign: createCampaign,
    readCampaign: readCampaign,
    deleteCampaign: deleteCampaign,
    //LOCATIONS
    createLocation: createLocation,
    createMultipleLocation: createMultipleLocation,
    readLocation: readLocation,
    deleteLocation: deleteLocation,
    //CALL CENTER
    createCallCenter: createCallCenter,
    createMultipleCallcenter: createMultipleCallcenter,
    readCallCenter : readCallCenter,
    deleteCallcenter : deleteCallcenter,
    //EMAIL US
    createEmail: createEmail,
    readEmail : readEmail,
    deleteEmail: deleteEmail,
    //Mobile
    mobileRequest: mobileRequest,
    mobileContactUs : mobileContactUs,
    mobileContactUs: mobileContactUs
}