export class Country{
    countryName: string;
    countryCode: string;

    constructor(countryName= "", countryCode=""){
        this.countryName = countryName;
        this.countryCode = countryCode;
    }
}
