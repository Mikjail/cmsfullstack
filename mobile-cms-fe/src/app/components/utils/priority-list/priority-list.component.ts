import { Component, OnInit, Input } from '@angular/core';
import { NestableSettings } from 'ngx-nestable/src/nestable.models';

@Component({
  selector: 'app-priority-list',
  templateUrl: './priority-list.component.html',
  styleUrls: ['./priority-list.component.scss']
})
export class PriorityListComponent implements OnInit {
  public options = {
    fixedDepth: true
  } as NestableSettings;
  @Input('propertyList') propertyList=  [];

  constructor() { }

  ngOnInit() {
  }

}
