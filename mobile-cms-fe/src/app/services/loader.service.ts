import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { LoaderState } from '../models/loader';

@Injectable()
export class LoaderService {
private loaderSubject = new Subject<LoaderState>();
loaderState = this.loaderSubject.asObservable();
times=0;
constructor() { }
show() {
        this.times++;
        this.loaderSubject.next(<LoaderState>{show: true});
    }
hide() {
        this.times--;
        if(this.times ==0){
        this.loaderSubject.next(<LoaderState>{show: false});
        }
    }
}