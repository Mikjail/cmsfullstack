import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormArray, ReactiveFormsModule, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService, GenericService } from '../../../services/index';
import { Image, Constants, API_REST, ContactUs, ContactToSearch } from '../../../models/index';
import { PaginationComponent } from '../../utils/pagination/pagination.component';
import { SearchFilterComponent} from '../../utils/search-filter';

@Component({
  selector: 'app-emailsetting',
  templateUrl: './emailsetting.component.html',
  styleUrls: ['./emailsetting.component.scss']
})
export class EmailsettingComponent implements OnInit {
  @Input('init') locationToEdit = { edition: false, emailList: new ContactUs() };
  @ViewChild(PaginationComponent) changePagination: PaginationComponent;
  @Input('searchInput') searchFilter = { 
    apiRest:'', 
    availableSearch:{
      searchStatus: true,
      searchName:true,
      searchCountry: false,
      searchLanguage: false
    },
    searchStatus: true,
    countryFromServer:[],
    languageArray:[]
   };
/**
   * TODO AFTER GET FROM SEARCH SEND IT TO ENTITYCOMPONENT
   * Search Filter Directive.
   * 
   * @type {SearchFilterComponent}
   * @memberof OfficeLocationComponent
   */
  @ViewChild(SearchFilterComponent) searchFilterComponent: SearchFilterComponent;
  getFromSearch(emailFound){
    this.locationToEdit.edition = false;
    if(emailFound.value.length > 0){
      this.emails = emailFound.value; 
      this.paginationList.entityList = this.emails;
      this.resultsFound = true;
      if( this.changePagination != undefined){
        this.changePagination.setPage(1);
     }
    }else{
      this.resultsFound = false;
    }
  } 

  public emailForm: FormGroup;
  public multipleEmail;
  public selectOptions;
  public mailToPublish;
  public emails;
  public resultsFound;
  public emailToDelete;
  public emailToCreate;
  public emailToSearch;
  public emailToEdit;
  public editView;
  public emailCreationSetting;
  /**
   * ROOT URL HOST
   * 
   * @type {string}
   * @memberOf CampaignEditComponent
   */
  public rootDir: string;
  public apiRest: API_REST;
  
 /**
   * PAGINATION
   * 
   * @private
   * @type {any[]}
   * @memberOf CampaignListComponent
   */
  public paginationList={ 
    entityList:[], 
    pagedItems: [],
     pager: {},
     customNumberPages: 20
  };


  
  constructor(private fb: FormBuilder,
    private genServices: GenericService,
    private alertService: AlertService,
    private constants: Constants) {
      this.rootDir = constants.root_dir;
      this.apiRest = constants.IApiRest;
      this.searchFilter.apiRest = this.apiRest.API_EMAILUS;
      this.multipleEmail=[];
      this.resultsFound =false;
      this.emailToEdit = new ContactUs();
      this.emailToCreate = new ContactUs();
      this.emailToSearch = new ContactUs();
      this.emails=[];
     }

  ngOnInit() {
    this.initForm();
    this.initEmailus();
    this.selectOptions='Bulk Action';
    this.emailToEdit.published="false";
    this.emailToCreate.published="false";
  }

  initForm(): void {
    this.emailForm = this.fb.group({
      email: new FormControl(''),
      published: new FormControl(''),
      publishedEdit: new FormControl('')
    })
  }

  /**
   * This get all Emails and save it in
   * @param emails 
   * After that it will initialize entity list for pagination component
   * 
   * @memberOf EmailsettingComponent
   */
  initEmailus(){
    this.genServices.get(this.apiRest.API_EMAILUS).subscribe(
      response =>{
          if(response.data.length>0){
            this.emails = response.data;
            this.mailToPublish = new Array<Boolean>(this.emails.length);
            this.paginationList.entityList = this.emails;
            if( this.changePagination != undefined){
              this.changePagination.setPage(1);
           }
            this.resultsFound=true;
          }else{
            this.resultsFound=false;
          }
      },
      error =>{

      },
      () =>{

      })
  }

  /**
   * This will create/update an email.
   * 
   * 
   * @memberOf EmailsettingComponent
   */
  onSubmit(contactUs:ContactUs){
    // console.log(contactUs);
    contactUs.email = contactUs.email.toLowerCase();
    this.genServices.create(contactUs,this.apiRest.API_EMAILUS).subscribe(
      response =>{
          this.emailCreationSetting=false;
          this.onSearch(new ContactUs());
      },
      error=>{

      },
      ()=>{

      }
    )
  }

  /**
   * This will delete an email from list.
   * 
   * 
   * @memberOf EmailsettingComponent
   */
  onDelete(){
    this.genServices.delete(this.emailToDelete.email, this.apiRest.API_EMAILUS).subscribe(
      response =>{

          this.emails.splice(this.emails.indexOf(this.emailToDelete),1);

      },
      error=>{

      },
      ()=>{
        this.onSearch("");
      }
    )
  }

  onSearch(contactus){
    let valueToSearch=new ContactToSearch();
    if(contactus.email != ''&& contactus.published != undefined){
      valueToSearch.email = contactus.email.toLowerCase();
    }
    if(contactus.published != '' && contactus.published != undefined){
      valueToSearch.published= contactus.published=='true';
    }
    // console.log(valueToSearch);
      this.genServices.getBySearch(this.apiRest.API_EMAILUS,valueToSearch).subscribe(
        response =>{
          if(response.data.length >0){
            this.emails = response.data;
            this.mailToPublish = new Array<Boolean>(this.emails.length);
            this.paginationList.entityList = this.emails;
            if( this.changePagination != undefined){
              this.changePagination.setPage(1);
           }
            this.resultsFound =true;
          }
          else{
            this.resultsFound =false;
          }
        },
        error=>{

        },
        () =>{

        })
  }
  /**
   * Once the modal is toggle this will add value selected 
   * into @param emailToEdit 
   * 
   * @param {any} emailToEdit 
   * 
   * @memberOf EmailsettingComponent
   */
  editEmail({...emailToEdit}){
    this.editView=true;
    this.emailToEdit = emailToEdit;
  
  }

  /**
   *Once the modal is toggle this will add value selected 
   * into @param emailToDelete
   * @param {any} emailToDelete 
   * 
   * @memberOf EmailsettingComponent
   */
  selectEmailToDelete(emailToDelete){
    this.editView=false
    this.emailToDelete = emailToDelete;
    
  }

  toggleStatus(emailToEdit, event){
    emailToEdit.published = event.srcElement.value == 'true';
  }

  validSettingForm(){
    const validation = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return validation.test(this.emailToCreate.email);
  }

  addNewEmail(emailToCreate){
    this.emails.push(emailToCreate);
    this.onSubmit(new ContactUs(emailToCreate.email,  emailToCreate.published));
  }
}
