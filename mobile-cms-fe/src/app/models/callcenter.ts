import { CityModel, Entity } from "./Entity";

export class CallCenter extends Entity{
    constructor(countryName = '', countryCode= '', cities= new Array<CityModel>()){
        super(countryName,countryCode, cities )
    }
}
