const memCache = require('memory-cache');
const constants = require('./constants');
const hash = require('object-hash');

const getFromCache = (id) => {
    return memCache.get(id);
};

const putToCache = (id, value) => {
    memCache.put(id, value, constants.SECONDS_IN_DAY, function (key, value) {
    });
};

const updateCache = function (key, value) {
    memCache.put(key, value,constants.SECONDS_IN_DAY,function (key,value) {});
    return value;
};

module.exports = {
    get: getFromCache,
    put: putToCache,
    updateCache: updateCache
};
