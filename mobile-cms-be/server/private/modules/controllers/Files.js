const MongoClient = require('mongodb').MongoClient;
const { MongoDataBase } = require('../mongodb/mongoCommonQuery');
const ParentController = require('./ParentController');
const replyHandler = require('../reply/replyHandler');
const replyMssgs = require('../reply/replyMssgs');
const _ = require('underscore');

class FileController extends ParentController{
    constructor(){
        super();
    }

    async getFiles(request, reply) {
        const MongoDB = new MongoDataBase();
        try{
            await MongoDB.connect();

            const res = await MongoDB.db.collection("files").find().toArray();
            const fileNameList = _.map(res, function (file) {
                    return _.pick(file, "filename");
                });
                
                reply(replyMssgs.foundResponse(null, fileNameList));
          
        }catch(error){
            
        reply(replyMssgs.createdResponse(error, null));
                
        }finally{

            MongoDB.db.close();
        }
    };

    async storeImages (request, reply, dbName) {
        let numberFiles = 0;
        for (let variable in request.payload) {
            numberFiles++;
            await this.storeImage(variable, request.payload[variable], dbName);
        }
        replyHandler.handleOk({ filesDownloaded: numberFiles }, reply);
    };

    async storeImage(filename, file, dbName){
        const MongoDB = new MongoDataBase();
        try{
            
            await MongoDB.connect();
    
            const payload =   {
                filename: filename,
                file: file
            }
    
            const res = await MongoDB.addUpdateQuery(payload, dbName,{ filename });   
            
        }catch(error){
            reply(replyMssgs.createdResponse(error, null));
        }finally{
            MongoDB.db.close();
        }
    };

    async removeMultiFile(request, reply) {
        const MongoDB = new MongoDataBase();
        try{
            await MongoDB.connect();    
            
            const filesToDelete = await this.parseMultipleQuery(request.payload.fileNames);
            
            const res = await MongoDB.db.collection("files").bulkWrite(filesToDelete);

            reply(replyMssgs.createdResponse(null, res));
            
        }catch(error){
            reply(replyMssgs.createdResponse(error, null));
        }finally{

            MongoDB.db.close();
        }
    
    };

    parseMultipleQuery(files) {
        return new Promise((resolve, reject) => {
           const filesToUpdate = []
            files.forEach(file => {
                filesToUpdate.push({
                    deleteOne:
                    {
                        "filter": { filename: file },
                    }
                })
            });
            if (filesToUpdate.length == files.length) {
                resolve(filesToUpdate);
            }
        })
    
    }
    
}

module.exports = {FileController};
