import { Component, OnInit, OnDestroy } from '@angular/core';

import { AlertService } from '../services/index';
import { Subscription } from 'rxjs/Subscription';

@Component({
    moduleId: module.id,
    selector: 'alert',
    templateUrl: 'alert.component.html'
})

export class AlertComponent implements OnInit, OnDestroy {
    message: any;
    authSubscription: Subscription;
    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.authSubscription = this.alertService.getMessage().subscribe(message => { this.message = message; });
        
    }
    ngOnDestroy(){
        this.authSubscription.unsubscribe();
    }
}