import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService,LoaderService } from '../../../services';
import { UserModel } from '../../../models';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  navBar=false;
  isAuth=false;
  user:UserModel; 
  authSubscription: Subscription;
  constructor(private authService: AuthenticationService, private router: Router, private loader: LoaderService ) { 
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));   
  }
  
  logOut(){
    this.authService.logout().subscribe(
      response => {
        this.navBar=false;
      },
      error =>{
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        this.router.navigateByUrl("/login");
      })
  }


}