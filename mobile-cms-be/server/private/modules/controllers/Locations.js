const MongoClient = require('mongodb').MongoClient;
const { MongoDataBase } = require('../mongodb/mongoCommonQuery');
const ParentController = require('./ParentController');
const replyHandler = require('../reply/replyHandler');
const replyMssgs = require ('../reply/replyMssgs');
const CriteriaBuilder = require('../utils/criteriaBuilder');
const _ = require('underscore');



class LocationController extends ParentController{
    constructor(){
        super();
    }

    async addMultipleLocation(request, reply, dbName) {
        const MongoDB = new MongoDataBase();
        try{
            await MongoDB.connect();
            
            const locationsToReplace = await this.parseMultipleQueryInsert(request.payload);
           
            const res = await MongoDB.addMultipleQuery(locationsToReplace, dbName);
            
                reply(replyMssgs.createdResponse(null, res));
    
        }catch(error){
            reply(replyMssgs.createdResponse(error, null));
        }finally{
            MongoDB.db.close();
        }
    }

   
    async getMobileLocations(request, reply, dbName){
        const {countryCode, language} =request.payload.app;
        const limit = request.url.query.limit ? parseInt(request.url.query.limit) : Number.MAX_SAFE_INTEGER;
        const criteriaBuilder = new CriteriaBuilder();
        const MongoDB = new MongoDataBase();
        try{
            
            await MongoDB.connect();
                
            criteriaBuilder.filterByCountryLanguageLoc(countryCode, language);
        
            const res = await MongoDB.findQuery(criteriaBuilder.build(), dbName, limit);
            
            reply(replyMssgs.mobileResponse(null, this.parseLocationsToMobile(res, request.payload)));
    
        }catch(error){
            reply(replyMssgs.createdResponse(error, null));
        }finally{
            MongoDB.db.close();
        }
        
    }

   parseLocationsToMobile (locations, payload){
        let mobileResult=[];
            locations.forEach((location) => {
                let countriesArray= {countryName: '', countryCode: '', cities:[]}
                    countriesArray.countryCode = location.countryCode;
                    countriesArray.countryName = location.countryName;
                location.cities.forEach( async (locationCity)=> { 
                   let offices= await this.getOffices(locationCity , payload.app.language);
                    if(locationCity.published && offices.length>0){
                        countriesArray.cities.push({
                            city: offices[0].cityName,
                            office: offices
                        })
                        
                    }
                }); 
                mobileResult.push(countriesArray);
            });
        return mobileResult;
    }

    getOffices(locationCity, language){
        return new Promise((resolve ,reject) => {
            let offices= []
            locationCity.properties.forEach((office)=>{
                office.forEach((property)=>{
                    if(property.language == language){
                        offices.push(property) 
                    }
                })
            })
            resolve(offices);
        })
  
    }

    parseMultipleQueryInsert(locations) {
        return new Promise((resolve, reject) =>{
            let locationsToUpdate=[]
            locations.forEach(location => {
                locationsToUpdate.push({replaceOne :
                    {
                    "filter" : { countryCode: location.countryCode},
                    "replacement" : location
                    }
                })
            });
            if(locationsToUpdate.length == locations.length){
                resolve(locationsToUpdate);
            }
        })
    }

}

module.exports = {
    LocationController
};