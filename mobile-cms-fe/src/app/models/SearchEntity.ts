export class EntityToSearch{
    public name: string;
    public country:Array<string>;
    public language:Array<string>;
    public published: boolean;
}
