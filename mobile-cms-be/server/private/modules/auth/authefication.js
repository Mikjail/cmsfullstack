const sessionCache = require('memory-cache');
const bcrypt = require('bcryptjs');
const uuid1 = require('uuid/v1');
const base64 = require('base-64');
const Joi = require('joi');
const cookieManager = require('cookieparser');
const jwt = require('jsonwebtoken');
const replyHandler = require('../reply/replyHandler');
const { PRIVATE_KEY, USERS, SESSION_TIME} = require('../constants');

const notAuth= function( request, reply){
    request.authStatus = { 
        isAccessGranted: true, 
        code: 200, 
        message: 'No credentials needed' 
    }
    replyHandler.handleAuth(request, reply);
}

const authCredential= async (request,reply) => {
    if(hasCredentials(request)){
        request.authStatus = await credentialsStatus(request);
        replyHandler.handleAuth(request, reply);
    }   
}
const authToken = async (request, reply) => {
    
    if(hasCredentials(request,reply)){
        request.authStatus = await tokenStatus(request)
    }
    replyHandler.handleAuth(request, reply);
}

const hasCredentials = function (request) {
    const authCredErrors = Joi.validate(request, requestBasicAuthSchema, {stripUnknown: true}).error;
    
    request.authStatus = { 
        isAccessGranted: authCredErrors==null || false, 
        code: authCredErrors? 400 :  200, 
        message: 'Empty credential' 
    }
    return request.authStatus.isAccessGranted;
   
};

const credentialsStatus = function (request) {
    return new Promise(resolve=>{
        const credentials = request.headers.authorization ? base64.decode(request.headers.authorization.split(" ")[1]) : null;
        const username = credentials.split(":")[0];
        const password = credentials.split(":")[1];
        let token="";
        const existingPassword = USERS[username];
        const credentialsMatch = !existingPassword ? false : bcrypt.compareSync(password, existingPassword);
        // let sessionId = credentialsMatch ? createSession(request) : cookieManager.parse(request.headers.cookie).SID;
        
        if(credentialsMatch){
            token = jwt.sign({ accountId: username }, PRIVATE_KEY, { algorithm: 'HS256'} );
        }
       
        resolve ({
            isAccessGranted: credentialsMatch,
            message:{
                code: credentialsMatch? 200 : 401,
                title: credentialsMatch ? 'Ok' : 'Password or name is wrong',
                description: ''
            },
            token:  token
        }) 
    })
    
    // return generateAuthStatus({
    //     isAccessGranted: credentialsMatch,
    //     message:{
    //         code: 401,
    //         title: credentialsMatch ? 'Ok' : 'Password or name is wrong',
    //         description: ''
    //     } 
    // },sessionId)
};


const tokenStatus = function (request) {
    return new Promise(resolve =>{
        try{
            const token = request.headers.authorization;
            const decoded = jwt.verify(token, PRIVATE_KEY);
            const isAccessGranted = USERS[decoded.accountId]? true: false; 
                resolve({
                    isAccessGranted: isAccessGranted,
                })
            }catch(err){
                resolve({
                    isAccessGranted: false,
                    code: 400,
                    message: 'Empty credentials and absent session TokenId'
                })
            }

    })
   
     
}

const authenticate = function (request) {
    const sessionId = hasSession(request);
    const itHasCredentials = hasCredentials(request);
    if (itHasCredentials) {
        return credentialsStatus(request);
    }
    else if (sessionId) {
        return sessionStatus(sessionId);
    }
    return {
        isAccessGranted: false,
        code: 401,
        message: 'Empty credentials and absent session id'
    };
};


const requestBasicAuthSchema = {
    headers: Joi.object().keys({
        authorization: Joi.string().required()
    })
};

const requestCookieAuthSchema = {
    headers: Joi.object().keys({
        cookie: Joi.string().required()
    })
};


const generateAuthStatus = function (status,sessionId) {
    refreshSession(sessionId);
    return status;
};

const hasSession = function (request) {
    let sid = false;
    const authValidationErrors = Joi.validate(request, requestCookieAuthSchema, {stripUnknown: true}).error;
    if (!(authValidationErrors === null)) return sid;
    
    try {
        sid = cookieManager.parse(request.headers.cookie).SID;
    } catch (err) {
    }
    return sid;
};

const logout = function (request,reply) {
    const sessionId = hasSession(request);
    if(sessionId){
        sessionCache.del(sessionId);
        request.authStatus =  {
            isAccessGranted: false,
            message:{
                code: 419,
                title: 'Session expired',
                description: ''
            } 
        };
    }else{
        request.authStatus = {
            isAccessGranted: false,
            message:{
                code: 401,
                title: 'No session id',
                description: ''
            } 
        }
    }
    replyHandler.handleAuth(request,reply);
};
module.exports = {
    authenticate: authenticate,
    logout: logout,
    authToken: authToken,
    authCredential: authCredential,
    notAuth: notAuth
};
