import { MainEntity, MainBasedProps, MainProperties } from "./MainEntity";

export class Entity extends MainEntity{
    cities:Array<CityModel>;
    constructor(countryName = '', countryCode= '', cities= new Array<CityModel>()){
        super(countryName, countryCode);
        this.cities = cities;
    }   
    
}

export class CityModel extends MainBasedProps{
    city:string;
    published: boolean;

    constructor(
     city:string="",
     properties = new Array<Array<CityProperty>>(),
     published:boolean=false){
        super(properties);
        this.city = city;
        this.published = published;
    }
}
export class CityProperty extends MainProperties{
    openingTime: string;
    telephone: string;
    cityName: string;

    constructor( 
    language:string="",
    cityName: string="",
    openingTime:string="",
    telephone:string=""){
        super(language);
        this.cityName = cityName;
        this.openingTime = openingTime;
        this.telephone = telephone;
    }
}