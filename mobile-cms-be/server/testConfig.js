var Mocha = require("mocha");

var mocha = new Mocha({
    ui: "tdd",
    reporter: "spec"
});

mocha.addFile("./server/e2e.js");
mocha.run();