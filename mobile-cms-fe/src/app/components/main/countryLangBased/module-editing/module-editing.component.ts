import { Component, OnInit, ViewChild, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import { FormArray, ReactiveFormsModule, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService, GenericService, PagerService, LoaderService } from '../../../../services';
import { Constants, API_REST, Entity, MainBasedProps, CityModel, CityProperty, CampaignProperty, OfficeProperties, CityOffices, CampaignBasedModel, MainProperties, Campaign, CallCenter, OfficeLocation, MainEntity, CampaignEntity, CampaignBasedEntity } from '../../../../models';
import { PaginationComponent } from '../../../utils';
import { SelectModule } from 'ng2-select-compat';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { PropertyBindingType } from '@angular/compiler';

@Component({
  selector: 'app-module-editing',
  templateUrl: './module-editing.component.html',
  styleUrls: ['./module-editing.component.scss']
})
export class ModuleEditingComponent implements OnInit {
  formSubmitAttempt=false;
  @Input('nameModule') nameModule:any =""
  @ViewChild(PaginationComponent) changePagination: PaginationComponent;
  @Input('init') locationToEdit = {
    edition: false,
    entityLocation: new Entity(),
    cityIndexSelected: '',
    apiService: '',
    module: '',
    mainEntity:'',
    citiesEntity: ''
  };
  @Input('newButton') newButton:any = "Office" 
  @Output('newListValue') valueFromEdit = new EventEmitter();
  colorTheme = 'theme-red';

  /**
   * PAGINATION - CREATE MODAL IMAGES COMP
   * 
   * @private
   * @type {any[]}
   * @memberOf CampaignListComponent
   */
  public indexForm;
  public selectedFile: string;
  public selectedUploadFile: File;
  public files
  public newImageSelected: boolean;
  public paginationList = {
    entityList: [],
    pagedItems: [],
    pager: {},
    setPage: {},
    customNumberPages: 4
  };

  /**
 * Variable to create formvalidation.
 * @param myForm
 * @type {FormGroup}
 * @memberOf LabelComponent
 *
 * @param submitted
 * @type {boolean}
 */
  public submitted;
  public myForm: FormGroup;
  public arrayForm;
  public languageSelected;
  public existingLanguage;
  public langSelectionSetting = false;
  public languagesArray;
  public availableSelectionLanguage;
  public toggleLocationModal = false;
  public toggleCampaignModal =false;
  public locationCityToEdit;
  public cityToEdit;
  public indexCity;
 

  /**
 * Variables used in new Language selection.
 * 
 * 
 * @memberOf OfficeLocationEditComponent
 */
  public languageToCopy = 'Copy Content From';
  public newLanguageCopied = 'Select New Language';

  /**
   * Variables to manipulate upload image.
   * @param imgStatus
   * @type {boolean}
   *
   * @param imageUrl
   * @type {string}
   * @memberOf CampaignEditComponent
   */

  public fileForm: FormGroup;
  public imgStatus: boolean;
  public imageUrl: string;
  public filesLoaded:boolean =false;
  /**
   * ROOT URL HOST
   * 
   * @type {string}
   * @memberOf CampaignEditComponent
   */
  public rootDir: string;
  public apiRest: API_REST;

  /**
   * Date Picker
   * 
   * @type {Partial<BsDatepickerConfig>}
   * @memberOf CampaignEditComponent
   */
  bsConfig: Partial<BsDatepickerConfig>;


  constructor(private fb: FormBuilder,
    private genServices: GenericService,
    private alertService: AlertService,
    private loader: LoaderService, 
    private constants: Constants) {
    this.rootDir = constants.root_dir;
    this.apiRest = constants.IApiRest;
    this.bsConfig = Object.assign({}, { dateInputFormat: 'YYYY/MM/DD', containerClass: this.colorTheme });
    
  

  }

  ngOnInit() {
    this.languageSelected = 0;
    this.indexCity = this.locationToEdit.cityIndexSelected;
    this.initEditionForm();
    this.initLanguages();
    this.setExistingLanguages();
    if(this.locationToEdit.module =='Campaign'){
      this.files = [];
      this.initFileForm();
    }
  }
    /**
   * Listing all files from server - CREATE MODAL IMAGES COMP
   * 
   * 
   * @memberOf CampaignEditComponent
   */
  initFiles(): void {
    this.genServices.get(this.apiRest.API_FILES).subscribe(
      response => {
        this.files = response.data;
        this.paginationList.entityList= this.files.reverse();
      },
      error=>{
        
      },
      () => {

      })
  }


  /**
  * Get all Languages from server
  *
  *
  * @memberOf CampaignEditComponent
  */
  initLanguages(): void {
    this.genServices.get(this.apiRest.API_LANGUAGE).subscribe(
      response => {
        this.languagesArray = response.data.languages.map(language => language.languageCode);
      },
      error => {

      });
  }

    /**
   * This will be used when user wants to add a new file.
   * - CREATE MODAL IMAGES COMP
   * 
   * 
   * @memberOf CampaignEditComponent
   */
  initFileForm(): void {
    this.fileForm = this.fb.group({
      image: new FormControl('')
    })
  }

  /**
    * Initialize form in case it is for edit porpose.
    * 
    * 
    * @memberOf CampaignEditComponent
    */
  initEditionForm() {
    this.myForm = this.fb.group({
      city: new FormControl(this.locationToEdit.entityLocation.countryName),
      arrayCityForm: this.fb.array([]),
    })
    this.arrayForm = (<FormArray>this.myForm.controls.arrayCityForm).controls;
    //TODO ARRAY OF CITIES
    // this.arrayForm.push(this.fb.array[])
    if (this.locationToEdit.entityLocation.cities[this.indexCity].properties.length > 0) {
        this.addCityForm(this.locationToEdit.entityLocation.cities[this.indexCity]);
    }
  }

  /**
   * Adding a new City Form
   * 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  async addCityForm(city=undefined) {
    let cityForm={};
    console.log(city)
    if(!city){ city = await this.initCity() }
      cityForm = this.createFormField(city);

      city.properties.forEach(property =>{
        const control = <any>this.myForm.controls['arrayCityForm'];
        let formGroup = this.createForm(property)
        formGroup = Object.assign(formGroup, cityForm);
        control.push(this.fb.group(formGroup));
      })
  }


/**
 * Create a new property and return it
 * 
 * @param {MainProperties} properties 
 * @returns 
 * @memberof ModuleEditingComponent
 */
addPropertyInstance(properties: MainProperties){
    if (properties[0] == undefined) {
      let newProp=[];
      let size = (<any> this.locationToEdit.entityLocation.cities[this.indexCity].properties[0]).length;
      if (size >= 1) {
        for (var i = 0; i < size; i++) {
          newProp[i] = this.initPropertyInstance(this.locationToEdit.module,
            this.locationToEdit.entityLocation.cities[this.indexCity].properties[0][i].language, 
            i);
        }
        return newProp;
      }
    }
    return this.initPropertyInstance(this.locationToEdit.module);
  }



  /**
   * Return a new City Form.
   * 
   * @param {*} [city=""] 
   * @param {*} [address=""] 
   * @param {*} [telephone=""] 
   * @param {*} [openingTime=""] 
   * @param {*} [latitude=""] 
   * @param {*} [longitude=""] 
   * @param {*} [published=""] 
   * @returns 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  initCity() : any {
    return new Promise( (resolve, reject) => {
        let newCity;
        let newOfficeProp;
        //If it is undefined it is because is a new Form.
         newCity = this.initModelInstance(this.locationToEdit.module);
         console.log(newCity.properties)

         newOfficeProp = this.addPropertyInstance(newCity.properties)
         newCity.properties.push(newOfficeProp);
         this.locationToEdit.entityLocation.cities[this.indexCity].properties.push(<any>newOfficeProp);
         resolve((<MainBasedProps>newCity));
    })
   
  }
  /**
   * Instantiate Entity based on Edition component.
   * 
   * @param {any} entityModule 
   * @returns 
   * @memberof ModuleEditingComponent
   */
  initModelInstance(entityModule){
      switch(entityModule){
        case 'CallCenter':
        return new CityModel()
      case 'OfficeLocation':
        return new CityOffices();
      case 'Campaign':
        return new CampaignBasedModel();
      }
    }


  /**
   * Instantiate properties depending of entityModule.
   * 
   * @param {any} entityModule 
   * @returns 
   * @memberof ModuleEditingCompoaddNewLanguagenent
   */
  initPropertyInstance(entityModule, lang="en", i=0):MainProperties {
    
    switch (entityModule) {
      case 'CallCenter':
        return new CityProperty(
          lang,
          (<CityProperty>this.locationToEdit.entityLocation.cities[this.indexCity].properties[0][i]).cityName)
      case 'OfficeLocation':
        return new OfficeProperties(lang, 
          (<CityProperty>this.locationToEdit.entityLocation.cities[this.indexCity].properties[0][i]).cityName);
      case 'Campaign':
        return new CampaignProperty(lang);
    }
  }

  /**
   * 
   * 
   * @param {any} value 
   * @returns 
   * @memberof ModuleEditingComponent
   */
  createForm(property) {
    let propertyForm = {};
    property.forEach(office => {
        propertyForm = this.createFormField(office);
    });
    return propertyForm;
   
  }

  createFormField(fieldValue){
    let newField={};
    let newForm = {};
    for (const name in fieldValue){
      newField = fieldValue[name]
      if (typeof newField != 'object') {
        newForm[name] = new FormControl(newField);
      } 
    }
    return newForm;
  }

  /**
   * Sumbitting new Office Location.
   * 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  async onSubmit() {
    console.log(this.locationToEdit.entityLocation)
    let valueToSend = await this.parseDataToSend(this.locationToEdit.entityLocation)
    this.formSubmitAttempt= true;
    this.genServices.create( valueToSend, this.locationToEdit.apiService).subscribe(
      response => {
        if (response.message.code != 501) {
          this.valueFromEdit.emit({
            value: this.locationToEdit.entityLocation
          })
        }
      },
      error => {
        this.loader.hide()
        this.alertService.errorArray([error.message.description])
        console.log(error)
      },
      () => {

      }
    )
  }

  parseDataToSend(entityLocation){
    return new Promise((resolve, reject)=>{
      switch(this.locationToEdit.module){
          case 'Campaign':
          let valueToSend = new CampaignEntity(entityLocation.countryName, entityLocation.countryCode, entityLocation.defaultCamp)
          entityLocation.cities.forEach(city => {
            city.properties.forEach((properties:Array<CampaignProperty>) => {
              let newCampaign = new CampaignBasedEntity();
              newCampaign.name= properties[0].campaignName
              newCampaign.properties = properties;
              newCampaign.properties.forEach(property => {
                property.fromSchedule = this.parseDateAndReturn(property.fromSchedule)
                property.toSchedule = this.parseDateAndReturn(property.toSchedule)
                property.modifiedDate = this.parseDateAndReturn(property.modifiedDate)
              })
              valueToSend.campaigns.push(newCampaign);
            });
          });
          
          resolve(valueToSend);
          break;
        default:
          resolve(entityLocation);
          break;
      }
    })

  }

  parseDateAndReturn(date){
    if(typeof date == 'object'){
        return date.toJSON().slice(0,10).replace(/-/g,'/');
    }
    return date;
  }

  changePropertyValue(fieldValue, indexLang, propKey){
    this.locationToEdit.entityLocation.cities[this.indexCity].city=fieldValue;
    this.locationToEdit.entityLocation.cities[this.indexCity].properties.forEach((property:Array<CityProperty>) =>{
      for (const key in property[indexLang]) {
          if(key === propKey){
            property[indexLang][key] = fieldValue
          }
        }
    })
  }

  /**
   * ADD NEW OFFICE LOCATION BTN.
   * Set Langage to Copy and new Language availables in Select Input.
   * 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  setExistingLanguages() {
    this.existingLanguage = this.locationToEdit.entityLocation.cities[this.indexCity].properties[0]
      .map(property => { return property.language })
    this.languageToCopy = 'Copy Content From';
    this.newLanguageCopied = 'Select New Language';
  }


  /**
   * Text area Customize
   * 
   * @param {*} $event 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  onKey($event: any) {
    $event.srcElement.style.height = "34px"
    $event.srcElement.style.marginTop = "";

    if ($event.srcElement.scrollHeight >= 52) {
      $event.srcElement.style.height = "62px"
    }
    if ($event.srcElement.scrollHeight >= 72) {
      $event.srcElement.style.height = "82px"
    }
  }


  /**
   * Adding Language translation
   * 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  addNewLanguage() {
    // this.langSelectionSetting =true;
    console.log(this.locationToEdit.entityLocation.cities)
    this.locationToEdit.entityLocation.cities[this.indexCity].properties.forEach((location:any) =>{
        let newProperty = location.find(property => property.language == this.languageToCopy)
        
        switch (this.locationToEdit.module) {
          case 'OfficeLocation':
            location.push(new OfficeProperties(
              this.newLanguageCopied,
              (<OfficeProperties>newProperty).cityName,
              (<OfficeProperties>newProperty).openingTime,
              (<OfficeProperties>newProperty).telephone,
              (<OfficeProperties>newProperty).address,
              (<OfficeProperties>newProperty).latitude,
              (<OfficeProperties>newProperty).longitude));;
            break;
          case 'CallCenter':
          location.push(new CityProperty(
              this.newLanguageCopied,
              (<CityProperty>newProperty).cityName,
              (<CityProperty>newProperty).openingTime,
              (<CityProperty>newProperty).telephone));
            break;
          case 'Campaign':
            location.push(new CampaignProperty(
            this.newLanguageCopied,
            (<CampaignProperty>newProperty).campaignName,
            (<CampaignProperty>newProperty).fileName,
            (<CampaignProperty>newProperty).link));
            break;
  
          default:
            break;
        }
  
      })
    this.langSelectionSetting = false;
  }



  /**
   * This will enable/disable submit button.
   * 
   * @returns 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  getCityFormValidation() {
    let isInvalid = false;
    let validatingArray = new Array<Boolean>();
    this.locationToEdit.entityLocation.cities.forEach(location => {
      location.properties.forEach((property) => {
        property.forEach(element => {
          validatingArray.push(this.validateByModule(element))
        });
      })
    })
    return validatingArray.indexOf(isInvalid) > -1;
  }

  validateByModule(property){
    switch(this.locationToEdit.module){
      case 'CallCenter':
        return property.cityName != '';
      case 'OfficeLocation':
        return property.address != '';
      case 'Campaign':
        return property.name != '';
    }
  }

  /**
   * Change Published value when status is selected.
   * 
   * @param {any} event 
   * @param {any} index 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  changeStatusValue(event, index, langIndex) {
    if (event.srcElement.value == 'true') {
      (<CityModel>this.locationToEdit.entityLocation.cities[this.indexCity]).published = true;
      return
    }
    (<CityModel>this.locationToEdit.entityLocation.cities[this.indexCity]).published = false;
  }


  /**
   *Set @param langSelectionSetting to true in order to toggle to  
   * a Modal to select new language.
   * 
   * @memberOf OfficeLocationEditComponent
   */
  toggleLangSelection() {
    if (this.langSelectionSetting) {
      this.langSelectionSetting = false
    } else {
      this.langSelectionSetting = true;
    }
    this.availableSelectionLanguage = this.languagesArray.filter((lang) => {
      return this.existingLanguage.indexOf(lang) < 0;
    })
  }


  /**
   * This will enable/disable Bulk Selection.
   * 
   * @returns 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  validSettingForm() {
    if (this.languageToCopy != 'Copy Content From' &&
      this.newLanguageCopied != 'Select New Language') {
      return false;
    }
    return true;
  }

  /**
   * Remove City Form.
   * 
   * @param {any} indexCity 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  removeCity(indexCity, indexProperty) {
    this.locationToEdit.entityLocation.cities[indexCity].properties.splice(indexProperty, 1);
    let countForm = (<any>this.myForm.controls['arrayCityForm']);
    countForm.removeAt(indexProperty);
  }



  //GOOGLE MAP MODAL.

  /**
   * Once select new location button is clicked, this will set
   * @param this.locationCityToEdit in order to see it on googlemap
   * component
   * 
   * @param {CityProperty} city 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  selectCityLocation(city: OfficeProperties) {
    console.log(city);
    this.cityToEdit = city;
    this.locationCityToEdit = {
      latitude: 0,
      longitude: 0,
      newLoc: false
    }
    this.locationCityToEdit.newLoc = true;

    this.locationCityToEdit.latitude = +city.latitude;
    this.locationCityToEdit.longitude = +city.longitude;

  }


  /**
   * Once location is selected, button saved will 
   * save the locations on @param this.cityToEdit.
   * 
   * 
   * @memberOf OfficeLocationEditComponent
   */
  saveLocation(): void {
    this.toggleLocationModal = true;
    this.cityToEdit.latitude = this.locationCityToEdit.latitude;
    this.cityToEdit.longitude = this.locationCityToEdit.longitude;
  }


  /**
   * input languageLanguage
   * 
   * @param {any} arr 
   * 
   * @memberOf OfficeLocationComponent
   */
  inputLanguage(lang) {
    if (lang == 'ar') {
      return 'rtl';
    } else {
      return 'ltr'
    }
  }

  /**
   * Check if value is Empty
   * 
   * @memberof ModuleEditingComponent
   */
  hasValue(value) {
    return (value !== undefined && value !== '' && value !==  null)
  }

    /**
   * Parse imgSource.
   *
   * @param {string} imageName
   * @returns
   *
   * @memberOf CampaignEditComponent
   */
  getUrlImage(imageName: string): string {
    return this.rootDir + this.apiRest.API_FILES +"/"+ imageName;
  }


  /**
   * Upload new Image to server;
   * 
   * @param {any} event 
   * 
   * @memberOf CampaignEditComponent
   */
  imageUpload(event) {
    this.newImageSelected = true;
    let reader = new FileReader();
    this.selectedUploadFile = event.target.files[0];

    //get the selected file from event
    if (this.selectedUploadFile.type.match('image.*')) {
      reader.readAsDataURL(this.selectedUploadFile);
    }
  }


    /**
   * Uploading a new Image.
   * 
   * 
   * @memberOf CampaignEditComponent
   */
  submitFile(value) {

    let formData = new FormData();
    formData.append(this.selectedUploadFile.name, this.selectedUploadFile);
    this.imageUrl = "";
    this.genServices.create(formData,this.apiRest.API_FILES).subscribe(
      response => {
        if (response.message.code == 200) {
          this.files.unshift({ filename: this.selectedUploadFile.name });
          this.newImageSelected = false;
          this.paginationList.entityList = this.files;
          this.changePagination.setPage(1);
        }
      },
      error => {
        this.alertService.errorArray(error);

      },
      () => {
        
      })
  }

  setIndexForm(indexForm){
    if(!this.filesLoaded){
      this.initFiles();
      this.filesLoaded = true;
    }
    this.indexForm = indexForm;
  }

  /**
   * Once the image is selected it will save it on @param selectedFile
   * 
   * @param {any} fileName 
   * 
   * @memberOf CampaignEditComponent
   */
  selectImageFromList(fileName) {
    (<CampaignProperty>this.locationToEdit.entityLocation.cities[this.indexCity].properties[this.indexForm][this.languageSelected]).fileName = fileName
    console.log(this.locationToEdit.entityLocation.cities)
  }


    /**
   * When SaveChanges button is clicked it will save it con FormControl.Image.
   * 
   * 
   * @memberOf CampaignEditComponent
   */
  saveImageChanges() {
    let form = (<any>this.myForm.controls['arrayCityForm']).controls[this.indexForm];
    form.controls.fileName.setValue(this.selectedFile);
    console.log(form)
    console.log(this.selectedFile)
    // form.controls.newImage.setValue(this.selectedFile);

  }
}
