const constants = require('./constants');
const replyHandler = require('../reply/replyHandler');

const httpsPostCall = function (payload, url, reply) {
    const request = require('request').defaults({'proxy': constants.HTTPS_PROXY});
    const body = payload.dataModel;
    let responseData = '';

    const options = {
        method: 'post',
        json: true,
        body: body,
        url: url
    };
    console.log(url);
    let response;
    request(options, function (err, res, body) {
        console.log("");
    }).on('data', function (chunk) {
        responseData += chunk;
    }).on('response', function (resp) {
        response = resp;
    }).on('error', function (error) {
        console.log("")
    }).on('end', function (someData) {
        console.log("response: " + response);
        const data = JSON.parse(responseData);
        replyHandler.handleOk(
            {
                hash: null,
                data: data
            },
            reply
        );
    });
};




module.exports = {
    httpsPostCall: httpsPostCall
};
