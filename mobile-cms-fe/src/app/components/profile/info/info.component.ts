import { Component, OnInit } from '@angular/core';
import {FormControl, FormBuilder} from '@angular/forms';
import { User } from '../../../models';
import { AuthenticationService, AlertService } from '../../../services';
@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  public userForm;
  public formSubmitAttempt:boolean;
  public editMode:boolean;
  public user: User;

  constructor(private fb: FormBuilder, private authService: AuthenticationService,
    private alertService: AlertService) {
    this.editMode=false;
    this.formSubmitAttempt=false
    let currentUser =JSON.parse(localStorage.getItem("currentUser"));
    console.log(currentUser)
    this.user = new User(currentUser._id,
      currentUser.username, 
      currentUser.firstname, 
      currentUser.lastname,
      currentUser.role);
   }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    let user = JSON.parse(localStorage.getItem("currentUser"));
    this.userForm = this.fb.group({
      firstname: new FormControl(this.user.firstname),
      lastname: new FormControl(this.user.lastname),
      role: new FormControl({ value: this.user.role, disabled: true}),
    })
  }

  onSubmit(value){

      this.authService.updateUserDetails({username: this.user.username, ...value}).subscribe(
        response=>{
          this.user = response.data;
          localStorage.setItem('currentUser',JSON.stringify(response.data));
          this.alertService.success(["Password changed succesfully"]);
        },
        error =>{
          this.alertService.errorArray([error.message.description]);
        })
  }

}
