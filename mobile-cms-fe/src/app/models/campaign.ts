import { MainProperties, MainEntity, MainBasedProps } from "./MainEntity";

export  class Campaign extends MainEntity{
    public cities:CampaignBasedModel[];
    public defaultCamp: boolean;
    constructor(countryName = '', countryCode= '',defaultCamp=false, cities= new Array<CampaignBasedModel>() ){
        super(countryName,countryCode);
        this.cities=cities;
        this.defaultCamp = defaultCamp;
    }
}
export class CampaignBasedModel extends MainBasedProps{
    public name:string;
    constructor(
        name?,
        properties=new Array<Array<CampaignProperty>>()
        ){
        super(properties);
        this.name=name;
    }
}

export class CampaignProperty extends MainProperties{
    public campaignName:string;
    public fileName : any;
    public link: any;
    public fromSchedule: any;
    public toSchedule:any;
    public modifiedDate: any;
    constructor(
        language="", 
        campaignName="",
        fileName="", 
        link="",
        fromSchedule:any= new Date(), 
        toSchedule:any= new Date(),
        date= new Date()){
        super(language);
        this.campaignName=campaignName;
        this.fileName = fileName;
        this.link = link;
        this.fromSchedule = this.setDate(fromSchedule);
        this.toSchedule = this.setDate(toSchedule);
        this.modifiedDate = this.setDate(date);
    };

    setDate(date){
        if(typeof date == 'object'){
            return date.toJSON().slice(0,10).replace(/-/g,'/');
        }
        return date;
    }
  
}

export  class CampaignEntity extends MainEntity{
    public campaigns:CampaignBasedEntity[];
    public defaultCamp:boolean;
    constructor(countryName = '', countryCode= '', defaultCamp = false, campaigns= new Array<CampaignBasedEntity>()){
        super(countryName,countryCode);
        this.campaigns=campaigns;
        this.defaultCamp= defaultCamp;
    }
}

export class CampaignBasedEntity{
    public name: string;
    public properties:Array<CampaignProperty>;
    constructor(
        name="",
        properties=new Array<CampaignProperty>(), 
        ){
        this.properties =properties;
        this.name = name;
    }
} 

