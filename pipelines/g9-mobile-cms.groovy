stage 'Checkout'

def userInput = input(
 id: 'userInput', message: 'Please input Tag/Branch', parameters: [
 [$class: 'TextParameterDefinition', defaultValue: '', description: 'Environment', name: 'Tag/Branch : ']
])

node('master') {
    checkout scm: [$class: 'GitSCM', userRemoteConfigs: [[url: 'ssh://git@bitbucket.accelaero.cloud/mob/mobile-pss-suite.git']], branches: [[name: userInput]]],poll: false
}


stage 'Build and Docker'
node('master') {
    dir('mobile-cms') {
        sh 'docker build -t aeromart/mobile-cms-airarabia:1.0.0 --build-arg CARRIER_CODE=g9 --build-arg HTTP_PROXY=http://10.30.100.19:8080 --build-arg HTTPS_PROXY=http://10.30.100.19:8080 .'

        sh 'docker login --username=admin --password=admin docker-registry.isaaviations.com:443'
		docker.withRegistry('https://docker-registry.isaaviations.com:443') {
		    docker.image('aeromart/mobile-cms-airarabia:1.0.0').push('1.0.0.${BUILD_NUMBER}')
		}
		sh 'docker rmi $(docker images | grep \'mobile-cms-airarabia\' | awk \'{print $3}\') -f || echo "no images to remove"'
    }
}


stage 'Deploy '
node('mobilecms-uat') {
    docker.withRegistry('https://docker-registry.isaaviations.com:443') {

        sh 'docker stop mobile-cms-airarabia || echo "no container to stop"'
        sh 'docker rm mobile-cms-airarabia || echo "no container to remove"'
        sh 'docker rmi $(docker images | grep \'mobile-cms-airarabia\' | awk \'{print $3}\') -f || echo "no images to remove"'
        sh 'docker login --username=admin --password=admin docker-registry.isaaviations.com:443'
        docker.image('aeromart/mobile-cms-airarabia:1.0.0.${BUILD_NUMBER}').pull()
        sh 'docker run --restart=always --name mobile-cms-airarabia -p 3160:3160 -d -e API_CMS_CONTEXT=\'{"CLIENT":"g9","CLIENT_URL":"https://g916.isaaviations.com/service-app/controller/","SERVER_PORT":3160,"SERVER_URL_PREFIX":"mobile-engine","HAS_PROXY":false,"HTTPS_PROXY":"","MONGO_DB_ADDRESS":"10.30.26.52","MONGO_DB_CREDENTIALS":"","MONGO_DB_PORT":27017,"MONGO_DB_NAME":"cms","MONGO_DB_AUTHDB":"admin", "CMS_INSTANCE":"http://mobilecms.isaaviations.com","PRIVATE_KEY": "isaPrivateKey","USERS":{ "admin" :"$2a$04$BgNfIzQP1fBYsPlAzDPTwuMMFSMAUAYYJNPg7o.b7rPTK0s5IJBUK"}}\' -d docker-registry.isaaviations.com:443/aeromart/mobile-cms-airarabia:1.0.0.${BUILD_NUMBER}'
    }
}