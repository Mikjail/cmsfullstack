const Joi = require('joi');
const jwt = require('jsonwebtoken');
const MongoClient = require('mongodb').MongoClient;
const {PRIVATE_KEY} = require('../constants');
const { MongoDataBase } = require('../mongodb/mongoCommonQuery');
const replyMssgs = require ('../reply/replyMssgs')
const base64 = require('base-64');
const bcrypt = require('bcryptjs');
const CriteriaBuilder = require('../utils/criteriaBuilder');
const replyHandler = require('../reply/replyHandler');

const requestBasicAuthSchema = {
    headers: Joi.object().keys({
        authorization: Joi.string().required()
    })
};

class LoginController{
    constructor(){
    }

    signup(request,reply){
        
    }

    async login(request,reply, dbName){
        if(this.hasCredentials(request)){
            const MongoDB = new MongoDataBase();
            const credentials = request.headers.authorization ? base64.decode(request.headers.authorization.split(" ")[1]) : null;
            const username = credentials.split(":")[0];
            const password = credentials.split(":")[1];
          
            try{  
                const criteriaBuilder = new CriteriaBuilder();
                const limit = request.url.query.limit ? parseInt(request.url.query.limit) : Number.MAX_SAFE_INTEGER;
                await MongoDB.connect();

                criteriaBuilder.filters(username, dbName);
                
                const user = await MongoDB.findQuery(criteriaBuilder.build(), dbName, limit)

                const credentialsMatch = !password ? false : bcrypt.compareSync(password, user[0].password);
                let token=""
                // replyHandler.handleAuth(request, reply);
 
                if(credentialsMatch){
                    token = jwt.sign({ accountId: username }, PRIVATE_KEY, { algorithm: 'HS256'} );
                }
                
                delete user[0].password;

                reply(replyMssgs.replyAuth(null, credentialsMatch, token, user));

            }catch(err){
                reply(replyMssgs.replyAuth(err,false, null));
            }
        }   
    }

    async changePass(request, reply, dbName){
        if(this.hasCredentials(request)){
            const MongoDB = new MongoDataBase();
            const credentials = request.payload ? base64.decode(request.payload) : null;
            const username = credentials.split(":")[0].trim();
            const password = credentials.split(":")[1].trim();
            const newPassword = credentials.split(":")[2].trim();
            try{  
                let criteriaBuilder = new CriteriaBuilder();
                const limit = request.url.query.limit ? parseInt(request.url.query.limit) : Number.MAX_SAFE_INTEGER;
                await MongoDB.connect();

                criteriaBuilder.filters({username}, dbName);

                const user = await MongoDB.findQuery(criteriaBuilder.build(), dbName, limit)

                const credentialsMatch = !password ? false : bcrypt.compareSync(password, user[0].password);
                
                let token=""

                if(credentialsMatch){

                    const newHashPassword = await this.hashPassword(newPassword)
                    
                    criteriaBuilder.filters({username, newHashPassword}, dbName);
          
                    const user = await MongoDB.addUpdateQuery(criteriaBuilder.build(), dbName, {username})
            
                    token = jwt.sign({ accountId: username }, PRIVATE_KEY, { algorithm: 'HS256'} );
                }
                reply(replyMssgs.replyAuth(null, credentialsMatch, token));

            }
            catch(error){
                reply(replyMssgs.replyAuth(error,false, null));
            }
        }
    }

    async updateUserDetails(request, reply, dbName){
        if(this.hasCredentials(request)){
            const MongoDB = new MongoDataBase();
            const { username, firstname, lastname} = request.payload;
            
            try{  
                let criteriaBuilder = new CriteriaBuilder();

                const limit = request.url.query.limit ? parseInt(request.url.query.limit) : Number.MAX_SAFE_INTEGER;
               
                await MongoDB.connect();

                criteriaBuilder.filters({username}, dbName);

                let user = await MongoDB.findQuery(criteriaBuilder.build(), dbName, limit)

                user[0].firstname = firstname;
                user[0].lastname = lastname;

                user = await MongoDB.addUpdateQuery(user[0], dbName, criteriaBuilder.build())
                    
                
                reply(replyMssgs.replyAuth(null, true, "No Token", user));
            }
            catch(error){
                reply(replyMssgs.replyAuth(error,false, null));
            }
        }
    }

    hasCredentials(request) {
        const authCredErrors = Joi.validate(request, requestBasicAuthSchema, {stripUnknown: true}).error;
        
        request.authStatus = { 
            isAccessGranted: authCredErrors==null || false, 
            message: {
                code: !authCredErrors? 200 :  400, 
                title:  !authCredErrors? "success" : 'error',
                description: !authCredErrors? "": 'Empty credentials' 
            }
        }

        return request.authStatus.isAccessGranted;
       
    };

  
    loginAuth (request,reply, data){
        if (request.authStatus.isAccessGranted) {
            reply({
                code: 200, 
                message: request.authStatus.message,
                data: data ? data:'', 
                token: request.authStatus.token
            });
        } else {
            reply({
                code: request.authStatus.code || 400, 
                message: request.authStatus.message || "error"
            })
        }
    };

    async hashPassword(password){
        const saltRounds = 10
        try{
            const salt = await bcrypt.genSalt(saltRounds); 
            const hashpass = await  bcrypt.hash(password, salt);
    
            return hashpass;
    
        }
        catch(error){

        }

    }


}

module.exports = {
    LoginController
}