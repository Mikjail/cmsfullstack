import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Constants, 
  API_REST, 
  CallCenter, 
  CampaignBasedModel,
  CampaignBasedEntity,
  CityProperty,
  Campaign,
  CampaignProperty,
  CampaignEntity} from '../../../models';
import { ModuleListingComponent} from '../countryLangBased/module-listing';
import { GenericService } from '../../../services';
import { SearchFilterComponent} from '../../utils/search-filter';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {
  @Input('searchInput') searchFilter = { 
    apiRest:'', 
    availableSearch:{
      searchStatus: false,
      searchName:false,
      searchCountry: true,
      searchLanguage: true
    },
    countryFromServer:[],
    languageArray:[]
   };

  /**
   * Entity directive.
   * 
   * @type {ModuleListingComponent}
   * @memberof OfficeLocationComponent
   */
  @ViewChild(ModuleListingComponent) ListingComponent: ModuleListingComponent;
  /**
  * Select an Office and set @param this.campaignToEdit to edit it.
  * 
  * @param {any} officeLocation 
  * 
  * @memberOf OfficeLocationComponent
  */
  editLocation({newLocationToEdit, cityIndex}): void {
    console.log("va a editar")
    this.campaignToEdit.edition = true;
    this.campaignToEdit.cityIndexSelected=cityIndex;
    this.campaignToEdit.entityLocation = newLocationToEdit;
    this.campaignToEdit.apiService = this.apiRest.API_CAMPAIGN;
    this.campaignToEdit.module = 'Campaign';
  }

  @ViewChild(SearchFilterComponent) searchFilterComponent: SearchFilterComponent;
  getFromSearch(campaignFound){
    this.campaignToEdit.edition = false;
    if(campaignFound.value.length > 0){
      this.campaigns = campaignFound.value.sort(this.compareByCountry);
      this.ListingComponent.refreshLocations(this.campaigns);
      this.entityLocation.resultsFound = true;
    }else{
      this.entityLocation.resultsFound = false;
    }

  } 
   /**
   * Filter Form.
   * 
   * @type {Array<string>}
   * @memberOf CallcenterComponent
   */
  public resultsFound = false;

  /**
   * Table list.
   * 
   * @param this.campaigns publish checkBox value.
   * @memberOf CallcenterComponent
   */
  public campaigns;
  
  /**
   * ROOT URL HOST
   * 
   * @type {string}
   * @memberOf CallcenterComponent
   */
  public apiRest: API_REST;

  /**
  * To enable Campaign edit directive.
   * 
   * @memberof CallcenterComponent
   */
  public campaignToEdit = { 
    edition: false, 
    cityIndexSelected:0,
    entityLocation: new Campaign(),
    module: 'Campaign',
    apiService: '' 
  };

     /**
   * Module Listing/ Panel Creation Directive 
   * 
   * @memberof OfficeLocationComponent
   */
  public entityLocation = { 
    module: 'Campaign',
    locationList: [], 
    locationParsed: [],
    apiService:'',
    apiServiceMulti: '', 
    resultsFound:false,
    editEntity: false ,
    countrySelectionSetting: false
  };

  /**
   * Pagination Directive
   * 
   * @private
   * @type {any[]}
   * @memberOf CallcenterComponent
   */
  public paginationList = {
    entityList: [],
    pagedItems: [],
    pager: {},
    setPage: {},
    customNumberPages: 8
  };


  /**
   * Edit Data Table.
   * 
   * 
   * @memberOf CallcenterComponent
   */
  public countrySelectionSetting;
  public existingCountLang=[];
  public officeLocations;
  public existingCountries;
  public buttonNewCountry = "Settings"


  constructor( 
    private genServices: GenericService,
    private constants: Constants) {
    this.apiRest = constants.IApiRest;
    this.existingCountries = [];
    this.searchFilter.apiRest= this.apiRest.API_CAMPAIGN;
    this.entityLocation.apiService= this.apiRest.API_CAMPAIGN;
    this.campaignToEdit.apiService = this.apiRest.API_CAMPAIGN;
    this.campaigns=[];
    this.entityLocation.countrySelectionSetting = false;
  
  };
   

  ngOnInit() {
  }


   /**
   * Helper to sort an Array of Campaigns 
   * 
   * @param {CallCenter} countryA 
   * @param {CallCenter} countryB 
   * @returns {number} 
   * 
   * @memberOf CampaignListComponent
   */
  compareByCountry(countryA:CallCenter,countryB:CallCenter):number{
    if(countryA.countryName < countryB.countryName){
      return -1
    }
    if(countryA.countryName > countryB.countryName){
      return 1
    }
    return 0;
  }
/**
 * pARSE VALUES BEFORE SEND IT TO EDIT TEMPLATE
 * 
 * @param {any} {newLocationToEdit , cityIndex} 
 * @memberof CampaignComponent
 */
editCampaign({newLocationToEdit }){
    this.editLocation({newLocationToEdit, cityIndex: 0});
  }
  /**
   * Add a new country with all it's default properties.
   * 
   * 
   * @memberOf OfficeLocationComponent
   */
  addNewCountry({newCountryToEdit, newLanguageToEdit, newLocationToEdit,  newTextToEdit }): void {
   
    let countrySelected = this.searchFilter.countryFromServer.find(country => country.countryName == newCountryToEdit);
    let cityIndex;
    if(!newLocationToEdit){
      newLocationToEdit = new Campaign(countrySelected.countryName, countrySelected.countryCode);
      newLocationToEdit = this.addNewProperty(newLocationToEdit, newLanguageToEdit, newTextToEdit);
      cityIndex= 0;
    }else{
      newLocationToEdit = this.entityLocation.locationList.find(location => location.countryName == newLocationToEdit.countryName);
      newLocationToEdit = this.addNewProperty(newLocationToEdit, newLanguageToEdit, newTextToEdit);
      cityIndex= newLocationToEdit.cities.length -1;
    }
    this.editLocation({newLocationToEdit, cityIndex})
    this.entityLocation.countrySelectionSetting = false;
}

  addNewProperty(newLocationToEdit, newLanguageToEdit, newTextToEdit){
    newLocationToEdit.cities.push(new CampaignBasedModel(newTextToEdit))
      let cityIndex = newLocationToEdit.cities.length-1;
      let properties = []
      let newCampaign = 
      properties[0] = new CampaignProperty(newLanguageToEdit, newTextToEdit);
      newLocationToEdit.cities[cityIndex].properties[0] = properties;
      return newLocationToEdit
  }


  parseDataCamp(entityLocation){
    return new Promise((resolve, reject)=>{
      let valueToSend = new CampaignEntity(entityLocation.countryName, entityLocation.countryCode, entityLocation.defaultCamp)
      entityLocation.cities.forEach(city => {
        city.properties.forEach((properties:Array<CampaignProperty>) => {
          let newCampaign = new CampaignBasedEntity();
          newCampaign.name= properties[0].campaignName
          newCampaign.properties = properties;
          valueToSend.campaigns.push(newCampaign);
        });
      });
      this.entityLocation.locationParsed = <any>valueToSend;
      resolve(valueToSend)
    })
  }

}
