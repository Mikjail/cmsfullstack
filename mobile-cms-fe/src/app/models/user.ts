﻿export class User {
    public id: string;
    public username: string;
    public role:string;
    public firstname: string;
    public lastname: string;
    constructor(id="", 
        username='',
        firstname='',
        lastname="",
        role="", 
       ){
        this.id= id;
        this.username= username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.role=role;
    }
}