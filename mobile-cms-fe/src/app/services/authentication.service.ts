import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Constants } from '../models/index';
import { LoaderService } from './loader.service';
import { Subject } from 'rxjs/Subject';
import { Router, CanActivate } from '@angular/router'
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    private url;
    private isAuthenticated=false;
    private changeUser= false;
    authChange = new Subject<boolean>();

    constructor(private http: Http, 
        cs:Constants,
        private loader: LoaderService,
        private router: Router) {
        this.url = cs.root_dir;
     }

    login(user) {
        this.loader.show();
        let headers: Headers = new Headers();
        headers.append("Authorization", "Basic " + btoa(user.username + ":" + user.password)); 
        return this.http.post(this.url + '/auth/login',"", { headers: headers, withCredentials: true}).map(response  => this.authentifySession(response.json())); 
    }

    logout() {
        this.loader.show();
        // remove user from local storage to log user out
        return this.http.post(this.url + '/auth/logout', {withCredentials: true}).map((response :any) => { return this.authentifySession(response.json())}); 
    }

    updatePass(user){
        this.changeUser= true;
        this.loader.show();
        const headers: Headers = new Headers();
        headers.append("Authorization", localStorage.getItem('token')); 
        const valueToSend =  btoa(`${user.username}: ${user.password} : ${user.newPassword}`);
        return this.http.post(this.url + '/auth/changePass',valueToSend ,{headers: headers, withCredentials: true}).map((response :any) => { return this.authentifySession(response.json())}); 
    }

    updateUserDetails(user){
        this.changeUser= true;
        this.loader.show();
        const headers: Headers = new Headers();
        headers.append("Authorization", localStorage.getItem('token'));
        return this.http.post(this.url + '/auth/updateUser',user ,{headers: headers, withCredentials: true}).map((response :any) => { return this.authentifySession(response.json())}); 
    }
    
    authentifySession(response){
        this.loader.hide();
        if(!response.token){
            this.isAuthenticated =false;
            if(!this.changeUser) this.authChange.next(false)  
            this.changeUser =false    
            throw response; 
        }
        else{
            if(!this.changeUser) localStorage.setItem('token', response.token);
            this.isAuthenticated =true;
            this.authChange.next(true)
            return response;
        }
    }

    isAuth(){
        
        if(this.hasValue(localStorage.getItem('token')) && 
        this.hasValue(localStorage.getItem('currentUser'))){
            this.authChange.next(true)
            return true;
        }
        else{
            localStorage.removeItem('token')
            localStorage.removeItem('currentUser')
            this.authChange.next(false) 
            return false; 
        }
    }

    hasValue(value){
        return value !== undefined && value !== '' && value !== null;
    }

}