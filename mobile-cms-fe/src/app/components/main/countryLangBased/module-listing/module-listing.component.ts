import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { GenericService } from '../../../../services';
import {
  Constants,
  MainEntity,
  OfficeLocation,
  CampaignBasedModel,
  Campaign
} from '../../../../models';
import { PaginationComponent } from '../../../utils';
import { Entity, CityModel } from '../../../../models/Entity';


@Component({
  selector: 'app-module-listing',
  templateUrl: './module-listing.component.html',
  styleUrls: ['./module-listing.component.scss']
})
export class ModuleListingComponent implements OnInit {
  @Output('valueToEdit') valueToEdit = new EventEmitter();
  @Output('parseDataCamp') parseDataCamp = new EventEmitter();
  @Output('existingCountries') existingCountries = new EventEmitter();
  @Input('locationInput') entityLocation = {
    module: '',
    locationList: [],
    locationParsed: [],
    apiService: '',
    apiServiceMulti: '',
    resultsFound: false,
    editEntity: false,
    countrySelectionSetting: false
  };

  /**
     * Pagination directive.
     * 
     * @type {PaginationComponent}
     * @memberof OfficeLocationComponent
     */
  @ViewChild(PaginationComponent) changePagination: PaginationComponent;


  /**
 * PAGINATION
 * 
 * @private
 * @type {any[]}
 * @memberOf CampaignListComponent
 */
  public paginationList = {
    entityList: [],
    pagedItems: [],
    pager: {},
    setPage: {},
    customNumberPages: 8
  };


  /**
 * Edit data table. 
 * 
 * 
 * @memberOf OfficeLocationComponent
 */

  public entities;
  public entity;
  public entityToEdit;
  public entityToDelete;
  public toggleDelete = false;
  public entityToPublish;
  public bulkAction;
  public languageToDelete;
  public countryToDelete
  public toggleDeleteLanguage = false;
  public indexCityToDelete;
  public defaultCamp = false;
  public orderASC= false;
  public orderDESC = false;
  public editPriority = false;
  public propertySelected = []
  public entityToPrior;
  constructor(private genServices: GenericService) { }

  ngOnInit() {
    this.entityToPublish = new Array();
    this.initLocations();
  }

  /**
   * Get All Office Locations.
   * 
   * 
   * @memberOf OfficeLocationComponent
   */
  initLocations(): void {
    this.genServices.get(this.entityLocation.apiService).subscribe(
      response => {
        // console.log(data);
        if (response.data.length > 0) {
          this.entityLocation.resultsFound = true;
          // this.officeLocations = response.message;
          let newOfficeLocations = this.parseLocations(response.data).then((newOfficeLocations) => {
            // this.officeLocations = newOfficeLocations.reverse();
            this.entityLocation.locationList = newOfficeLocations.reverse();
            // console.log(newOfficeLocations);
            this.paginationList.entityList = this.entityLocation.locationList;
            if (this.changePagination != undefined) {
              this.changePagination.setPage(1);
            }
            this.entityLocation.resultsFound = true;
            this.setEntitySatusList(this.entityLocation.locationList.length);
            this.existingCountries.emit();
          })
        } else {
          this.entityLocation.resultsFound = false;
        }

      },
      error => {

      });
  }
  /**
  * Instantiate properties depending of entityModule.
  * 
  * @param {any} entityModule 
  * @returns 
  * @memberof ModuleEditingCompoaddNewLanguagenent
  */
  initMainInstance(element) {
    if (this.entityLocation.module === 'Campaign') {
      let newEntity = new Campaign(element.countryName, element.countryCode, element.defaultCamp);
      if(!this.defaultCamp &&  element.defaultCamp) this.defaultCamp = element.defaultCamp;
      element.campaigns.forEach(entityCampaign => {
        let properties = []
        properties[0] = entityCampaign.properties;
        newEntity.cities.push(new CampaignBasedModel(entityCampaign.name, properties));
      });
      // console.log(newEntity)
      return newEntity;
    } 

    return new Entity(element.countryName, element.countryCode, element.cities);
  }


  /**
    * Will create/update an office location status 
    * 
    * @param {any} officeLocation 
    * 
    * @memberOf OfficeLocationComponent
    */
  onSubmit(entityLocation): void {
    this.genServices.create(entityLocation, this.entityLocation.apiService).subscribe(
      response => {
        // console.log(response);
          this.initLocations();
        // this.refreshLocations(response.data)
      },
      error => {

      },
      () => {
        // 
      }
    )
  }

  /**
   * Will delete an specific country with all it's locations.
   * 
   * @param {any} officeLocation 
   * 
   * @memberOf ModuleListingComponent
   */
  onDelete(entityLocation): void {
    this.genServices.delete(entityLocation.countryCode, this.entityLocation.apiService).subscribe(
      response => {
        this.initLocations();
      },
      error => {

      },
      () => {
        // EVENT EMITER TO SEARCH FILTER
        // this.searchFilterComponent.onSearch('');
      })
  }

  /**
   * Will Submit/Replace more than one location. 
   * 
   * @param {any} [officeLocations=Array<OfficeLocation>()] 
   * 
   * @memberOf OfficeLocationComponent
   */
  onSubmitMultiLocations(locationList = new Array<Entity>()): void {
    this.genServices.create(locationList, this.entityLocation.apiServiceMulti).subscribe(
      response => {
        console.log(response);
      },
      error => {

      },
      () => {
        // EVENT EMITER TO SEARCH FILTER
        // this.searchFilterComponent.onSearch('');
      }
    )
  }

  firstDefault(){
    this.entityLocation.locationList.sort((campA, campB) => { return  campA.defaultCamp ? -1: campB.defaultCamp ? 1: 0;});
    this.paginationList.entityList = this.entityLocation.locationList;
    if (this.changePagination != undefined) {
      this.changePagination.setPage(1);
    }
  }

  sortDatabyCountry(){
    this.orderASC = !this.orderASC;
    this.entityLocation.locationList.sort(this.compareByCountry.bind(this))
    this.paginationList.entityList = this.entityLocation.locationList;
    if (this.changePagination != undefined) {
      this.changePagination.setPage(1);
    }
  }
/**
  * Helper to sort an Array by Country 
  * 
  * @param {MainEntity} countryA 
  * @param {MainEntity} countryB 
  * @returns {number} 
  * 
  * @memberOf ModuleListingComponent
  */
 compareByCountry(countryA: MainEntity, countryB: MainEntity): number {
  if (countryA.countryName < countryB.countryName) {
   return this.orderASC ? -1 : 1;
  }
  if (countryA.countryName > countryB.countryName) { 
   return this.orderASC ? 1 : -1;
  }
  return 0;
}


  /**
   * Set entity status checkbox to false (unselected)
   * 
   * @param {any} sizeList 
   * 
   * @memberOf ModuleListingComponent
   */
  setEntitySatusList(sizeList): void {
    for (var i = 0; i < sizeList; i++) {
      this.entityToPublish.push(false);
    }
  }

  /**
   * 
   * 
   * @param {any} entityLocations 
   * @memberof ModuleListingComponent
   */
  async refreshLocations(locationList) {
    this.indexCityToDelete=0
    let newLocationList = await this.parseLocations(locationList);
      this.entityLocation.locationList = newLocationList;
      this.paginationList.entityList = newLocationList;
      if (this.changePagination != undefined) {
        this.changePagination.setPage(1);
      }
  }
  /**
   * 
   * 
   * @param {any} locationFromDB 
   * @returns {Promise<Array<OfficeLocation>>} 
   * @memberof ModuleListingComponent
   */
  parseLocations(locationFromDB): Promise<Array<MainEntity>> {
    return new Promise((resolve, promse) => {
      let newOfficeLocations = new Array<MainEntity>();
      locationFromDB.forEach((element: MainEntity) => {
        newOfficeLocations.push(this.initMainInstance(element))
      });
      if (newOfficeLocations.length == locationFromDB.length) {
        resolve(newOfficeLocations);
      }
    })
  }
  /**
   * Event upon select input. This will toggle published property
   * 
   * @param {any} event 
   * 
   * @memberOf OfficeLocationComponent
   */
  changePublishStatus(event): void {
    this.changeStatusForAll(event.srcElement.value == 'true').then((locationList: Array<Entity>) => {
      this.onSubmitMultiLocations(locationList);
    })
  }
  /**
   * Asynchronous to change officeLocation Published status.
   * 
   * @param {any} published 
   * @returns 
   * 
   * @memberOf OfficeLocationComponent
   */
  changeStatusForAll(published): Promise<Array<Entity>> {
    return new Promise((resolve, reject) => {
      let newofficeStatus = new Array<Entity>();
      this.entityLocation.locationList.forEach((location: Entity) => {
        location.cities.forEach(location => {
          location.published = published;
        });
        newofficeStatus.push(location);
      });
      if (newofficeStatus.length == this.entityLocation.locationList.length) {
        resolve(newofficeStatus);
      }
    })
  }


  /**
 * Delete an entity with all it's properties.
 * 
 * @param {any} officeLocation 
 * 
 * @memberOf OfficeLocationComponent
 */
  selectEntityToDelete(entity): void {
    this.entityToDelete = entity;
    this.toggleDelete = true;
    this.toggleDeleteLanguage = false;
    this.editPriority= false;
  }
  /**
 * Delete an entity with all it's properties.
 * 
 * @param {any} officeLocation 
 * 
 * @memberOf OfficeLocationComponent
 */
  selectPropertyToDelete(entity, indexCity): void {
    this.countryToDelete = entity.countryName
    this.entityToDelete = entity
    this.indexCityToDelete = indexCity
    this.toggleDelete = true;
    this.toggleDeleteLanguage = false;

    // console.log(entity);
  }


  /**
   * Delete a language from a Location.
   * 
   * @param {any} officeLocation 
   * @param {any} location 
   * 
   * @memberOf OfficeLocationComponent
   */
  selectLanguageToDelete(entity, language, indexCity): void {
    this.indexCityToDelete = indexCity;
    this.entityToDelete = entity;
    this.languageToDelete = language;
    this.toggleDeleteLanguage = true;
    this.toggleDelete = false;
  }
  /**
   *This will take the property with the language selected and delete it from the entity
   *
   * @memberof ModuleListingComponent
   */
  async removeLangAndSubmit() {
    this.entityToDelete.cities[this.indexCityToDelete].properties.forEach(office => {
      let indexToDelete;
      for (var i = 0; i < office.length; i++) {
        if (office[i].language == this.languageToDelete) {
          indexToDelete = i;
        }
      }
      office.splice(indexToDelete, 1);
    });
    if (this.entityLocation.module == 'Campaign') {
      await this.parseDataCamp.emit(this.entityToDelete);
      this.onSubmit(this.entityLocation.locationParsed);
    } else {
      this.onSubmit(this.entityToDelete)
    }
  }

  async removePropertyAndSubmit(entityToDelete) {
    if(entityToDelete.cities.length > 1){
      entityToDelete.cities.splice(this.indexCityToDelete, 1);
      this.indexCityToDelete=0
      if (this.entityLocation.module == 'Campaign') {
        await this.parseDataCamp.emit(this.entityToDelete);
        this.onSubmit(this.entityLocation.locationParsed);
      } else {
        this.onSubmit(this.entityToDelete)
      }
    }else{
      this.onDelete(entityToDelete);
    }
    
  }

  async setAsDefualtAndSubmit(entity, value){
    this.defaultCamp= value;
    entity.defaultCamp =value;
    if (this.entityLocation.module == 'Campaign') {
      await this.parseDataCamp.emit(entity);
      this.onSubmit(this.entityLocation.locationParsed);
    }
  }
  /**
   * Set to publish an office location property.
   * 
   * @param {any} officeLocation 
   * 
   * @memberOf OfficeLocationComponent
   */
  setLocationPublishStatus(entityLocation: Entity, publishStatus = true): void {

    entityLocation.cities.forEach(location => {
      location.published = publishStatus;
    });
    this.onSubmit(entityLocation);
  }

  /**
   * This will enable to show publish/un-published button.
   * 
   * @param {OfficeLocation} officeLocation 
   * @returns 
   * 
   * @memberOf OfficeLocationComponent
   */
  getStatusPublish(entityLocation: Entity): boolean {
    let publishButton;
    publishButton = entityLocation.cities.map(location => !location.published);
    return publishButton.indexOf(true) > -1;
  }


  /**
   * Togle selection from all country status.
   * 
   * 
   * @memberOf OfficeLocationComponent
   */
  toggleStatusSelection(): void {
    for (var i = 0; i < this.entityToPublish.length; i++) {
      this.entityToPublish[i] = this.bulkAction;
    }
  }


  editEntity(newLocationToEdit, cityIndex, countryIndex) {
    this.valueToEdit.emit({ newLocationToEdit, cityIndex, countryIndex })
  }


  /**
   *Open modal 
  *
  * @param {*} entity
  * @memberof ModuleListingComponent
  */
  setPriority(entity){
    this.editPriority = true;   
    this.toggleDeleteLanguage = false;
    this.toggleDelete = false;
    this.entityToPrior = new Campaign(entity.countryName, entity.countryCode, entity.defaultCamp, [...entity.cities]);
    this.propertySelected= this.entityToPrior.cities
  }

  async onSubmitPriority(entityToPrior){
    await this.parseDataCamp.emit(entityToPrior);
    // console.log(this.entityLocation.locationParsed)
    this.onSubmit(this.entityLocation.locationParsed);

  }
}
