import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleEditingComponent } from './module-editing.component';

describe('ModuleEditingComponent', () => {
  let component: ModuleEditingComponent;
  let fixture: ComponentFixture<ModuleEditingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleEditingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleEditingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
