
const MongoClient = require('mongodb').MongoClient;
const { MongoDataBase } = require('../mongodb/mongoCommonQuery');
const ParentController = require('./ParentController');
const replyHandler = require('../reply/replyHandler');
const replyMssgs = require ('../reply/replyMssgs');
const CriteriaBuilder = require('../utils/criteriaBuilder');
const ObjectId = require('mongodb').ObjectID;
const _ = require('underscore');

class ContactUsController extends ParentController{
    constructor(){
        super();
    }

   async addUpdateContactUs (request, reply, dbName) {
    request.payload._id = request.payload._id?  ObjectId(request.payload._id): new ObjectId();
    const { _id} = request.payload;
    const MongoDB = new MongoDataBase();
    const criteriaBuilder = new CriteriaBuilder();
        try{
            const { name } = request.payload;

            await MongoDB.connect();

            criteriaBuilder.setId(_id);
    
            const res = await MongoDB.addUpdateQuery(request.payload, dbName, criteriaBuilder.build());   
            
             reply(replyMssgs.createdResponse(null, res));
    
        }catch(error){
                
            reply(replyMssgs.createdResponse(error, null));
                    
        }finally{
    
            MongoDB.db.close();
        }
   
    };

}

module.exports = {
    ContactUsController
}