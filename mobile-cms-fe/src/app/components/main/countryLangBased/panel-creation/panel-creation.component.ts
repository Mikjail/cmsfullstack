import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GenericService, AlertService } from '../../../../services';

@Component({
  selector: 'app-panel-creation',
  templateUrl: './panel-creation.component.html',
  styleUrls: ['./panel-creation.component.scss']
})
export class PanelCreationComponent implements OnInit {
  @Input('locationInput') entityLocation = { 
    locationList: [], 
    customProperties: {}, 
    apiService:'', 
    apiServiceMulti:'',
    resultsFound:false,
    editEntity:false,
    countrySelectionSetting:false
   };


   @Input('languageList') languageList= [];
   @Input('countryList') countryList= [];
   @Input('countryField') countryField= false;
   @Input('textField') textField= false;
   @Input('textLabel') textLabel = "Name";
   @Input('languageField') languageField= false;
   @Input('buttonName') buttonName= "Setting";
   @Input('nameModule') nameModule = "Locations";


    @Output('addNewCountry') newCountryEvent = new EventEmitter();

      /**
   * Add New Country setting.
   * 
   * 
   * @memberOf OfficeLocationComponent
   */
  public titleSeting;
  public availableSelectionCountries;
  public countrySelectionSetting;
  public existingCountries;
  public newCountryToEdit;
  public newTextToEdit;
  public newLanguageToEdit;
  public newCountryPannel;
  public newCityPannel;
  public availableSelectionLanguage;
  public existingLocations;
  public existingCities;

  constructor( private genServices: GenericService,
    private alertService: AlertService) { 
    this.newCountryToEdit = 'Select New Country';
    this.existingCountries = new Array();
    this.newLanguageToEdit = 'Select New Language';
    this.newCountryPannel=false;
    this.newCityPannel=false;
    this.existingCities=[];
  }

  ngOnInit() {
    this.initExistingCountries();
  }

  initExistingCountries() {
    this.genServices.get(this.entityLocation.apiService).subscribe(
      response => {
        this.existingCountries = response.data.map(location => location.countryName);
        this.existingLocations = response.data;
        if(this.nameModule != 'Campaigns'){
          this.existingLocations.forEach(location => {
            location.cities.forEach(cityLoc => {
                this.existingCities.push(cityLoc.city.toLowerCase().replace(/\s/g,""))
                cityLoc.properties[0].forEach(office => {
                  this.existingCities.push(office.cityName.toLowerCase().replace(/\s/g,""));
                });
            });
        });
        }
 
        },
      error => {

      },
      () => {
      });
  }



  /**
   * Add a new country with all it's default properties.
   * 
   * 
   * @memberOf OfficeLocationComponent
   */
  addNewCountry(): void {
    if(!this.validForm()){
      let newLocationToEdit = this.existingLocations.filter( location => location.countryName == this.newCountryToEdit);
    
      this.newCountryEvent.emit({
        newCountryToEdit:this.newCountryToEdit,
        newLanguageToEdit: this.newLanguageToEdit,
        newTextToEdit: this.newTextToEdit,
        newLocationToEdit: newLocationToEdit[0]
      });
  
      this.newCountryToEdit = '';
    }
    console.log("error")
  }

  /**
   * Disabled add new Country Setting if there is not
   * Country/Language selected.
   * 
   * @returns 
   * 
   * @memberOf OfficeLocationComponent
   */
  validForm() {

    let errorArray=[];
    if (this.newLanguageToEdit == ''|| this.newLanguageToEdit == 'Select New Language') {
        errorArray.push("Select one of the available Languages");
    }

    if(this.availableSelectionCountries.indexOf(this.newCountryToEdit) < 0 ||
        !this.hasValue(this.newCountryToEdit) ||
        this.newCountryToEdit == 'Select New Language'){
        errorArray.push("Select one of the available Countries")
    }

    if(this.nameModule != 'Campaigns'){
      if(!this.hasValue(this.newTextToEdit)){
        errorArray.push(this.textLabel + " is required")
      }else{
        if(this.existingCities.indexOf(this.newTextToEdit.toLowerCase().replace(/\s/g,"")) > -1){
          errorArray.push( this.newTextToEdit+ " already exist")
        }
      }
    }
    

    if(errorArray.length > 0){ 
      this.alertService.errorArray(errorArray);
    }

    return errorArray.length > 0
  
  }



  hasValue(value){
    if(value !== undefined && value !== null){
      return value.replace(/\s/g,"") != ''
    }
    
  }
  /**
   * Once add New Office Location is clicked 
   * It change @param countrySelectionSetting variable to the opposite value (true -> false or false->true)
   * In order to open/close dropdown
   * 
   * @memberOf OfficeLocationComponent
   */
  toggleCountrySelection(): void {
    this.togglePanel();
    this.newCountryPannel = true;
    let countriesArray = this.countryList.map(country => country.countryName);
    this.titleSeting="New Country"
    this.newCountryToEdit=""
    if(this.existingCountries.length > 0){
      this.availableSelectionCountries = countriesArray
      .filter(country =>  this.existingCountries.indexOf(country) < 0)
    }else{
      this.availableSelectionCountries = countriesArray
    }
  }

  /**
   * 
   * 
   * @memberof PanelCreationComponent
   */
  toggleCitySelection(){
      this.newCountryPannel = false;
      this.togglePanel();
      let countriesArray = this.countryList.map(country => country.countryName);
      this.newCountryToEdit="Select New Country"
      this.titleSeting=`New ${this.textLabel}`
      this.availableSelectionCountries = countriesArray;
    }

  toggleCampaignSelection(){

  }

  togglePanel(){
    this.entityLocation.countrySelectionSetting = !this.entityLocation.countrySelectionSetting;
  }
}
