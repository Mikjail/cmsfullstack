import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { Constants } from '../models/index';
import { AlertService, AuthenticationService } from '../services/index';
import { Router, RouterModule, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  
  beforeEach(async(() => {
  
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports:[ HttpModule, FormsModule ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [ 
        AuthenticationService,
        AlertService,
        {provide: Router, useClass: MockRouter},
        Constants ] 

    })
    .compileComponents();
  }));

  beforeEach( async(inject([AuthenticationService],()=> {
  fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  })));
  
  it('should create', async(inject([AuthenticationService],(genService) => {
      expect(genService).toBeDefined();
      expect(component.login).toBeDefined();

  })));

  it('should create', () => {
    // spyOn(component.login).
  });
});

class RouterStub {
  navigateByUrl(url: string) { return url; }
}

class MockRouter {
  public ne = new NavigationEnd(0, 'http://localhost:4200/login', 'http://localhost:4200/login');
  public events = new Observable(observer => {
    observer.next(this.ne);
    observer.complete();
  });
}