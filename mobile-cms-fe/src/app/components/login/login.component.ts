import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User, UserModel } from '../../models';
import { AlertService, AuthenticationService } from '../../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    userForm: any = {};
    username;
    loading = false;
    returnUrl: string;
    user : User;
    userModel: UserModel;
    constructor(
       private router: Router,
       private authenticationService: AuthenticationService,
       private alertService: AlertService
    ) { this.user=new User();}

    ngOnInit() {
       // get return url from route parameters or default to '/'
       this.returnUrl = '/';
     
       if(this.isSignedIn()){
        this.router.navigateByUrl(this.returnUrl);
       } 
        // reset login status
        this.authenticationService.logout().subscribe(
            response => {
            
            },
            error =>{
              localStorage.removeItem('currentUser');
              localStorage.removeItem('token');
              this.router.navigateByUrl("/login");
            })

    }

    isSignedIn() {
        let user = localStorage.getItem('currentUser');
        return user != null && user != undefined && user != ""
    }
    
    login(value) {
        console.log(value)
        this.loading = true;
        this.authenticationService.login(value).subscribe(
                response => {
                    localStorage.setItem('currentUser', JSON.stringify(response.data[0]));
                    this.router.navigateByUrl(this.returnUrl);
                    this.loading = false;
                },
                error => {
                    this.alertService.errorArray([error.message.description]);
                    this.loading = false;
                });
    }
}
