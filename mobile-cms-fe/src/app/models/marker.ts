export interface Marker{
    longitude: any;
    latitude: any;
    draggable: boolean;
}