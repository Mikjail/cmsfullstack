const should = require('should');
const mocha = require('mocha');
const Glue = require('glue');
const constants = require('./private/modules/constants');
const Config = require('./config');
const replyHandler = require('./private/modules/reply/replyHandler');
const request = require('request');
const options = {
    relativeTo: '/home/degorov/nodeoracledb/examples/api-transformer/server/private/modules'
};

//MESSAGE TO LASA/ANY OTHER DEV WHO WILL CONTINUE TO WORK ON THIS:
//IT'S UGLY SOLUTION BECAUSE WE USE GLUE PLUGING
//AND CANT EASILY TAKE CONTEXT FROM SERVER
//WE CAN LIVE WITH IT TEMPORALLY
// TO MAKE 1ST TEST WORK WE NEED TO ADD REQUEST FINISHES AND AFTER ASSERT RESULTS

mocha.describe('test', function(){
    mocha.before('starting server',function(){
        Glue.compose(Config.manifest, options, function (err, server) {
            Config.initApplication();
            if (err) {
                throw err;
            }
            // add unexpected exception handler
            server.ext('onPreResponse',function (request, reply) {
                replyHandler.preResponseHandler(request, reply);
            });
            // Start the server
            server.start((err) => {
                if (err) {
                    throw err;
                }
                console.log('Server running at:', server.info.uri);
            });

            console.log('test started..');
        });
    });

    mocha.it('FIRST E2E TEST', () => {
        let combinedData = '';
        const body = {
            coreModel:{
                token:"fkdajhsfkhkshgkhfksghksg",
                app:{
                    appVersion:"1.0.0",
                    apiKey:"dashlfkdjalfhka",
                    os:"android",
                    language:"all"
                }
            },
            hash: ""
        };
        const options = {
            method: 'post',
            body: body,
            json: true,
            url: 'http://localhost:' + constants.SERVER_PORT + '/mobile-engine/api/appdata'
        };
        request(options).on('data', function (chunk) {
            combinedData += String.fromCharCode.apply(null, new Uint8Array(chunk));
        }).on('response', function (response) {
            if (response.statusCode !== 200) {
                console.log("E2E WORKS!");
            }
        }).on('error', function (error) {
            mocha.assert(combinedData,'');
            console.log("E2E ERROR!");
        }).on('end', function (data) {
            mocha.assert(combinedData,'');
            // console.log("data: " + combinedData);
        });
    });

});


