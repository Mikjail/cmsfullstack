import { CityModel, CityProperty, Entity } from "./Entity";

export class OfficeLocation extends Entity{
    cities: Array<CityOffices>
    constructor(countryName:string = "", countryCode:string="",cities:Array<CityOffices> = new Array<CityOffices>()){
        super(countryName,countryCode, cities)
    }
}

export class CityOffices extends CityModel{
   properties:Array<Array<OfficeProperties>>;
   
   constructor(city:string="", properties:Array<Array<OfficeProperties>>=new Array<Array<OfficeProperties>>(), published:boolean=false){
       super(city,properties,published)
   }
}

export class OfficeProperties extends CityProperty{
    address: string;
    latitude:string;
    longitude:string;

    constructor(language,
         cityName:string="",
         openingTime:string="", 
         telephone:string="",
         address:string="", 
         latitude:string="", 
         longitude:string=""){
             
        super(language, cityName, openingTime, telephone);
        
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}


