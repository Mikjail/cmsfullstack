const MongoClient = require('mongodb').MongoClient;
const { MongoDataBase } = require('../mongodb/mongoCommonQuery');
const ParentController = require('./ParentController');
const replyHandler = require('../reply/replyHandler');
const replyMssgs = require ('../reply/replyMssgs');
const constants = require('../constants');
const CriteriaBuilder = require('../utils/criteriaBuilder');

const _ = require('underscore');

class CampaignController extends ParentController{
    constructor(){
        super();
    }
    

    async getMobileCampaings (request, reply){
        const { language} =request.payload.app;
        const { countryCode } = request.payload;
        const limit = request.url.query.limit ? parseInt(request.url.query.limit) : Number.MAX_SAFE_INTEGER;
        let criteriaBuilder = new CriteriaBuilder();
        const MongoDB = new MongoDataBase();
        try{
            
            await MongoDB.connect();
                
            criteriaBuilder.filterByCountryLanguageCamp(countryCode, language);
     
            let res = await MongoDB.findQuery(criteriaBuilder.build(), "campaigns", limit);
            if(res.length < 1){
                delete request.payload.app.language;
                criteriaBuilder = new CriteriaBuilder();   
                criteriaBuilder.filterByCountryLanguageCamp(countryCode);
                res = await MongoDB.findQuery(criteriaBuilder.build(), "campaigns", limit);
                if(res.length < 1){
                    delete request.payload.countryCode;
                    criteriaBuilder = new CriteriaBuilder();
                    criteriaBuilder.setDefCampOr();
                    res = await MongoDB.findQuery(criteriaBuilder.build(), "campaigns", limit);
                }
            }
            const dataParsed= await  this.parseDataCampaignToMobile(res, request.payload);
            
            reply(replyMssgs.foundMobileResponse(null, dataParsed));
        
        }catch(error){
            reply(replyMssgs.createdResponse(error, null));
        }finally{
            MongoDB.db.close();
        }
    }

  async parseDataCampaignToMobile(campaigns, payload){
    let mobileResult=[];

        for (const campaign of campaigns) {
            if(payload.countryCode){
                if (campaign.countryCode == payload.countryCode.toUpperCase()){
                    for(let currentElement of campaign.campaigns) {
                        let campaignsProp = await this.getCampaigns(currentElement.properties , payload.app.language);
                        if(campaignsProp)
                        mobileResult.push(campaignsProp);
                    }  
                }
            }
            else{
                for(let currentElement of campaign.campaigns) {
                    let campaignsProp = await this.getCampaigns(currentElement.properties , payload.app.language);
                    if(campaignsProp)
                    mobileResult.push(campaignsProp);
                };  
            }
        }
        return mobileResult;
    }

    getCampaigns(currentElements, language){
       return new Promise((resolve ,reject) => {
        let today = this.getToday();
        let mobileCampaign;
        if(language){
            mobileCampaign = currentElements.find(currentCampaign => language == currentCampaign.language &&
                currentCampaign.fromSchedule.replace(/\//g,'') <= today &&
                currentCampaign.toSchedule.replace(/\//g,'') >= today);
        }
        else{
            mobileCampaign =  currentElements.find(currentCampaign => currentCampaign.fromSchedule.replace(/\//g,'') <= today &&
                currentCampaign.toSchedule.replace(/\//g,'') >= today);
        }

        resolve( {link:mobileCampaign.link, image:  constants.CMS_INSTANCE + '/mobile-engine/api/CMS/files/'+mobileCampaign.fileName });
        
        })
    }
    
    getToday() {
        let today = new Date();
        return [today.getFullYear(),
                ('0' + (today.getMonth() + 1)).slice(-2),
                ('0' + today.getDate()).slice(-2)
                ].join('')
    }
}

module.exports = {
        CampaignCtrl : new CampaignController(),
        CampaignController
};
