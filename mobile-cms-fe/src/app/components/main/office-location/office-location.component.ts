import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { 
  FormArray,
  ReactiveFormsModule, 
  FormBuilder, 
  FormGroup, 
  FormControl,
  Validators 
} from '@angular/forms';
import { AlertService } from '../../../services';
import { 
    Constants,
    API_REST, 
    OfficeLocation, 
    CityProperty,
    CityModel, 
    CityOffices,
    OfficeProperties } from '../../../models';
import { SearchFilterComponent} from '../../utils/search-filter';
import { ModuleListingComponent} from '../countryLangBased/module-listing';


@Component({
  selector: 'app-office-location',
  templateUrl: './office-location.component.html',
  styleUrls: ['./office-location.component.scss']
})

export class OfficeLocationComponent implements OnInit {
  @Input('searchInput') searchFilter = { 
    apiRest:'', 
    availableSearch:{
      searchStatus: false,
      searchName:false,
      searchCountry: true,
      searchLanguage: true
    },
    countryFromServer:[],
    languageArray:[],
    makeAction:false };


  /**
   * Entity directive.
   * 
   * @type {ModuleListingComponent}
   * @memberof OfficeLocationComponent
   */
  @ViewChild(ModuleListingComponent) EntityComponent: ModuleListingComponent;
   /**
   * Select an Office and set @param this.editOfficeLocation to edit it.
   * 
   * @param {any} officeLocation 
   * 
   * @memberOf OfficeLocationComponent
   */
  editLocation({newLocationToEdit, cityIndex}): void {
        this.locationToEdit.edition = true;
        this.locationToEdit.cityIndexSelected = cityIndex;
        this.locationToEdit.entityLocation = newLocationToEdit
        this.locationToEdit.apiService=this.apiRest.API_OFFICE_LOCATIONS;
        this.locationToEdit.module = 'OfficeLocation';
  }


  /**
   * TODO AFTER GET FROM SEARCH SEND IT TO ENTITYCOMPONENT
   * Search Filter Directive.
   * 
   * @type {SearchFilterComponent}
   * @memberof OfficeLocationComponent
   */
  @ViewChild(SearchFilterComponent) searchFilterComponent: SearchFilterComponent;
  getFromSearch(officeLocationsFound){
    this.locationToEdit.edition = false;
    if(officeLocationsFound.value.length > 0){
      this.officeLocations = officeLocationsFound.value.sort(this.compareByCountry);
      this.EntityComponent.refreshLocations(this.officeLocations); 
      this.entityLocation.resultsFound = true;
    }else{
      this.entityLocation.resultsFound = false;
    }
  } 
  /**
   * Filter Form.
   * 
   * @type {Array<string>}
   * @memberOf CampaignListComponent
   */
  public languagesArray;
  public resultsFound = false;
  
  /**
   * ROOT URL HOST
   * 
   * @type {string}
   * @memberOf CampaignEditComponent
   */
  public apiRest: API_REST;

  /**
 * For variable to create formvalidation.
 * @param myForm
 * @type {FormGroup}
 * @memberOf LabelComponent
 *
 * @param submitted
 * @type {boolean}
 */
  public myForm: FormGroup;
  public submitted = false;

  /**
   * List to show.
   * 
   * 
   * @memberOf CampaignListComponent
   */
  public countriesSelected;
  public value: any = ["United Arab Emirates"];
  public officeLocations;

  
   /**
   * To enable Callcenter edit directive.
   * 
   * @memberof CallcenterComponent
   */
  public locationToEdit = { 
    edition: false, 
    entityLocation: new OfficeLocation(),
    cityIndexSelected: '',
    apiService:'',
    module: ''
  };
  
/**
 * Module Listing / Panel Creation Directive 
 * 
 * @memberof ModuleListingComponent
 */
public entityLocation = { 
    locationList: [],
    customProperties: {}, 
    apiService:'',
    apiServiceMulti: '', 
    resultsFound:false,
    editEntity: false ,
    countrySelectionSetting: false,
  };
  

  /**
   * Pagination Directive
   * 
   * @private
   * @type {any[]}
   * @memberOf CampaignListComponent
   */
  public paginationList = {
    entityList: [],
    pagedItems: [],
    pager: {},
    setPage: {},
    customNumberPages: 8
  };
 
  /**
   * Edit data table. 
   * 
   * 
   * @memberOf OfficeLocationComponent
   */
  public officeToDelete;
  public toggleDelete = false;
  public officeToPublish;
  public bulkAction;
  public languageToDelete;
  public toggleDeleteLanguage = false;
  

  constructor(private fb: FormBuilder,
    private alertService: AlertService,
    private constants: Constants) {
    this.apiRest = constants.IApiRest;
    this.countriesSelected = [];
    this.officeLocations = new Array<OfficeLocation>();
    this.entityLocation.apiService=this.apiRest.API_OFFICE_LOCATIONS;
    this.entityLocation.apiServiceMulti = this.apiRest.API_OFFICE_LOCATIONS_MULTI;
    this.searchFilter.apiRest = this.apiRest.API_OFFICE_LOCATIONS;
    this.officeToDelete = new OfficeLocation();
    this.officeToPublish = new Array();
    this.locationToEdit = { 
      edition: false, 
      cityIndexSelected: '',
      entityLocation: new OfficeLocation(),
      apiService:this.apiRest.API_OFFICE_LOCATIONS,
      module: 'OfficeLocation'
    };
    
  }

  ngOnInit() {
  }

   

   /**
   * Helper to sort an Array of Campaigns 
   * 
   * @param {OfficeLocation} countryA 
   * @param {OfficeLocation} countryB 
   * @returns {number} 
   * 
   * @memberOf CampaignListComponent
   */
  compareByCountry(countryA:OfficeLocation,countryB:OfficeLocation):number{
    if(countryA.countryName < countryB.countryName){
      return -1
    }
    if(countryA.countryName > countryB.countryName){
      return 1
    }
    return 0;
  }
  
  
  /**
   * Add a new country with all it's default properties.
   * 
   * 
   * @memberOf OfficeLocationComponent
   */
  addNewCountry({newCountryToEdit, newLanguageToEdit, newTextToEdit, newLocationToEdit}): void {
    let countrySelected = this.searchFilter.countryFromServer.find(country => country.countryName == newCountryToEdit);
    if(!newLocationToEdit){
        newLocationToEdit = new OfficeLocation(countrySelected.countryName, countrySelected.countryCode);
    }
    newLocationToEdit.cities.push(new CityOffices(newTextToEdit));
    let cityIndex = newLocationToEdit.cities.length-1;
    let properties = []
    properties[0] = new OfficeProperties(newLanguageToEdit, newTextToEdit)
    newLocationToEdit.cities[cityIndex].properties[0]=properties;
    this.editLocation({newLocationToEdit, cityIndex});
    this.entityLocation.countrySelectionSetting = false;
  }


}
