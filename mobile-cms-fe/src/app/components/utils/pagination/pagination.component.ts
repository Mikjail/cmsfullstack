import { Component, OnInit, Input } from '@angular/core';
import { PagerService, LoaderService } from '../../../services/index';


@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input('init') paginationList = { entityList: [] , pagedItems: [], pager: {}, customNumberPages: undefined };
 
  /**
   * PAGINATION
   * 
   * @private
   * @type {any[]}
   * @memberOf CampaignListComponent
   */
  // pager object
  public pager: any = {};
  // paged items
  public pagedItems: any[];
  //entity to List.
  public entityList
  
  public customNumberPages: number;

    
  constructor( private pagerService: PagerService,
    private loader: LoaderService) { }

  ngOnInit() {
    this.setPage(1);
  }
  /**
   * Pagination logic.
   * 
   * @param {number} page 
   * @returns 
   * 
   * @memberOf CampaignListComponent
   */
  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
        return;
    }
    this.paginationList.entityList;
    // get pager object from service
    this.loader.show()
    this.pagerService.getPager(this.paginationList.entityList.length, page, this.paginationList.customNumberPages || 8).then((pager)=>{
    this.pager = pager;
      // get current page of items
    this.pagedItems = this.paginationList.entityList.slice(this.pager.startIndex, this.pager.endIndex + 1);
    this.loader.hide();

    this.paginationList.pager = this.pager;
    this.paginationList.pagedItems = this.pagedItems;
    })
  }



}
