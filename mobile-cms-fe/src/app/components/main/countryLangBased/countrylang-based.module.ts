import { NgModule, CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA  } from "@angular/core";
import { ModuleEditingComponent, ModuleListingComponent, PanelCreationComponent } from ".";
import { BrowserModule } from "@angular/platform-browser";
import { UtilsModule } from "../../utils/utils.module";

@NgModule({
    declarations:[
        ModuleEditingComponent,
        ModuleListingComponent,
        PanelCreationComponent
    ],
    exports:[
        ModuleEditingComponent,
        ModuleListingComponent,
        PanelCreationComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    imports:[
        UtilsModule
    ],
    entryComponents:[ModuleListingComponent]
})

export class CountryLangBasedModule {}
